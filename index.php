<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title>HOTEL MANAGEMENT SYSTEM</title>
    <!-- Bootstrap Core CSS -->
    <link href="main/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="main/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="main/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="main/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="http://www.w3schools.com/lib/w3data.js"></script>
    <!--alerts CSS -->
    <link href="plugins/bower_components/sweetalert/sweetalert2.min.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal form-material" method="post">
                    <h3 class="box-title m-b-20 text-center">LOG IN</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" required="" placeholder="Username" name="username"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" placeholder="Password" name="password"/>
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>
                            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
                    </div>-->
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="btnLogin">Log In</button>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="index.html">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="main/bootstrap/dist/js/tether.min.js"></script>
    <script src="main/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="main/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="main/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Sweet-Alert  -->
    <script src="plugins/bower_components/sweetalert/sweetalert2.min.js"></script>
</body>

</html>
<?php
if(isset($_POST['btnLogin'])){
    include 'main/phpfunctions/connect.php';
    $uname = $_POST['username'];
    $pword = md5($_POST['password']);
    $query_user = "SELECT * FROM tbl_users WHERE username = '$uname' AND password = '$pword'";
    $stmt_user = $DBcon->prepare( $query_user );
    $stmt_user->execute();

    if($stmt_user->rowCount() == 1) {
        $query_user_info = "SELECT * FROM tbl_users WHERE username = '$uname' AND password = '$pword'";
        $stmt_user_info = $DBcon->prepare( $query_user_info );
        $stmt_user_info->execute();
        while($row_user_info=$stmt_user_info->fetch(PDO::FETCH_ASSOC)) {
            $my_id = $row_user_info["id"];
            extract($row_user_info);

            $id = $row_user_info["id"];
            $username = $row_user_info["username"];
            $password = $row_user_info["password"];
            $password_temp = $row_user_info["password_temp"];
            $firstname = $row_user_info["first_name"];
            $lastname = $row_user_info["last_name"];
            $email = $row_user_info["email"];
            $userlevel = $row_user_info["userlevel"];


        }

        $_SESSION["user_id"] = $id;
        $_SESSION["username"] = $username;
        $_SESSION["password"] = $password;
        $_SESSION["password_temp"] = $password_temp;
        $_SESSION["first_name"] = $firstname;
        $_SESSION["last_name"] = $lastname;
        $_SESSION["email"] = $email;
        $_SESSION["userlevel"] = $userlevel;

        $test_uname = $userlevel;
        if($test_uname == "CLERK"){

            $query_log = "INSERT INTO tbl_logs (user,activity) VALUES ('$username','$username logged in.')";
            $stmt_log = $DBcon->prepare( $query_log );
            $stmt_log ->execute();
            echo"
                <script type='text/javascript'>
                    swal({
                      title: 'SUCCESS!',
                      text: 'Welcome Clerk!',
                      type: \"success\",
                      timer: 1000,
                      allowOutsideClick: false,
                    }).then(
                      function() {
                    // Redirect the user
                    window.location.href = \"main/_frontdesk/index.php\";
                    console.log('The Ok Button was clicked.');
                    },
                      // handling the promise rejection
                      function (dismiss) {
                        if (dismiss === 'timer') {
                           window.location.href = \"main/_frontdesk/index.php\";
                        }
                      }
                    )
                </script>
            ";
        }
        elseif($test_uname == "MANAGER"){
            $query_log = "INSERT INTO tbl_logs (user,activity) VALUES ('$username','You logged in to the system')";
            $stmt_log = $DBcon->prepare( $query_log );
            $stmt_log ->execute();
            echo"
                <script type='text/javascript'>
                    swal({
                      title: 'SUCCESS!',
                      text: 'Welcome Manager!',
                      type: \"success\",
                      timer: 1000,
                      allowOutsideClick: false,
                    }).then(
                      function() {
                    // Redirect the user
                    window.location.href = \"main/_manager/index.php\";
                    console.log('The Ok Button was clicked.');
                    },
                      // handling the promise rejection
                      function (dismiss) {
                        if (dismiss === 'timer') {
                           window.location.href = \"main/_manager/index.php\";
                        }
                      }
                    )
                </script>
            ";
        }
        elseif($test_uname == "INVENTORY_ADMIN"){
            $query_log = "INSERT INTO tbl_logs (user,activity) VALUES ('$username','Logged in to the system')";
            $stmt_log = $DBcon->prepare( $query_log );
            $stmt_log ->execute();
            echo"
                <script type='text/javascript'>
                    swal({
                      title: 'SUCCESS!',
                      text: 'Welcome!',
                      type: \"success\",
                      timer: 1000,
                      allowOutsideClick: false,
                    }).then(
                      function() {
                    // Redirect the user
                    window.location.href = \"main/_inventory_admin/index.php\";
                    console.log('The Ok Button was clicked.');
                    },
                      // handling the promise rejection
                      function (dismiss) {
                        if (dismiss === 'timer') {
                           window.location.href = \"main/_inventory_admin/index.php\";
                        }
                      }
                    )
                </script>
            ";
        }
        elseif($test_uname == "INVENTORY_FOOD"){
            $query_log = "INSERT INTO tbl_logs (user,activity) VALUES ('$username','Logged in to the system')";
            $stmt_log = $DBcon->prepare( $query_log );
            $stmt_log ->execute();
            echo"
                <script type='text/javascript'>
                    swal({
                      title: 'SUCCESS!',
                      text: 'Welcome!',
                      type: \"success\",
                      timer: 1000,
                      allowOutsideClick: false,
                    }).then(
                      function() {
                    // Redirect the user
                    window.location.href = \"main/_inventory_food/index.php\";
                    console.log('The Ok Button was clicked.');
                    },
                      // handling the promise rejection
                      function (dismiss) {
                        if (dismiss === 'timer') {
                           window.location.href = \"main/_inventory_food/index.php\";
                        }
                      }
                    )
                </script>
            ";
        }
        elseif($test_uname == "INVENTORY_UTILITIES"){
            $query_log = "INSERT INTO tbl_logs (user,activity) VALUES ('$username','Logged in to the system')";
            $stmt_log = $DBcon->prepare( $query_log );
            $stmt_log ->execute();
            echo"
                <script type='text/javascript'>
                    swal({
                      title: 'SUCCESS!',
                      text: 'Welcome!',
                      type: \"success\",
                      timer: 1000,
                      allowOutsideClick: false,
                    }).then(
                      function() {
                    // Redirect the user
                    window.location.href = \"main/_inventory_utilities/index.php\";
                    console.log('The Ok Button was clicked.');
                    },
                      // handling the promise rejection
                      function (dismiss) {
                        if (dismiss === 'timer') {
                           window.location.href = \"main/_inventory_utilities/index.php\";
                        }
                      }
                    )
                </script>
            ";
        }
    }
    else{
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'Invalid Login! Check your credentials',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"index.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"index.php\";
                    }
                  }
                )
			</script>
		";
    }
}
?>
