-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 05:10 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hotel_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ims_stock_history`
--

CREATE TABLE IF NOT EXISTS `ims_stock_history` (
  `id` int(11) NOT NULL,
  `activity` text NOT NULL,
  `stock_name` varchar(100) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user` varchar(35) NOT NULL,
  `care_of` varchar(60) NOT NULL,
  `acquisition_type` varchar(5) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ims_stock_history`
--

INSERT INTO `ims_stock_history` (`id`, `activity`, `stock_name`, `supplier`, `quantity`, `user`, `care_of`, `acquisition_type`, `timestamp`) VALUES
(1, 'inv_f added 2 stocks to HAM (1-Pack)', 'HAM (1-Pack)', 'Purefoods', 2, 'inv_f', '', 'IN', '2018-02-28 14:55:26'),
(2, 'inv_f release a (1)HAM (1-Pack)', 'HAM (1-Pack)', 'N/A', 1, 'inv_f', 'Justin', 'OUT', '2018-02-28 14:58:34'),
(3, 'inv_u added 10 stocks to Cleaning Mop', 'Cleaning Mop', 'Magic Inc.', 10, 'inv_u', '', 'IN', '2018-02-28 16:25:13');

-- --------------------------------------------------------

--
-- Table structure for table `ims_tbl_category`
--

CREATE TABLE IF NOT EXISTS `ims_tbl_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `category_user` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ims_tbl_category`
--

INSERT INTO `ims_tbl_category` (`id`, `name`, `category_user`) VALUES
(7, 'Food', 'INVENTORY_FOOD'),
(8, 'Supply', 'INVENTORY_UTILITIES');

-- --------------------------------------------------------

--
-- Table structure for table `ims_tbl_stocks`
--

CREATE TABLE IF NOT EXISTS `ims_tbl_stocks` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `category` varchar(60) NOT NULL,
  `quantity` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ims_tbl_stocks`
--

INSERT INTO `ims_tbl_stocks` (`id`, `name`, `category`, `quantity`) VALUES
(1, 'HAM (1-Pack)', 'Food', 15),
(2, 'Salt (10Kilo)', 'Food', 67),
(3, 'Cleaning Mop', 'Supply', 13),
(4, 'Sugar (10Kilo)', 'Food', 0),
(5, 'Bread', 'Food', 0),
(6, 'Coca-Cola (1Can)', 'Food', 0),
(7, 'Cheese (1Bar)', 'Food', 0),
(8, 'Tomato Sauce (1pack)', 'Food', 0),
(9, 'Liver Spread (small)', 'Food', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities`
--

CREATE TABLE IF NOT EXISTS `tbl_activities` (
  `activity_id` int(11) NOT NULL,
  `act_name` varchar(60) NOT NULL,
  `act_status` varchar(20) NOT NULL,
  `floor_position` varchar(60) NOT NULL,
  `room_name` varchar(60) NOT NULL,
  `room_type` varchar(60) NOT NULL,
  `room_rate` varchar(60) NOT NULL,
  `checkin_date` varchar(60) NOT NULL,
  `checkin_time` varchar(60) NOT NULL,
  `checkout_date` varchar(60) NOT NULL,
  `checkout_time` varchar(60) NOT NULL,
  `guest_av_days` int(3) NOT NULL,
  `room_charges` varchar(100) NOT NULL,
  `transaction_date` varchar(60) NOT NULL,
  `transaction_number` text NOT NULL,
  `guest_discount` varchar(5) NOT NULL,
  `guest_total_addons` varchar(100) NOT NULL,
  `guest_total_payable_wo_discount` varchar(100) NOT NULL,
  `guest_total_payable_w_discount` varchar(100) NOT NULL,
  `guest_cash` varchar(100) NOT NULL,
  `guest_change` varchar(100) NOT NULL,
  `guest_name` varchar(100) NOT NULL,
  `guest_gender` varchar(25) NOT NULL,
  `guest_address` varchar(200) NOT NULL,
  `guest_id_type` varchar(60) NOT NULL,
  `guest_id_number` varchar(30) NOT NULL,
  `guest_phone_number` varchar(20) NOT NULL,
  `clerk` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activities`
--

INSERT INTO `tbl_activities` (`activity_id`, `act_name`, `act_status`, `floor_position`, `room_name`, `room_type`, `room_rate`, `checkin_date`, `checkin_time`, `checkout_date`, `checkout_time`, `guest_av_days`, `room_charges`, `transaction_date`, `transaction_number`, `guest_discount`, `guest_total_addons`, `guest_total_payable_wo_discount`, `guest_total_payable_w_discount`, `guest_cash`, `guest_change`, `guest_name`, `guest_gender`, `guest_address`, `guest_id_type`, `guest_id_number`, `guest_phone_number`, `clerk`) VALUES
(1, 'CHECKIN', 'DONE', '1', 'Room 100', 'Single', '1500', '02/25/2018', '7:25 PM', '02/26/2018', '7:25 PM', 1, '1500', '2018/02/25', 'HTL0000003', '0', '200', '1700', '1700', '1700', '0', 'Mario Quinto', 'Male', 'Manila, Philippines', 'Drivers License', '3232323', '09304923311', 'clerk01'),
(2, 'CHECKIN', 'ONGOING', '1', 'ROOM 101', 'Single', '1500', '02/25/2018', '7:35 PM', '02/27/2018', '7:35 PM', 2, '3000', '2018/02/25', 'HTL0000004', '0', '700', '3700', '3700', '3700', '0', 'Andres Dioquinto', 'Male', 'Manila, Philippines', 'Voters ID', '232323454', '5457872', 'clerk01'),
(3, 'CHECKIN', 'ONGOING', '2', 'ROOM 200', 'Economical', '1300', '02/26/2018', '07:40 PM', '02/27/2018', '07:40 PM', 1, '1300', '2018/02/25', 'HTL0000005', '0', '1200', '2500', '2500', '3000', '500', 'John Santos', '', 'Mangaldan, Pangasinan', 'Drivers License', '09543248', '09506852211', 'clerk01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_addons`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_addons` (
  `id` int(11) NOT NULL,
  `ao_name` varchar(60) NOT NULL,
  `ao_quantity` varchar(4) NOT NULL,
  `ao_cost` varchar(4) NOT NULL,
  `ao_total` varchar(60) NOT NULL,
  `transaction_number` varchar(60) NOT NULL,
  `transaction_date` varchar(60) NOT NULL,
  `paid_status` varchar(10) NOT NULL,
  `clerk` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activities_addons`
--

INSERT INTO `tbl_activities_addons` (`id`, `ao_name`, `ao_quantity`, `ao_cost`, `ao_total`, `transaction_number`, `transaction_date`, `paid_status`, `clerk`) VALUES
(1, 'Pillow', '1', '200', '200', 'HTL0000003', '02/25/2018', 'YES', 'clerk01'),
(2, 'Pillow', '1', '200', '200', 'HTL0000004', '02/25/2018', 'YES', 'clerk01'),
(3, 'Bed Sheet', '1', '500', '500', 'HTL0000004', '02/25/2018', 'YES', 'clerk01'),
(4, 'Blanket', '2', '600', '1200', 'HTL0000005', '02/25/2018', 'YES', 'clerk01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_addons_temp`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_addons_temp` (
  `id` int(11) NOT NULL,
  `ao_name` varchar(60) NOT NULL,
  `ao_quantity` varchar(4) NOT NULL,
  `ao_cost` varchar(4) NOT NULL,
  `ao_total` varchar(60) NOT NULL,
  `transaction_number` varchar(60) NOT NULL,
  `transaction_date` varchar(60) NOT NULL,
  `paid_status` varchar(10) NOT NULL,
  `clerk` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activities_addons_temp`
--

INSERT INTO `tbl_activities_addons_temp` (`id`, `ao_name`, `ao_quantity`, `ao_cost`, `ao_total`, `transaction_number`, `transaction_date`, `paid_status`, `clerk`) VALUES
(1, 'Pillow', '1', '200', '200', 'HTL0000003', '', '', ''),
(2, 'Pillow', '1', '200', '200', 'HTL0000004', '02/25/2018', '', 'clerk01'),
(3, 'Bed Sheet', '1', '500', '500', 'HTL0000004', '02/25/2018', '', 'clerk01'),
(4, 'Blanket', '2', '600', '1200', 'HTL0000005', '', '02/25/2018', 'clerk01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_history`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_history` (
  `id` int(11) NOT NULL,
  `act_name` text NOT NULL,
  `transaction_number` varchar(30) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_unbilled_charges`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_unbilled_charges` (
  `id` int(11) NOT NULL,
  `checkout_time` varchar(20) NOT NULL,
  `checkout_date` varchar(20) NOT NULL,
  `room_charges` varchar(5) NOT NULL,
  `addon_charges` varchar(5) NOT NULL,
  `total_unbilled_charges` varchar(5) NOT NULL,
  `transaction_number` varchar(20) NOT NULL,
  `guest_cash` varchar(60) NOT NULL,
  `guest_change` varchar(60) NOT NULL,
  `paid_status` varchar(20) NOT NULL,
  `transaction_date` varchar(60) NOT NULL,
  `clerk` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activities_unbilled_charges`
--

INSERT INTO `tbl_activities_unbilled_charges` (`id`, `checkout_time`, `checkout_date`, `room_charges`, `addon_charges`, `total_unbilled_charges`, `transaction_number`, `guest_cash`, `guest_change`, `paid_status`, `transaction_date`, `clerk`) VALUES
(1, '', '', '0', '0', '0', 'HTL0000003', '0', '0', 'YES', '2018/02/25', ''),
(2, '08:25 PM', '03/01/2018', '3000', '0', '3000', 'HTL0000004', '0', '0', 'NO', '', ''),
(3, '', '', '0', '0', '0', 'HTL0000005', '0', '0', 'NO', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_guests`
--

CREATE TABLE IF NOT EXISTS `tbl_guests` (
  `id` int(11) NOT NULL,
  `guest_name` varchar(100) NOT NULL,
  `guest_gender` varchar(25) NOT NULL,
  `guest_address` varchar(250) NOT NULL,
  `guest_id_type` varchar(60) NOT NULL,
  `guest_id_number` varchar(30) NOT NULL,
  `guest_phone_number` varchar(19) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id` int(11) NOT NULL,
  `user` text NOT NULL,
  `activity` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_logs`
--

INSERT INTO `tbl_logs` (`id`, `user`, `activity`, `timestamp`) VALUES
(1, 'clerk01', 'clerk01 logged in.', '2018-02-25 14:45:15'),
(2, 'clerk01', 'Inserted a reservation', '2018-02-25 14:52:51'),
(3, '', 'Attempted to delete a reservation', '2018-02-25 14:52:54'),
(4, 'clerk01', 'Inserted a reservation', '2018-02-25 14:54:45'),
(5, 'clerk01', 'Attempted to delete a reservation', '2018-02-25 14:54:47'),
(6, 'admin', 'You logged in to the system', '2018-02-25 15:33:18'),
(7, 'clerk01', 'clerk01 logged in.', '2018-02-26 09:39:30'),
(8, 'admin', 'You logged in to the system', '2018-02-26 09:39:46'),
(9, 'admin', 'You logged in to the system', '2018-02-26 11:45:46'),
(10, 'admin', 'You logged in to the system', '2018-02-26 12:10:00'),
(11, 'admin', 'You logged in to the system', '2018-02-26 14:06:58'),
(12, 'inv_admin', 'Logged in to the system', '2018-02-27 13:08:38'),
(13, 'inv_admin', 'Logged in to the system', '2018-02-27 13:09:05'),
(14, 'inv_f', 'Logged in to the system', '2018-02-27 13:52:21'),
(15, 'admin', 'You logged in to the system', '2018-02-27 14:52:29'),
(16, 'inv_admin', 'Logged in to the system', '2018-02-27 14:53:06'),
(17, 'inv_f', 'Logged in to the system', '2018-02-27 15:17:43'),
(18, 'inv_f', 'Logged in to the system', '2018-02-27 15:44:56'),
(19, 'inv_admin', 'Logged in to the system', '2018-02-27 15:56:48'),
(20, 'inv_f', 'Logged in to the system', '2018-02-27 16:05:40'),
(21, 'inv_f', 'Logged in to the system', '2018-02-27 16:05:45'),
(22, 'inv_f', 'Logged in to the system', '2018-02-27 16:06:11'),
(23, 'inv_u', 'Logged in to the system', '2018-02-27 16:37:28'),
(24, 'inv_f', 'Logged in to the system', '2018-02-27 16:37:49'),
(25, 'inv_u', 'Logged in to the system', '2018-02-27 16:38:54'),
(26, 'inv_admin', 'Logged in to the system', '2018-02-27 16:45:21'),
(27, 'inv_admin', 'Logged in to the system', '2018-02-28 10:40:42'),
(28, 'inv_f', 'Logged in to the system', '2018-02-28 10:58:45'),
(29, 'inv_f', 'Logged in to the system', '2018-02-28 11:08:55'),
(30, 'inv_u', 'Logged in to the system', '2018-02-28 13:47:40'),
(31, 'inv_f', 'Logged in to the system', '2018-02-28 14:09:14'),
(32, 'inv_admin', 'Logged in to the system', '2018-02-28 14:09:54'),
(33, 'inv_f', 'Logged in to the system', '2018-02-28 14:34:17'),
(34, 'inv_f', 'Logged in to the system', '2018-02-28 14:36:11'),
(35, 'inv_f', 'Logged in to the system', '2018-02-28 14:51:33'),
(36, 'inv_f', 'Logged in to the system', '2018-02-28 15:13:00'),
(37, 'inv_f', 'Logged in to the system', '2018-02-28 15:19:02'),
(38, 'inv_f', 'Logged in to the system', '2018-02-28 15:20:50'),
(39, 'inv_f', 'Logged in to the system', '2018-02-28 15:21:04'),
(40, 'inv_u', 'Logged in to the system', '2018-02-28 16:24:49'),
(41, 'inv_admin', 'Logged in to the system', '2018-02-28 16:27:00'),
(42, 'inv_f', 'Logged in to the system', '2018-03-01 13:51:19'),
(43, 'inv_admin', 'Logged in to the system', '2018-03-01 14:14:14'),
(44, 'inv_f', 'Logged in to the system', '2018-03-01 14:40:14'),
(45, 'inv_admin', 'Logged in to the system', '2018-03-01 14:56:27'),
(46, 'inv_f', 'Logged in to the system', '2018-03-01 15:37:42'),
(47, 'inv_admin', 'Logged in to the system', '2018-03-01 15:42:23'),
(48, 'inv_f', 'Logged in to the system', '2018-03-01 16:02:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservations`
--

CREATE TABLE IF NOT EXISTS `tbl_reservations` (
  `id` int(11) NOT NULL,
  `room_name` varchar(60) NOT NULL,
  `checkin_time` varchar(20) NOT NULL,
  `checkin_date` varchar(60) NOT NULL,
  `checkout_time` varchar(20) NOT NULL,
  `checkout_date` varchar(60) NOT NULL,
  `guest_name` varchar(60) NOT NULL,
  `guest_address` varchar(60) NOT NULL,
  `guest_id_type` varchar(60) NOT NULL,
  `guest_id_number` varchar(60) NOT NULL,
  `guest_phone` varchar(20) NOT NULL,
  `reservation_status` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_reservations`
--

INSERT INTO `tbl_reservations` (`id`, `room_name`, `checkin_time`, `checkin_date`, `checkout_time`, `checkout_date`, `guest_name`, `guest_address`, `guest_id_type`, `guest_id_number`, `guest_phone`, `reservation_status`) VALUES
(1, 'ROOM 200', '07:40 PM', '02/26/2018', '07:40 PM', '02/27/2018', 'John Santos', 'Mangaldan, Pangasinan', 'Drivers License', '09543248', '09506852211', 'done'),
(2, 'ROOM 205', '10:50 PM', '02/26/2018', '10:50 PM', '02/28/2018', 'Kokey', 'Manila', 'Drivers License', '232323', '09635554444', 'trash'),
(3, 'ROOM 104', '10:50 PM', '02/27/2018', '10:50 PM', '03/02/2018', 'Mario Quinto', 'Manila', 'Passport', '3232323', '09506852211', 'trash');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_addons`
--

CREATE TABLE IF NOT EXISTS `tbl_room_addons` (
  `id` int(11) NOT NULL,
  `ao_name` varchar(60) NOT NULL,
  `ao_cost` varchar(60) NOT NULL,
  `stock` varchar(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_addons`
--

INSERT INTO `tbl_room_addons` (`id`, `ao_name`, `ao_cost`, `stock`) VALUES
(1, 'Pillow', '200', ''),
(2, 'Bed Sheet', '500', ''),
(3, 'Blanket', '600', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_category`
--

CREATE TABLE IF NOT EXISTS `tbl_room_category` (
  `id` int(11) NOT NULL,
  `room_category` varchar(60) NOT NULL,
  `rate` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_category`
--

INSERT INTO `tbl_room_category` (`id`, `room_category`, `rate`) VALUES
(3, 'Single', '1500'),
(7, 'Elite', '2500'),
(9, 'Economical', '1300'),
(10, 'Budget', '50000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_list`
--

CREATE TABLE IF NOT EXISTS `tbl_room_list` (
  `id` int(11) NOT NULL,
  `floor_position` varchar(10) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `room_type` varchar(60) NOT NULL,
  `rate` varchar(60) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_list`
--

INSERT INTO `tbl_room_list` (`id`, `floor_position`, `room_name`, `room_type`, `rate`, `status`) VALUES
(1, '1', 'Room 100', 'Single', '1500', 'AVAILABLE'),
(2, '1', 'ROOM 101', 'Single', '1500', 'OCCUPIED'),
(3, '2', 'ROOM 250', 'Economical', '1300', 'AVAILABLE'),
(4, '1', 'ROOM 102', 'Single', '1500', 'AVAILABLE'),
(5, '1', 'ROOM 103', 'Single', '1500', 'AVAILABLE'),
(6, '1', 'ROOM 104', 'Elite', '2500', 'AVAILABLE'),
(7, '2', 'ROOM 200', 'Economical', '1300', 'OCCUPIED'),
(8, '2', 'ROOM 201', 'Elite', '2500', 'AVAILABLE'),
(9, '2', 'ROOM 203', 'Elite', '2500', 'AVAILABLE'),
(10, '2', 'ROOM 205', 'Budget', '50000', 'AVAILABLE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(11) NOT NULL,
  `minimum_hour` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `minimum_hour`) VALUES
(9999, '12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_number`
--

CREATE TABLE IF NOT EXISTS `tbl_transaction_number` (
  `id` int(11) NOT NULL,
  `t_number` text NOT NULL,
  `status` varchar(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction_number`
--

INSERT INTO `tbl_transaction_number` (`id`, `t_number`, `status`) VALUES
(3, 'HTL0000003', ''),
(4, 'HTL0000004', ''),
(5, 'HTL0000005', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(200) NOT NULL,
  `password_temp` varchar(60) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `email` text NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `password_temp`, `first_name`, `last_name`, `email`, `paths`, `userlevel`) VALUES
(1, 'clerk01', 'aafae9b562154fa444a91de51d0f8053', 'clerk01', 'Pedro', 'Penduko', 'pedropenduko@gmail.com', '', 'CLERK'),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'Rico', 'Tree', 'ricopuno@gmail.com', '', 'MANAGER'),
(3, 'inv_admin', '3bc21d1e9fb16632d02fb9e78cfa7f43', 'inv_admin', 'Jose', 'Rizal', '', '', 'INVENTORY_ADMIN'),
(4, 'inv_f', '17c76b78a9c7d95e711dbf2b481f4934', 'inv_f', 'Apolinario', 'Mabini', '', '', 'INVENTORY_FOOD'),
(5, 'inv_u', 'e45c73fdb69a63c0621a2aa133e15ea0', 'inv_u', 'Lapu', 'Lapu', '', '', 'INVENTORY_UTILITIES');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ims_stock_history`
--
ALTER TABLE `ims_stock_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_tbl_category`
--
ALTER TABLE `ims_tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_tbl_stocks`
--
ALTER TABLE `ims_tbl_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `tbl_activities_addons`
--
ALTER TABLE `tbl_activities_addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activities_addons_temp`
--
ALTER TABLE `tbl_activities_addons_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activities_history`
--
ALTER TABLE `tbl_activities_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activities_unbilled_charges`
--
ALTER TABLE `tbl_activities_unbilled_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_guests`
--
ALTER TABLE `tbl_guests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_addons`
--
ALTER TABLE `tbl_room_addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_category`
--
ALTER TABLE `tbl_room_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_list`
--
ALTER TABLE `tbl_room_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaction_number`
--
ALTER TABLE `tbl_transaction_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ims_stock_history`
--
ALTER TABLE `ims_stock_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ims_tbl_category`
--
ALTER TABLE `ims_tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ims_tbl_stocks`
--
ALTER TABLE `ims_tbl_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_activities_addons`
--
ALTER TABLE `tbl_activities_addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_activities_addons_temp`
--
ALTER TABLE `tbl_activities_addons_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_activities_history`
--
ALTER TABLE `tbl_activities_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_activities_unbilled_charges`
--
ALTER TABLE `tbl_activities_unbilled_charges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_guests`
--
ALTER TABLE `tbl_guests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_room_addons`
--
ALTER TABLE `tbl_room_addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_room_category`
--
ALTER TABLE `tbl_room_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_room_list`
--
ALTER TABLE `tbl_room_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_transaction_number`
--
ALTER TABLE `tbl_transaction_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
