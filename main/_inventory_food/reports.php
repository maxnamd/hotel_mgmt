<?php
include "includes/header.php";
?>


<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 1em">
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title"> <i class="fa fa-list"></i> Acquisition History</h3>
                        <div class="col-sm-12">
                            <table class="table color-bordered-table dark-bordered-table" id="tbl_report">
                                <thead>
                                <tr>
                                    <th>ACQUISITION TYPE</th>
                                    <th>Stock</th>
                                    <th>QUANTITY</th>
                                    <th>DATE</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php include "../phpfunctions/ims_stocks_history_alpha.php"?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>



<?php
include "includes/scripts.php";
?>

<script>
    $('#tbl_report').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        pageLength: '15',
    });
</script>


</body>

</html>
