<?php
include "includes/header.php";
?>
<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 1em">
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title"> Dashboard</h3>
                        <?php
                        include "../phpfunctions/ims_dashboard_main.php";
                        ?>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

<?php
include "includes/scripts.php";
?>

</body>

</html>
