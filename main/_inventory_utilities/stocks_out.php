<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/28/2018
 * Time: 7:11 PM
 */

include "includes/header.php";

include "../phpfunctions/connect.php";
$query_fetch_stock_cat = "SELECT * FROM ims_tbl_category WHERE category_user = '$userlevel'";
$stmt_fetch_stock_cat = $DBcon->prepare( $query_fetch_stock_cat );
$stmt_fetch_stock_cat->execute();
while($row_stc=$stmt_fetch_stock_cat->fetch(PDO::FETCH_ASSOC)) {
    extract($row_stc);
    $what_cat = $row_stc["name"];
}
?>

<script>
    function getQuantityLeft(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ims_stocks_out_ajax.php",
            data:'stock_name='+val,
            success: function(data){
                $('#quantity_left').val(data);
            }
        });
    }
    /////////////////////////////////////////////////////
    $(document).ready(function(){
        $('#quantity').keyup(function(){

            var quantity = parseInt(document.getElementById('quantity').value);
            var quantity_left = parseInt(document.getElementById('quantity_left').value);
            if ( quantity_left <  quantity || quantity <= 0)
            {
                document.getElementById('warning-quantity').innerHTML = "Check the quantity! Quantity may exceeded the left quantity of the selected stock or the entered quantity is 0";
                $("#btnRelease").attr("disabled", "disabled");
            }
            else if (quantity == "" || quantity === ""){
                document.getElementById('warning-quantity').innerHTML = "CHECK THE QUANTITY! THE DESIRED QUANTITY IS EMPTY";
                $("#btnRelease").attr("disabled", "disabled");
            }
            else
            {
                document.getElementById('warning-quantity').innerHTML = "";
                $(":submit").removeAttr("disabled");
            }
        })

    });
</script>
<div id="wrapper">
    <input type="hidden" id="quantity_left"/>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 1em">

            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title"> Release Stocks</h3>
                        <div class="row white-box">
                            <!-- .left side -->
                            <div class="col-sm-6">
                                <h3 class="box-title"> <i class="fa fa-bed"></i> Available Stocks</h3>

                                <div class="tbl-div">
                                    <table id="tbl_rooms" class="table display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="col-sm-5">Stock Name</th>
                                            <th class="col-sm-3">Category</th>
                                            <th class="col-sm-3">Supply</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php include "../phpfunctions/ims_stocks_out_function.php";?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- .right side -->
                            <div class="col-sm-6">

                                <h3 class="box-title"> <i class="fa fa-plus"></i> Release Stock <a href="stocks.php" class="pull-right fcbtn btn btn-outline btn-success btn-1f" style="color: black !important;" >ADD NEW</a></h3>
                                <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-sm-12">Stock:</label>
                                                <div class="col-sm-12">
                                                    <select class="form-control" name="stock_name" id="stock_name" data-error="PLEASE SELECT STOCK" required onChange="getQuantityLeft(this.value);" >
                                                        <option value="">--SELECT STOCK--</option>
                                                        <?php
                                                        $query_stock = "SELECT * FROM ims_tbl_stocks WHERE category = '$what_cat'";
                                                        $stmt_stock = $DBcon->prepare( $query_stock );
                                                        $stmt_stock->execute();

                                                        if($stmt_stock->rowCount() > 0) {
                                                            while ($row_stock = $stmt_stock->fetch(PDO::FETCH_ASSOC)) {
                                                                extract($row_stock);
                                                                ?>
                                                                <option value="<?php echo $row_stock['name']; ?>"><?php echo $row_stock['name']; ?> (<?php echo $row_stock['quantity'] . ' left'; ?>)</option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-sm-12">Quantity:</label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" name="quantity" id="quantity" data-error="PLEASE ENTER A STOCK QUANTITY" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="col-sm-12">Requested By:</label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" name="requested_by" data-error="FILL-UP THIS" required/>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="col-sm-12">Purpose:</label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" name="purpose" data-error="INSERT THE PURPOSE">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <span class="text-red-imp" id="warning-quantity"></span>
                                        </div>
                                        <div class="col-sm-6 pull-right">
                                            <button class="btn btn-success pull-right" type="submit" name="btnRelease" id="btnRelease">RELEASE</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <!-- /.right side -->

                            <div class="col-sm-12">
                                <h3 class="box-title"> <i class="fa fa-list"></i> Stock History</h3>
                                <table class="table color-bordered-table dark-bordered-table">
                                    <thead>
                                    <tr>
                                        <th>ACQUISITION TYPE</th>
                                        <th>Stock</th>
                                        <th>QUANTITY</th>
                                        <th>DATE</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php include "../phpfunctions/ims_stocks_history_alpha.php"?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .row -->


        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

<?php
include "includes/scripts.php";
?>

</body>

</html>

