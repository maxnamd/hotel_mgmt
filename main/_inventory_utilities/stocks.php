<?php
include "includes/header.php";

include "../phpfunctions/connect.php";
$query_fetch_stock_cat = "SELECT * FROM ims_tbl_category WHERE category_user = '$userlevel'";
$stmt_fetch_stock_cat = $DBcon->prepare( $query_fetch_stock_cat );
$stmt_fetch_stock_cat->execute();
while($row_stc=$stmt_fetch_stock_cat->fetch(PDO::FETCH_ASSOC)) {
    extract($row_stc);
    $what_cat = $row_stc["name"];
}
?>
<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <!-- .page title -->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard Page</h4>
                </div>
                <!-- /.page title -->
                <!-- .breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Home</li>
                    </ol>
                </div>
                <!-- /.breadcrumb -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title"> Stocks</h3>
                        <?php
                        include "../phpfunctions/ims_stocks_utilities.php";
                        ?>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

<?php
include "includes/scripts.php";
?>

</body>

</html>
