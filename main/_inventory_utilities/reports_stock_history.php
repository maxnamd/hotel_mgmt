<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 3/1/2018
 * Time: 12:16 AM
 */


include "includes/header.php";
include "../phpfunctions/connect.php";
$stockID= $_GET["stockID"];


$query_fetch_selected = "SELECT * FROM ims_tbl_stocks WHERE id = '$stockID'";
$stmt_selected = $DBcon->prepare( $query_fetch_selected );
$stmt_selected->execute();
$result = $stmt_selected -> fetch();

$selected_stock_name = $result ["name"];
?>
<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 1em">
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title"> Transaction History of <span class="text-danger"> <?php echo $selected_stock_name; ?> </span></h3>
                        <?php
                        include "../phpfunctions/ims_reports.php";
                        ?>

                        <table class="table">
                            <thead>
                            <tr>
                                <th>ACQUISITION TYPE</th>
                                <th>Stock</th>
                                <th>Quantity</th>
                                <th>Supplier</th>
                                <th>Requested By</th>
                                <th>DATE</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            $query_history = "SELECT * FROM ims_stock_history WHERE stock_name = '$selected_stock_name' ORDER BY timestamp DESC";
                            $stmt_history = $DBcon->prepare( $query_history );
                            $stmt_history->execute();

                            if($stmt_history->rowCount() > 0) {

                                while($row_history=$stmt_history->fetch(PDO::FETCH_ASSOC)) {
                                    extract($row_history);
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            if($row_history["acquisition_type"]=="IN"){
                                                echo "<span class=\"label label-table label-success\">IN</span>";
                                            }
                                            elseif ($row_history["acquisition_type"]=="OUT"){
                                                echo "<span class=\"label label-table label-inverse\">OUT</span>";
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $row_history["stock_name"]; ?></td>
                                        <td><?php echo $row_history["quantity"]; ?></td>
                                        <td>
                                            <?php
                                            if($row_history["acquisition_type"]=="IN"){
                                                echo $row_history["supplier"];
                                            }
                                            else{
                                                ECHO "N/A";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($row_history["acquisition_type"]=="OUT"){
                                                echo $row_history["care_of"];
                                            }
                                            else{
                                                ECHO "N/A";
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $row_history["timestamp"]; ?></td>

                                    </tr>
                                    <?php
                                }
                            }
                            else{
                                echo "<tr><td colspan='4' class='text-center font-bold'>NO DATA AVAILABLE</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

<?php
include "includes/scripts.php";
?>

</body>

</html>

