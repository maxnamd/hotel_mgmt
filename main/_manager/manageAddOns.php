    <?php
    include "includes/header.php";
    ?>
    <?php
    include "includes/scripts.php";
    ?>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Manage</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">Manage Add-Ons</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>


                <!-- .row Manage AddOns-->
                <div class="row white-box">
                    <!-- .left side -->
                    <div class="col-sm-6 table-responsive">
                        <h3 class="box-title"> <i class="fa fa-bed"></i> Room Add-Ons </h3>
                        <table id="tbl_room_addons" class="table display nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="col-sm-6">Add-On Name</th>
                                <th class="col-sm-3">Add-On Cost</th>
                                <th class="col-sm-3">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php include "../phpfunctions/read_room_addons.php";?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.left side -->

                    <!-- .right side -->
                    <div class="col-sm-6">

                        <h3 class="box-title">
                            <i class="fa fa-plus"></i> Add Add-Ons

                        </h3>
                        <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12">Add-On Name:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="ao_name" data-error="INSERT ADD-ON NAME" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12">Add-On Cost:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="ao_cost" type="number" data-error="INSERT ADD-ON COST" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success pull-right" type="submit" name="btnAdd_ao">ADD</button>
                                </div>
                            </div>


                        </form>
                    </div>
                    <!-- /.right side -->

                </div>
                <!-- /.row Manage AddOns-->

            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <script>
        $('#tbl_room_addons').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            pageLength: '10'
        });
    </script>

    <script>
        $(document).ready(function(){

            $(document).on('click', '#delete_addons', function(e){
                var aoID = $(this).data('id');
                SwalDelete(aoID);
                e.preventDefault();
            });

        });


        function SwalDelete(aoID){
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,

                preConfirm: function() {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url: '../phpfunctions/delete_room_addons.php',
                            type: 'POST',
                            data: 'delete='+aoID,
                            dataType: 'json'
                        })
                            .done(function(response){
                                swal('Deleted!', response.message, response.status);
                                window.setTimeout(function(){
                                    window.location.href = "manageAddOns.php";
                                }, 2000);
                            })
                            .fail(function(){
                                swal('Oops...', 'Something went wrong with ajax !', 'error');
                            });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>


</body>

</html>

<?php
include "../phpfunctions/create_room_addons.php";
?>
