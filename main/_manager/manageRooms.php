
    <?php
    include "includes/header.php";
    include "includes/scripts.php";
    ?>

    <script>
        function getRoomRate(val) {
            $.ajax({
                type: "POST",
                url: "../phpfunctions/ajax_getRoomRate.php",
                data:'room_category='+val,
                success: function(data){
                    $('#room_rate').val(data);
                }
            });
        }
    </script>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Manage</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">Manage Rooms</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>



                <div class="row white-box">
                    <!-- .left side -->
                    <div class="col-sm-6">
                        <h3 class="box-title"> <i class="fa fa-bed"></i> Rooms</h3>
                        <table id="tbl_rooms" class="table display nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="col-sm-2">Floor</th>
                                <th class="col-sm-3">Room Name</th>
                                <th class="col-sm-3">Room Type</th>
                                <th class="col-sm-2">Rate</th>
                                <th class="col-sm-3">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php include "../phpfunctions/read_rooms.php";?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.left side -->

                    <!-- .right side -->
                    <div class="col-sm-6">

                        <h3 class="box-title"> <i class="fa fa-plus"></i> Add Rooms</h3>
                        <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12">Room Floor:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" data-error="INSERT ROOM NUMBER" name="floor_position" required>
                                                <option value="">--SELECT ROOM FLOOR--</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12">Room Name/Number:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="room_name" data-error="INSERT ROOM NUMBER" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12">Room Type:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="room_category" id="room_category" onChange="getRoomRate(this.value);" data-error="PLEASE CHOOSE A ROOM TYPE" required>
                                                <option value="">--SELECT ROOM TYPE--</option>
                                                <?php

                                                $DBhost = "localhost";
                                                $DBuser = "root";
                                                $DBpass = "";
                                                $DBname = "hotel_db";

                                                try {
                                                    $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname",$DBuser,$DBpass);
                                                    $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                } catch(PDOException $ex){
                                                    die($ex->getMessage());
                                                }
                                                $query = "SELECT * FROM tbl_room_category";
                                                $stmt = $DBcon->prepare( $query );
                                                $stmt->execute();

                                                if($stmt->rowCount() > 0) {

                                                    $i = 0;
                                                    while ($row_cat = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                                        $my_id = $row_cat["id"];
                                                        extract($row_cat);
                                                        ?>
                                                        <option value="<?php echo $row_cat['room_category']; ?>"><?php echo $row_cat['room_category']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12">Room Rate:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="room_rate" id="room_rate" readonly data-error="PLEASE CHOOSE A ROOM TYPE" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success pull-right" type="submit" name="btnAddRoom">ADD</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.right side -->
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>




    <script>
        $('#tbl_rooms').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            pageLength: '10',
        });
    </script>

    <script>
        $(document).ready(function(){

            $(document).on('click', '#delete_room', function(e){
                var roomId = $(this).data('id');
                SwalDelete(roomId);
                e.preventDefault();
            });

        });


        function SwalDelete(roomId){
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,

                preConfirm: function() {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url: '../phpfunctions/delete_room.php',
                            type: 'POST',
                            data: 'delete='+roomId,
                            dataType: 'json'
                        })
                        .done(function(response){
                            swal('Deleted!', response.message, response.status);
                            window.setTimeout(function(){
                                window.location.href = "manageRooms.php";
                            }, 2000);
                        })
                        .fail(function(){
                            swal('Oops...', 'Something went wrong with ajax !', 'error');
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
</body>

</html>

<?php

    include "../phpfunctions/create_room.php";
?>
