<?php
include "includes/header.php";
?>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" style="margin-bottom: 1em">
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Reports</h3>
                            <h5 class="text-danger font-bold">Click each row to see the breakdown</h5>
                            <label class="form-inline">Show
                                <select id="demo-show-entries" class="form-control input-sm">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                </select>
                                entries </label>

                            <table id="demo-foo-addrow" class="table toggle-circle color-bordered-table dark-bordered-table">
                            <thead>
                                <tr>
                                    <th data-toggle="true">Transaction No.</th>
                                    <th> Guest Name </th>
                                    <th> Room Name </th>
                                    <th> Check-in Date </th>
                                    <th> Check-out Date </th>
                                    <th> Total Guest Cash </th>
                                    <th> Total Payable </th>
                                    <th> Payment </th>
                                    <th> Assisted By </th>
                                    <th data-hide="all"> Guest Cash Breakdown </th>
                                    <th data-hide="all"> Guest Total Payable Breakdown </th>
                                    <th data-hide="all"> Room Charges Breakdown</th>
                                    <th data-hide="all"> Number of Days </th>
                                    <th data-hide="all"> Add-Ons </th>
                                    <th data-hide="all"> Extra Charges </th>
                                    <th data-hide="all"> Change </th>
                                    <th data-hide="all"> Discount </th>
                                </tr>
                            </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">
                                        <div class="col-sm-6 text-right m-b-20">
                                            <div class="form-group">
                                                <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="on">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <tbody>
                                <?php include '../phpfunctions/_manager_report_read_all.php'; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="16">
                                    <div class="text-right">
                                        <ul class="pagination pagination-split m-t-30">
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <?php
    include "includes/scripts.php";
    ?>

</body>

</html>
