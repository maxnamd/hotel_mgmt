<?php
include "includes/header.php";
?>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" style="margin-bottom: 1em">
                </div>



                <div class="row white-box">
                    <!-- .left side -->
                    <div class="col-sm-6">
                        <h3 class="box-title"> <i class="fa fa-guest"></i> Users</h3>
                        <table id="tbl_users" class="table display nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="col-sm-1">Username</th>
                                <th class="col-sm-2">Full Name</th>
                                <th class="col-sm-2">Email</th>
                                <th class="col-sm-1">User Type</th>
                                <th class="col-sm-2">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php include "../phpfunctions/read_users.php";?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.left side -->

                    <!-- .right side -->
                    <div class="col-sm-6">

                        <h3 class="box-title"> <i class="fa fa-plus"></i> Add Rooms</h3>
                        <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">

                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">User Name</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="username" data-error="USERNAME REQUIRED" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">Password</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="password" data-error="PASSWORD REQUIRED" required type="password">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">User Type:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" data-error="SELECT USER TYPE" name="userlevel" required>
                                                <option value="">--SELECT User Type--</option>
                                                <option value="CLERK">FRONT DESK CLERK</option>
                                                <option value="INVENTORY_ADMIN">INVENTORY ADMIN</option>
                                                <option value="INVENTORY_FOOD">INVENTORY FOOD CLERK</option>
                                                <option value="INVENTORY_UTILITIES">INVENTORY UTILITIES CLERK</option>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">First Name</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="first_name" data-error="FIRST NAME REQUIRED" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">Last Name</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="last_name" data-error="LAST NAME REQUIRED" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">Email</label>
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control" name="email" data-error="EMAIL REQUIRED" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success pull-right" type="submit" name="btnInsert">ADD USER</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.right side -->
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <?php
    include "includes/scripts.php";
    include "../phpfunctions/create_user.php";
    ?>


    <script>
        $('#tbl_users').DataTable({
            dom: 'Bfrtip',
            buttons: [
                /*'copy', 'csv', 'excel', 'pdf', 'print'*/
            ],
            pageLength: '15',
        });
    </script>
</body>

</html>
