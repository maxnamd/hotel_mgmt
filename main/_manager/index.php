<?php
include "includes/header.php";
include "../phpfunctions/connect.php";





$guest_query = "SELECT * FROM tbl_activities GROUP BY guest_name";
$stmt_guest  = $DBcon->prepare( $guest_query );
$stmt_guest->execute();
$count_guest = $stmt_guest->rowCount();


$checkin_query = "SELECT * FROM tbl_activities";
$stmt_checkin  = $DBcon->prepare( $checkin_query );
$stmt_checkin->execute();
$count_checkin = $stmt_checkin->rowCount();

$roomlist_query = "SELECT * FROM tbl_room_list";
$stmt_roomlist  = $DBcon->prepare( $roomlist_query );
$stmt_roomlist->execute();
$count_roomlist = $stmt_roomlist->rowCount();


$sumX = "SELECT SUM(guest_total_payable_w_discount) AS total_sumX FROM tbl_activities WHERE act_status = 'DONE'";
$stmt_sumX  = $DBcon->prepare( $sumX );
$stmt_sumX->execute();
$totalX = $stmt_sumX->fetch();

$sumY = "SELECT SUM(total_unbilled_charges) AS total_sumY FROM tbl_activities_unbilled_charges WHERE paid_status = 'YES'";
$stmt_sumY  = $DBcon->prepare( $sumY );
$stmt_sumY->execute();
$totalY = $stmt_sumY->fetch();


$totalXY = $totalX["total_sumX"] + $totalY["total_sumY"];
?>

    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" style="margin-bottom: 1em;">
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"> <i class="fa fa-dashboard"></i> Dashboard</h3>

                            <div class="row">
                                <div class="col-md-3 col-xs-12 col-sm-6">
                                    <div class="white-box text-center bg-purple">
                                        <h1 class="text-white counter"> <i class="icon-people"></i> <?php echo $count_guest?> </h1>
                                        <p class="text-white">Total Guest</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6">
                                    <div class="white-box text-center bg-info">
                                        <h1 class="text-white counter"> <i class="icon-loop"></i> <?php echo $count_checkin?> </h1>
                                        <p class="text-white">Total Check-In</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6">
                                    <div class="white-box text-center bg-primary">
                                        <h1 class="text-white counter"> <i class="fa fa-money"></i> <?php echo $totalXY ?> </h1>
                                        <p class="text-white">Total Sales in PHP</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6">
                                    <div class="white-box text-center bg-success">
                                        <h1 class="text-white counter"> <i class="fa fa-bed"></i> <?php echo $count_roomlist?> </h1>
                                        <p class="text-white">Total Rooms</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"> <i class="icon icon-book-open"></i> Logs</h3>


                            <div class="admintbl">
                                <table class="table borderless">
                                    <thead>
                                    <tr>
                                        <th>Activity</th>
                                        <th>Doer</th>
                                        <th>Time</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php include "../phpfunctions/_manager_user_logs_quick.php";?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <?php
    include "includes/scripts.php";
    ?>

</body>

</html>
