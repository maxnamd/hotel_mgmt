

    <?php
    include "includes/header.php";
    include "includes/scripts.php";

    //check the Minimum Hour


    $sql = "SELECT * FROM tbl_settings WHERE id = 9999";
    $result = $conn->query($sql);

    $min_hour = 0;
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $min_hour += $row["minimum_hour"];
        }
    }

    ?>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Manage</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">Manage Room Category</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>

                <!-- .row Manage Room Cat-->
                <div class="row white-box">
                        <!-- .left side -->
                        <div class="col-sm-6 table-responsive">
                            <h3 class="box-title"> <i class="fa fa-bed"></i> Room Categories </h3>
                            <table id="tbl_roomCat" class="table display nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="col-sm-6">Room Type</th>
                                    <th class="col-sm-3">Room Rate</th>
                                    <th class="col-sm-3">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php include "../phpfunctions/read_room_category.php";?>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.left side -->

                        <!-- .right side -->
                        <div class="col-sm-6">

                            <h3 class="box-title">
                                <?php
                                if($min_hour<=0){
                                ?>
                                    <i class="fa fa-plus"></i> Add Category <span class="text-danger pull-right">THE MINIMUM HOUR IS SET TO: <?php echo " " . $min_hour?> hour(s) <a href="#"><i class="text-danger fa fa-gear fa-lg"></i></a></span>
                                <?php }
                                else{
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <i class="fa fa-plus"></i> Add Category
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="text-success pull-right">THE MINIMUM HOUR IS SET TO: <?php echo " " .$min_hour?> hour(s) <a href="#"><i class="text-success fa fa-gear fa-lg"></i></a></span>

                                        </div>
                                    </div>

                                <?php } ?>
                            </h3>
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-12">Category Name:</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" name="room_category" data-error="INSERT CATEGORY NAME" required/>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-12">Rate:</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" name="room_rate" type="number" data-error="INSERT ROOM RATE" required/>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success pull-right" type="submit" name="btnAddCategory">ADD</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                        <!-- /.right side -->




                </div>
                <!-- /.row Manage Room Cat-->

            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>


    <script>
        $('#tbl_roomCat').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            pageLength: '10'
        });
    </script>

    <script>
        $(document).ready(function(){

            $(document).on('click', '#delete_roomcat', function(e){
                var roomCatId = $(this).data('id');
                SwalDelete(roomCatId);
                e.preventDefault();
            });

        });


        function SwalDelete(roomCatId){
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,

                preConfirm: function() {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url: '../phpfunctions/delete_room_category.php',
                            type: 'POST',
                            data: 'delete='+roomCatId,
                            dataType: 'json'
                        })
                        .done(function(response){
                            swal('Deleted!', response.message, response.status);
                            window.setTimeout(function(){
                                window.location.href = "manageRoomCategory.php";
                            }, 2000);
                        })
                        .fail(function(){
                            swal('Oops...', 'Something went wrong with ajax !', 'error');
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
</body>

</html>

<?php
    include "../phpfunctions/create_room_category.php";
?>
