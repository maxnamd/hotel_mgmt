<?php
include "includes/header.php";
?>
<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">

            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box" id="div_tbl_rm">
                        <h3 class="box-title"> <i class="fa fa-bed"> </i> Room Management</h3>

                        <table id="tbl_rm" class="table color-bordered-table dark-bordered-table">
                            <thead class="">
                                <tr>
                                    <th class="col-sm-2">Room</th>
                                    <th class="col-sm-2">Rate</th>
                                    <th class="col-sm-2">Status</th>
                                    <th class="col-sm-2">Action</th>
                                </tr>
                            </thead>
                            <tbody id="test">
                                <?php include "../phpfunctions/_frontdesk_room_management.php";?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

<?php
include "includes/scripts.php";
?>
<script>
    $('#tbl_rm').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        pageLength: '10',
    });
</script>
</body>

</html>