<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 5:54 PM
 */

include "includes/header.php";
include "../phpfunctions/_frontdesk_reservation_fetchclicked.php";
?>

<script>
    setInterval(function(){ getUsers(); }, 1000);

    function getUsers()
    {
        $.ajax({
            url: '../phpfunctions/ajax_refresh_tnumber.php',
            type: 'post',
            success: function(data) {
                $('#last_num').val(data);
            }
        });
    }
</script>

<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <!-- .page title -->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard Page</h4>
                </div>
                <!-- /.page title -->
                <!-- .breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Home</li>
                    </ol>
                </div>
                <!-- /.breadcrumb -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-3">

                </div>

                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading"> Reservation Info
                        </div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-6">Guest Name:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $guest_name?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-6">Guest Contact:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $guest_phone?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-6">Address:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $guest_address?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-12">ID Type:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $guest_id_type?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-12">ID Number:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $guest_id_number?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-6">Selected Room:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $room_name?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-12">Check-in Date:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $checkin_date?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-12">Check-in Time:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $checkin_time?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-12">Check-out Date:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $checkout_date?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-12">Check-out Time:</label>
                                                <div class="col-sm-12">
                                                    <span class="form-control"><?php echo $checkout_time?></span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <!--
                                            $guest_name = $row_res["guest_name"];
                                            $guest_phone = $row_res["guest_phone"];
                                            $room_name = $row_res["room_name"];
                                            $checkin_date = $row_res["checkin_date"];
                                            $checkout_date = $row_res["checkout_date"];
                                            $guest_address = $row_res["guest_address"];
                                            $guest_id_type = $row_res["guest_id_type"];
                                            $guest_id_number = $row_res["guest_id_number"];
                                            -->
                                            <input type="hidden" id="last_num" name="last_num"/>
                                            <input type="hidden" name="temp_resID" value="<?php echo $temp_resID;?>"/>
                                            <input type="hidden" name="guest_name" value="<?php echo $guest_name;?>"/>
                                            <input type="hidden" name="guest_phone" value="<?php echo $guest_phone;?>"/>
                                            <input type="hidden" name="room_name" value="<?php echo $room_name;?>"/>
                                            <input type="hidden" name="checkin_date" value="<?php echo $checkin_date;?>"/>
                                            <input type="hidden" name="checkout_date" value="<?php echo $checkout_date;?>"/>
                                            <input type="hidden" name="checkin_time" value="<?php echo $checkin_time;?>"/>
                                            <input type="hidden" name="checkout_time" value="<?php echo $checkout_time;?>"/>
                                            <input type="hidden" name="guest_address" value="<?php echo $guest_address;?>"/>
                                            <input type="hidden" name="guest_id_type" value="<?php echo $guest_id_type;?>"/>
                                            <input type="hidden" name="guest_id_number" value="<?php echo $guest_id_number;?>"/>
                                            <button class="btn btn-success btn-md pull-right" name="btnReserveFinal">RESERVE</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">

                </div>
            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

<?php
include "includes/scripts.php";
?>

</body>
</html>
<?php
include "../phpfunctions/_frontdesk_reservation_insert.php"
?>