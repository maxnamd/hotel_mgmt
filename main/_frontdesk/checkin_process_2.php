<?php
date_default_timezone_set('Asia/Manila');
include "includes/header.php";
include "../phpfunctions/_frontdesk_get_selected_room.php";
include "includes/scripts.php";




$transaction_number = $_SESSION["transaction_number"];
?>
<style type="text/css">
    #mydatepicker
    {
        position: relative; z-index: 1000;
    }
</style>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard Page</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">Home</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-8">
                        <div class="white-box">
                            <h4 class="box-title"><i class="fa fa-group"></i> CHECK-OUT DATE</h4>
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">
                                <input type="hidden" name="guest_checkin_time" id="guest_checkin_time"/>
                                <input type="hidden" name="guest_checkin_date" id="guest_checkin_date"/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" value="" id="mydatepicker" name="guest_checkout_date">
                                            <div class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row" style="margin-top: 1em">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <button class="btn btn-success btn-md pull-right" name="btnProceed">PROCEED</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="box-title"> <i class="fa fa-info-circle"> </i> CHECK-IN INFO</h4>
                            </div>

                            <div class="panel-body">
                                <p class="vt">
                                    <h5 class="box-title font-bold">Room Information</h5>
                                    You have selected the <strong><?php echo $sr_name;?></strong> with a category of <strong><?php echo $sr_type;?></strong>, and the Rate per day is <strong><?php echo $rate?></strong>.
                                </p>

                                <p>
                                    <h5 class="box-title font-bold">Customer Information</h5>
                                    <ul>
                                        <li>Name: <b><?php echo $_SESSION["guest_name"];?></b></li>
                                        <li>Gender: <b><?php echo $_SESSION["guest_gender"];?></b></li>
                                        <li>Address: <b><?php echo $_SESSION["guest_address"];?></b></li>
                                        <li>ID Type: <b><?php echo $_SESSION["guest_id_type"];?></b></li>
                                        <li>ID Number: <b><?php echo $_SESSION["guest_id_number"];?></b></li>
                                        <li>Phone Number: <b><?php echo $_SESSION["guest_phone_number"];?></b></li>
                                    </ul>
                                </p>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>


    <script>
        $('#mydatepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom',
            format: 'mm/dd/yyyy',
            startDate: '+1d',
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var today = mm+'/'+ dd +'/'+yyyy;
        document.getElementById("guest_checkin_date").value = today;

        function showTime() {
            var timeNow = new Date();
            var hours   = timeNow.getHours();
            var minutes = timeNow.getMinutes();
            var timeString = "" + ((hours > 12) ? hours - 12 : hours);
            timeString  += ((minutes < 10) ? ":0" : ":") + minutes;
            timeString  += (hours >= 12) ? " PM" : " AM";
            document.getElementById("guest_checkin_time").value = timeString;
            timerID = setTimeout("showTime()", 1000);
        }
        showTime();
    </script>

</body>

</html>

<?php
if(isset($_POST["btnProceed"])){
    $_SESSION["guest_checkin_date"] = $_POST["guest_checkin_date"];
    $_SESSION["guest_checkin_time"] = $_POST["guest_checkin_time"];
    $_SESSION["guest_checkout_date"] = $_POST["guest_checkout_date"];
    $_SESSION["guest_checkout_time"] = $_POST["guest_checkin_time"];
    echo "<script>
     window.location.href = \"checkin_process_3.php\";
    </script>";
}

?>
