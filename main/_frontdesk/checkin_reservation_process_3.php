<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 10:03 PM
 */
date_default_timezone_set('Asia/Manila');
include "includes/header.php";
include "includes/scripts.php";



$transaction_number = $_SESSION["res_transaction_number"];
include "../phpfunctions/_frontdesk_reservation_get_guest_session_details.php";
?>
    <!--<script>
        function insertAddOn(val) {
            $.ajax({
                type: "POST",
                url: "../phpfunctions/_frontdesk_insert_addons.php",
                data:'selected_addon='+val,
                success: function(data){
                    $("#test").html(data);
                }
            });
        }
    </script>-->
    <style>
        .table-borderless>thead>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tbody>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>tfoot>tr>td {
            border: none;
        }
    </style>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard Page</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">Home</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="white-box">
                            <h4 class="box-title"><i class="fa fa-group"></i> HOTEL SYSTEM</h4>
                            <table class="table table-sm table-borderless ">
                                <tr>
                                    <td>No. of Days:</td>
                                    <td class="font-bold"> <?php echo $guest_av_days;?> Days</td>
                                </tr>
                                <tr>
                                    <td>Room Rate: </td>
                                    <td class="font-bold">PHP <?php echo $room_rate;?></td>
                                </tr>

                                <tr>
                                    <td>Room Total Charge: </td>
                                    <td class="font-bold text-danger">PHP <?php echo $room_charges;?></td>
                                </tr>
                                <tr>
                                    <td>Extra Charges(Add-Ons: </td>
                                    <td class="font-bold text-danger">PHP <?php echo $guest_total_addons;?></td>
                                </tr>
                                <tr>
                                    <td>Discount: </td>
                                    <td class="font-bold"><?php echo $guest_discount;?>%</td>
                                </tr>
                                <tr>
                                    <td class="font-bold"></td>
                                    <td class="font-bold"></td>
                                </tr>



                                <tr>
                                    <td class="font-bold">Total Payable: </td>
                                    <td class="font-bold">PHP <?php echo $plain_total_payable;?></td>
                                </tr>

                                <tr>
                                    <td class="font-bold">Total Payable with Discount: </td>
                                    <td class="font-bold text-danger">PHP <?php echo $guest_total_payable;?></td>
                                </tr>

                            </table>

                            <form method="post">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon3">CASH: </span>
                                    <input class="form-control" type="number" data-error="INSERT THE CUSTOMER'S CASH" name="guest_tendered" required/>
                                    <div class="help-block with-errors"></div>
                                    <button class="btn btn-md btn-success" type="submit" name="btnPay">PAY</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-5 white-box">
                        <div class="">
                            <h4 class="box-title"><i class="fa fa-group"></i> GUEST'S SELECTED ADD-ONS</h4>
                            <table class="table table-hovered">
                                <thead class="table-inverse">
                                <td>Name</td>
                                <td>Qty.</td>
                                <td>Cost</td>
                                <td>Total</td>
                                </thead>
                                <tbody>
                                <?php include "../phpfunctions/_frontdesk_reservation_fetch_inserted_addons_final.php";?>
                                </tbody>
                            </table>


                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="box-title"> <i class="fa fa-info-circle"> </i> CHECK-IN INFO</h4>
                            </div>

                            <div class="panel-body">
                                <p class="vt">
                                <h5 class="box-title font-bold">Room Information:</h5>
                                <?php
                                $sr_name = $_SESSION["res_room_name"];
                                $sr_type = $_SESSION["res_room_type"];
                                $sr_rate = $_SESSION["res_room_rate"];
                                $sr_floor = $_SESSION["res_floor_position"];
                                ?>
                                You have selected the <strong><?php echo $sr_name?></strong> with a category of <strong><?php echo $sr_type;?></strong>, and the Rate per day is <strong><?php echo $sr_rate;?></strong>.
                                </p>

                                <p>
                                <h5 class="box-title font-bold">Customer Information:</h5>
                                <ul>
                                    <li>Name: <b><?php echo $_SESSION["res_guest_name"];?></b></li>
                                    <li>Gender: <b><?php echo $_SESSION["res_guest_gender"];?></b></li>
                                    <li>Address: <b><?php echo $_SESSION["res_guest_address"];?></b></li>
                                    <li>ID Type: <b><?php echo $_SESSION["res_guest_id_type"];?></b></li>
                                    <li>ID Number: <b><?php echo $_SESSION["res_guest_id_number"];?></b></li>
                                    <li>Phone Number: <b><?php echo $_SESSION["res_guest_phone"];?></b></li>
                                </ul>
                                </p>

                                <p>
                                <h5 class="box-title font-bold">Expected Checkout Date:</h5>
                                Your guest's expected checkout date is: <span class="font-bold"><?php echo $_SESSION["res_checkout_date"];?></span>

                                </p>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    </body>

    </html>

<?php
include "../phpfunctions/_frontdesk_reservation_payment_recording.php";
?>