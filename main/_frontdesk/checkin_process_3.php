<?php
date_default_timezone_set('Asia/Manila');
include "includes/header.php";
include "includes/scripts.php";



$transaction_number = $_SESSION["transaction_number"];
include "../phpfunctions/_frontdesk_get_selected_room.php";
include "../phpfunctions/_frontdesk_insert_addons.php";
?>
    <style>
        #center {
            position:relative !important;
        }
        #inner_center {
            position:absolute;
            bottom:0;
            right: 0;
            margin-bottom: 1em;
            margin-right: 1em;
        }
    </style>

    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard Page</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">Home</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="white-box">
                            <h4 class="box-title"><i class="fa fa-group"></i> ADD-ONS</h4>


                            <table class="table">
                                <thead class="table-inverse">
                                    <tr>
                                        <td>Add-On</td>
                                        <td>Price</td>
                                        <td>ACTION</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php include "../phpfunctions/_frontdesk_get_addons.php";?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-5 white-box" id="center">
                        <div class="">
                            <h4 class="box-title"><i class="fa fa-group"></i> GUEST'S ADD-ONS</h4>
                            <table class="table table-hovered">
                                <thead class="table-inverse">
                                    <td>Name</td>
                                    <td>Qty.</td>
                                    <td>Cost</td>
                                    <td>Total</td>
                                    <td>Action</td>
                                </thead>
                                <tbody id="tbody">
                                <?php include "../phpfunctions/_frontdesk_fetch_inserted_addons.php";?>
                                </tbody>
                            </table>
                        </div>
                        <div class="align-bottom" id="inner_center"><button class="btn btn-md btn-success" onclick="final_process();">PROCEED</button></div>
                    </div>


                    <div class="col-md-3">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="box-title"> <i class="fa fa-info-circle"> </i> CHECK-IN INFO</h4>
                            </div>

                            <div class="panel-body">
                                <p class="vt">
                                    <h5 class="box-title font-bold">Room Information:</h5>
                                    You have selected the <strong><?php echo $sr_name;?></strong> with a category of <strong><?php echo $sr_type;?></strong>, and the Rate per day is <strong><?php echo $rate?></strong>.
                                </p>

                                <p>
                                    <h5 class="box-title font-bold">Customer Information:</h5>
                                    <ul>
                                        <li>Name: <b><?php echo $_SESSION["guest_name"];?></b></li>
                                        <li>Gender: <b><?php echo $_SESSION["guest_gender"];?></b></li>
                                        <li>Address: <b><?php echo $_SESSION["guest_address"];?></b></li>
                                        <li>ID Type: <b><?php echo $_SESSION["guest_id_type"];?></b></li>
                                        <li>ID Number: <b><?php echo $_SESSION["guest_id_number"];?></b></li>
                                        <li>Phone Number: <b><?php echo $_SESSION["guest_phone_number"];?></b></li>
                                    </ul>
                                </p>

                                <p>
                                    <h5 class="box-title font-bold">Expected Checkout Date:</h5>
                                    Your guest's expected checkout date is: <span class="font-bold"><?php echo $_SESSION["guest_checkout_date"];?></span>

                                </p>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <script type="text/javascript">
        function final_process() {
            window.location.href = "checkin_process_4.php";
        }
    </script>
</body>

</html>
