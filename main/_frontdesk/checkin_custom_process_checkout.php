<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/9/2018
 * Time: 9:25 PM
 */

error_reporting(0);
include "includes/header.php";
include "includes/scripts.php";
$transaction_id = $_SESSION["actID"];
include "../phpfunctions/_frontdesk_custom_process_fetch_transaction.php";
include "scripts/final_checkout.php";

?>

<style>
    .table-borderless>thead>tr>th,
    .table-borderless>thead>tr>td,
    .table-borderless>tbody>tr>th,
    .table-borderless>tbody>tr>td,
    .table-borderless>tfoot>tr>th,
    .table-borderless>tfoot>tr>td {
        border: none;
    }
</style>


<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <!-- .page title -->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard Page</h4>
                </div>
                <!-- /.page title -->
                <!-- .breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Home</li>
                    </ol>
                </div>
                <!-- /.breadcrumb -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-4">
                    <div class="white-box">
                        <h4 class="box-title"><i class="fa fa-group"></i> HOTEL SYSTEM</h4>
                        <table class="table table-sm table-borderless ">
                            <?php

                            if($_SESSION["od_days"]>1){
                                $total+=$_SESSION["total_overdue"];
                            }
                            ?>
                            <tr>
                                <td>No. of Days:</td>
                                <td class="font-bold"> <?php echo $no_days;?> Days</td>
                            </tr>
                            <tr>
                                <td>Room Rate: </td>
                                <td class="font-bold">PHP <?php echo $a_room_rate;?></td>
                            </tr>

                            <tr>
                                <td>Room Total Charge: </td>
                                <td class="font-bold text-danger">PHP <?php echo $rc;?></td>
                            </tr>
                            <tr>
                                <td>Extra Charges(Add-Ons): </td>
                                <td class="font-bold text-danger">PHP <?php echo $ac;?></td>
                            </tr>
                            <tr>
                                <td>Overdue: </td>
                                <td class="font-bold text-danger">PHP


                                    <?php
                                    if($_SESSION["od_days"]>1){
                                        $_SESSION["total_overdue"] . ' ('. $_SESSION["od_days"] . ' days) ';
                                    }
                                    ?>

                                </td>
                            </tr>
                            <tr>
                                <td>Discount: </td>
                                <td class="font-bold"><?php echo $guest_discount;?>%</td>
                            </tr>
                            <tr>
                                <td class="font-bold"></td>
                                <td class="font-bold"></td>
                            </tr>



                            <tr>
                                <td class="font-bold">Total Payable: </td>
                                <td class="font-bold">PHP <?php echo $total;?></td>
                            </tr>

                            <tr>
                                <td class="font-bold">Total Payable with Discount: </td>
                                <td class="font-bold text-danger">PHP <?php echo $total;?></td>
                                <input type="hidden" value="<?php echo $total;?>" id="total_payable"/>
                            </tr>

                        </table>

                        <form method="post">
                            <div id="alert_status" class="text-red-imp"></div>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon3">CASH: </span>
                                <input id="customerCash" class="form-control" type="number" data-error="INSERT THE CUSTOMER'S CASH" name="guest_tendered" required />
                                <div class="help-block with-errors"></div>
                                <button class="btn btn-md btn-success" type="submit" name="btnPay" id="btnPay">PAY</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-5 white-box">
                    <div class="">
                        <h4 class="box-title"><i class="fa fa-group"></i> GUEST'S SELECTED ADD-ONS</h4>
                        <table class="table table-hovered">
                            <thead class="table-inverse">
                            <td>Name</td>
                            <td>Qty.</td>
                            <td>Cost</td>
                            <td>Total</td>
                            <td>Action</td>
                            </thead>
                            <tbody>
                            <?php include "../phpfunctions/_frontdesk_for_custom_process_get_inserted_ao.php";?>
                            </tbody>
                        </table>


                    </div>
                </div>


                <div class="col-md-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="box-title"> <i class="fa fa-info-circle"> </i> CHECK-IN INFO</h4>
                        </div>

                        <div class="panel-body">
                            <h5 class="box-title font-bold">Room Information:</h5>

                            <p class="vt">
                            Bill for <strong><?php echo $a_room_name?></strong> (<strong><?php echo $a_room_type?></strong>) With the rate per day <strong><?php echo $a_room_rate?></strong>
                            </p>

                            <p>
                            <h5 class="box-title font-bold">Customer Information:</h5>
                            <ul>
                                <li>Name: <b><?php echo $guest_name?></b></li>
                                <li>Gender: <b><?php echo $guest_gender?></b></li>
                                <li>Address: <b><?php echo $guest_address?></b></li>
                                <li>ID Type: <b><?php echo $guest_id_type?></b></li>
                                <li>ID Number: <b><?php echo $guest_id_number?></b></li>
                                <li>Phone Number: <b><?php echo $guest_phone_number?></b></li>
                            </ul>
                            </p>

                            <p>
                            <h5 class="box-title font-bold">Expected Checkout Date:</h5>
                            The guest's expected checkout date is: <span class="font-bold"><?php echo $checkout_date_up;?></span>
                            </p>

                        </div>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

</body>

</html>
<?php
include "../phpfunctions/_frontdesk_custom_process_payment_recording.php";
?>
