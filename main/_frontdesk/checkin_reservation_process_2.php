<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 9:11 PM
 */

date_default_timezone_set('Asia/Manila');
include "includes/header.php";
include "includes/scripts.php";



$transaction_number = $_SESSION["res_transaction_number"];
include "../phpfunctions/_frontdesk_reservation_insert_addons.php";

include "../phpfunctions/connect.php";
$query_clean_1 = "DELETE FROM tbl_activities_addons_temp WHERE transaction_number NOT IN(SELECT t_number FROM tbl_transaction_number WHERE t_number IS NOT NULL)";
$stmt_clean_1 = $DBcon->prepare( $query_clean_1 );
$stmt_clean_1->execute();
?>
<style>
    #center {
        position:relative !important;
    }
    #inner_center {
        position:absolute;
        bottom:0;
        right: 0;
        margin-bottom: 1em;
        margin-right: 1em;
    }
</style>


<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <!-- .page title -->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard Page</h4>
                </div>
                <!-- /.page title -->
                <!-- .breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Home</li>
                    </ol>
                </div>
                <!-- /.breadcrumb -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-4">
                    <div class="white-box">
                        <h4 class="box-title"><i class="fa fa-group"></i> ADD-ONS</h4>


                        <table class="table">
                            <thead class="table-inverse">
                            <tr>
                                <td>Add-On</td>
                                <td>Price</td>
                                <td>ACTION</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php include "../phpfunctions/_frontdesk_get_addons.php";?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-5 white-box" id="center">
                    <div class="">
                        <h4 class="box-title"><i class="fa fa-group"></i> GUEST'S ADD-ONS</h4>
                        <table class="table table-hovered">
                            <thead class="table-inverse">
                            <td>Name</td>
                            <td>Qty.</td>
                            <td>Cost</td>
                            <td>Total</td>
                            <td>Action</td>
                            </thead>
                            <tbody id="tbody">
                            <?php include "../phpfunctions/_frontdesk_reservation_fetch_inserted_addons.php";?>
                            </tbody>
                        </table>
                    </div>
                    <div class="align-bottom" id="inner_center"><button class="btn btn-md btn-success" onclick="final_process();">PROCEED</button></div>
                </div>


                <div class="col-md-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="box-title"> <i class="fa fa-info-circle"> </i> CHECK-IN INFO</h4>
                        </div>

                        <div class="panel-body">
                            <p class="vt">
                            <h5 class="box-title font-bold">Room Information:</h5>
                            You have selected the <strong><?php echo $_SESSION["res_room_name"];?></strong> with a category of <strong><?php echo $_SESSION["res_room_type"];;?></strong>, and the Rate per day is <strong><?php echo $_SESSION["res_room_rate"];?></strong>.
                            </p>

                            <p>
                            <h5 class="box-title font-bold">Customer Information:</h5>
                            <ul>
                                <li>Name: <b><?php echo $_SESSION["res_guest_name"];?></b></li>
                                <li>Gender: <b><?php echo $_SESSION["res_guest_gender"];?></b></li>
                                <li>Address: <b><?php echo $_SESSION["res_guest_address"];?></b></li>
                                <li>ID Type: <b><?php echo $_SESSION["res_guest_id_type"];?></b></li>
                                <li>ID Number: <b><?php echo $_SESSION["res_guest_id_number"];?></b></li>
                                <li>Phone Number: <b><?php echo $_SESSION["res_guest_phone"];?></b></li>
                            </ul>
                            </p>

                            <p class="font-small">
                            <h6 class="box-title font-bold text-small">Check-in Date:</h6>
                            Your guest's expected checkout date is: <span class="font-bold font-small"><?php echo $_SESSION["res_checkin_date"];?> @ <?php echo $_SESSION["res_checkin_time"];?></span>
                            </p>

                            <p>
                            <h5 class="box-title font-bold">Expected Checkout Date:</h5>
                            Your guest's expected checkout date is: <span class="font-bold font-small"><?php echo $_SESSION["res_checkout_date"];?> @ <?php echo $_SESSION["res_checkout_time"];?></span>

                            </p>

                        </div>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

<script type="text/javascript">
    function final_process() {
        window.location.href = "checkin_reservation_process_3.php";
    }
</script>
</body>

</html>