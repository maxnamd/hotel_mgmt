<?php
include "includes/header.php";
?>
    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" style="margin-bottom: 1em">

                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="white-box">
                            <h3 class="box-title">ROOMS</h3>
                            <?php include "../phpfunctions/_frontdesk_get_rooms.php";?>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="col-md-12 white-box">
                            <h4 class="box-title"><i class="fa fa-bed"></i> Rooms</h4>
                            <table class="table table-hover">
                                <tr>
                                    <td>Total: </td>
                                    <td><?php echo $count_all;?></td>
                                </tr>
                                <tr>
                                    <td>Occupied: </td>
                                    <td><?php echo $count_nv;?></td>
                                </tr>
                                <tr>
                                    <td>Available: </td>
                                    <td><?php echo $count_av;?></td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-12 white-box">
                            <h4 class="box-title"><i class="fa fa-bed"></i> Quick View</h4>
                            <table class="table table-hover">
                                <?php include "../phpfunctions/_frontdesk_count_av_per_type.php";?>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="col-md-12 white-box">
                            <h4 class="box-title"><i class="fa fa-eye"></i> Guests</h4>
                            <table class="table table-hover">
                                <?php include "../phpfunctions/_frontdesk_get_genders.php";?>
                                <tr>
                                    <td>Male: </td>
                                    <td><?php echo $count_m;?></td>
                                </tr>
                                <tr>
                                    <td>Female: </td>
                                    <td><?php echo $count_f;?></td>
                                </tr>
                                <tr>
                                    <td>Total: </td>
                                    <td><?php echo $count_ag;?></td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-12 white-box">
                            <h4 class="box-title"><i class="fa fa-outdent"></i> Checkouts</h4>
                            <table class="table table-hover">
                                <tr>
                                    <td>Checking-out Today: </td>
                                    <td><?php echo $count_today_checkout;?></td>
                                </tr>
                                <tr>
                                    <td>Unspecified Checkouts: </td>
                                    <td><?php echo 0;?></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-2 white-box">
                       <h3 class="box-title"><i class="fa fa-list-alt"> </i> QUICK RESERVATION LIST</h3>
                        <table class="table table-hover">
                            <tbody>
                                <?php include "../phpfunctions/_frontdesk_quick_res_list.php"?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <?php
    include "includes/scripts.php";


    ?>

</body>

</html>
