<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/9/2018
 * Time: 10:34 PM
 */


session_start();
date_default_timezone_set('Asia/Manila');
error_reporting(0);



$transaction_id = $_SESSION["actID"];
include "../phpfunctions/_frontdesk_custom_process_fetch_transaction.php";
?>

<style>
    html *
    {
        font-size: 12 !important;
        color: #000 !important;
        font-family: sans-serif !important;
    }
    div.ex1 {
        width:250px;
        margin: auto;


    }

    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 3px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        -webkit-transition-duration: 0.4s; /* Safari */
        transition-duration: 0.4s;
        cursor: pointer;
    }

    .button1 {
        background-color: white;
        color: black;
        border: 2px solid #4CAF50;
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
    }

    .button2 {
        background-color: white;
        color: black;
        border: 2px solid #008CBA;
    }

    .button2:hover {
        background-color: #008CBA;
        color: white;
    }

    .button3 {
        background-color: white;
        color: black;
        border: 2px solid #f44336;
    }

    .button3:hover {
        background-color: #f44336;
        color: white;
    }

    .button4 {
        background-color: white;
        color: black;
        border: 2px solid #e7e7e7;
    }

    .button4:hover {background-color: #e7e7e7;}

    .button5 {
        background-color: white;
        color: black;
        border: 2px solid #555555;
    }

    .button5:hover {
        background-color: #555555;
        color: white;
    }
</style>

<div style="text-align: center;">
    <b></b><center><b>
            <img height="60" align="center" src="../../plugins/images/g.png"><br>
            Elite Admin Inc<br></b>
        Montemayor St., Malasiqui, Pangasinan<br>
        (075) 832-9000<br>
    </center>
    <center>------------------------------------------------------------- <br></center>
    <!--border-style: solid; to check the width below-->
    <center><b>GUEST CHARGES</b><br></center>
    <br>

    <center>
        <table class="table table-bordered">
            <tbody>

            <tr>
                <td><div style="width: 70px; ">#<?php echo $transaction_id?></div></td>
                <td><div style="width: 170px; text-align: right; "><?php echo $transaction_date_up?></div></td>

            </tr>
            </tbody>
        </table>
    </center>
    <div style="display: inline-block; text-align: left;">
        Cashier: <?php echo $clerk?><br>
        Mode of Payment: Cash <br>
        Terminal #NA<br>
        <center>------------------------------------------------------------- <br></center>
        Room: <?php echo $a_room_name . '/'. $a_room_type?><br>
        Arrival: <?php echo $checkin_date?> <br>
        Departure: <?php echo $checkout_date_up?> <br>
        No of Days: <?php echo $no_days?> <br>
        <center>------------------------------------------------------------- <br></center>

        Room Charges: <?php echo $rc?><br><br>
        Extra Details: <br><br>
        Overdue:
        <?php
        if($_SESSION["od_days"]>1){
            echo $_SESSION["total_overdue"] . ' ('. $_SESSION["od_days"] . ' days) ';
        }
        else{
            echo "No overdue yet";
        }
        ?>
    </div>

    <center>
        <table class="table table-bordered">
            <tbody>
            <?php
            include "../phpfunctions/connect.php";
            $query_ao = "SELECT * FROM tbl_activities_addons WHERE transaction_number = '$transaction_id'";
            $stmt_ao = $DBcon->prepare( $query_ao );
            $stmt_ao->execute();
            $count_ao = $stmt_ao->rowCount();

            if($stmt_ao->rowCount() > 0) {

                $i = 0;
                while ($row_ao = $stmt_ao->fetch(PDO::FETCH_ASSOC)) {
                    extract($row_ao);
                    ?>
                    <tr>
                        <td><div style="width: 100px;"><?php echo $row_ao["ao_name"];?></div></td>
                        <td><div style="width: 80px;"><?php echo $row_ao["ao_quantity"] . 'PC(s) @ '. $row_ao["ao_cost"];?></div></td>
                        <td><div style="width: 57px;"><?php echo $row_ao["ao_total"];?></div></td>
                    </tr>
                    <?php
                }
            }
            else{
                echo "<tr> No Add-Ons Included </tr>";
            }
            ?>
            </tbody>
        </table>
    </center>


    <br><center>**** <?php echo $count_ao?> Item(s) ****</center>


    <center>------------------------------------------------------------- <br></center>

    <center>
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td><div style="width: 151px; text-align: right;">Amount Due:</div></td>
                <td><div style="width: 70px; text-align: right;"> <?php echo $total;?></div></td>

            </tr>
            <tr>
                <td><div style="width: 151px; text-align: right;">Tendered:</div></td>
                <td><div style="width: 70px; text-align: right;"> <?php echo $guest_cash_up;?></div></td>

            </tr><tr>
                <td><div style="width: 151px; text-align: right;"><b>Change Due:</b></div></td>
                <td><div style="width: 70px; text-align: right;"> <b> <?php echo $guest_change_up;?></b></div></td>

            </tr>


            <tr>
                <td><div style="width: 151px; text-align: right;">Discount:</div></td>
                <td><div style="width: 69px; text-align: right;"> <?php echo $guest_discount;?></div></td>
            </tr>
            </tbody>
        </table>

    </center>

    <center>------------------------------------------------------------- <br></center>
    Guest: <?php echo $guest_name?> <br>
    ID Type: <?php echo $guest_id_type?> <br>
    Address: <?php echo $guest_address?> <br>
    <center>------------------------------------------------------------- <br></center>

</div>
<script src="../../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            window.location.href = "index.php";
        }, 5000);
    });
</script>
</body>
</html>
