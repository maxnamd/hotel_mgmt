<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/5/2018
 * Time: 7:46 PM
 */

include "includes/header.php";
include "includes/scripts.php";
/*echo $_GET["actID"];*/

error_reporting(0);
if(is_null($_GET["actID"]) || empty($_GET["actID"])){
    $transaction_id = $_SESSION["actID"];
}
else{
    $transaction_id = $_GET["actID"];
    $_SESSION["actID"] = $transaction_id;
}
include "../phpfunctions/_frontdesk_for_custom_process.php";
?>
<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">

            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="white-box">
                        <h3 class="box-title">CHECK-IN INFORMATION</h3>

                        <ul class="list-icons">
                            <li><i class="fa fa-caret-right text-info"></i> Guest: <?php echo $guest_name?></li>
                            <li><i class="fa fa-caret-right text-info"></i> <i class="fa fa-calendar"></i> Check-in: <?php echo $date_checkin?> @ <?php echo $time_checkin?></li>
                            <li><i class="fa fa-caret-right text-info"></i> <i class="fa fa-calendar"></i> Check-out: <?php echo $date_checkout?> @ <?php echo $time_checkout?></li>
                            <li>
                                <i class="fa fa-caret-right text-info"></i>
                                <strong> Overdue: <span class="text-danger">

                                    <?php
                                    date_default_timezone_set('Asia/Manila');
                                    $current_date_cp = date('m/d/Y');
                                    $fk_current_date = date('m/d/Y');
                                    $query_test_date= "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$transaction_id'";
                                    $stmt_test_date = $DBcon->prepare( $query_test_date );
                                    $stmt_test_date->execute();
                                    if($stmt_test_date->rowCount() > 0) {
                                        while ($row_td = $stmt_test_date->fetch(PDO::FETCH_ASSOC)) {
                                            $up_checkout_date = $row_td["checkout_date"];
                                            $up_checkout_time = $row_td["checkout_time"];
                                        }
                                    }

                                    if($up_checkout_date != ""){
                                        $test_date_1b = $up_checkout_date . ' ' . $up_checkout_time;
                                        $dateDiff = intval((strtotime($current_date_cp)-strtotime($test_date_1b))/60);
                                        $hours_b = intval($dateDiff/60);
                                        if($hours_b==0){
                                            $total_overdue = $room_rate;
                                            echo 'PHP '. $total_overdue;
                                            $od_days = 1;
                                            $_SESSION["od_days"] = $od_days;
                                            $_SESSION["total_overdue"] = $total_overdue;
                                            echo "<span class=\"text-muted h6\"> *This will be added to the unpaid bill</span>";
                                            echo " <li><i class=\"fa fa-caret-right text-info\"></i> Number of days passed:  $od_days   (From: $up_checkout_date to $fk_current_date )</li>";
                                        }
                                        else{
                                            if((strtotime($up_checkout_date))<(strtotime($current_date_cp))){
                                                $od_days = strtotime($current_date_cp)-strtotime($up_checkout_date);
                                                $od_days = round($od_days / (60 * 60 * 24));
                                                $total_overdue = $room_rate * $od_days;
                                                $_SESSION["od_days"] = $od_days;
                                                $_SESSION["total_overdue"] = $total_overdue;
                                                echo 'PHP '.$total_overdue;
                                                echo "<span class=\"text-muted h6\"> *This will be added to the unpaid bill</span>";
                                                echo " <li><i class=\"fa fa-caret-right text-info\"></i> Number of days passed:  $od_days From: $up_checkout_date to $fk_current_date</li>";

                                            }
                                            else{
                                                echo "NO OVERDUE YET";
                                            }
                                        }
                                    }
                                    else{
                                        $test_date_2b = $date_checkout . ' ' . $time_checkout;
                                        $dateDiff = intval((strtotime($current_date_cp)-strtotime($test_date_2b))/60);
                                        $hours_b = intval($dateDiff/60);
                                        if($hours_b==0){
                                            $total_overdue = $room_rate;
                                            $od_days = 1;
                                            $_SESSION["od_days"] = $od_days;
                                            $_SESSION["total_overdue"] = $total_overdue;
                                            echo 'PHP '.$total_overdue;
                                            echo "<span class=\"text-muted h6\"> *This will be added to the unpaid bill</span>";
                                            echo " <li><i class=\"fa fa-caret-right text-info\"></i> Number of days passed:  $od_days   (From: $date_checkout to $fk_current_date )</li>";
                                        }
                                        else{
                                            if((strtotime($date_checkout))<(strtotime($current_date_cp))){
                                                $od_days = strtotime($current_date_cp)-strtotime($date_checkout);
                                                $od_days = round($od_days / (60 * 60 * 24));
                                                $total_overdue = $room_rate * $od_days;
                                                $_SESSION["od_days"] = $od_days;
                                                $_SESSION["total_overdue"] = $total_overdue;
                                                echo 'PHP '.$total_overdue;
                                                echo "<span class=\"text-muted h6\"> *This will be added to the unpaid bill</span>";
                                                echo " <li><i class=\"fa fa-caret-right text-info\"></i> Number of days passed:  $od_days   (From: $date_checkout to $fk_current_date )</li>";
                                            }
                                            else{
                                                echo "NO OVERDUE YET";
                                            }
                                        }

                                    }
                                    ?>
                                    </span>
                                </strong>
                            </li>
                            <li>
                                <i class="fa fa-caret-right text-info"></i> <i class="fa fa-calendar"></i> Extended checkout:

                                <?php
                                $query_test_date_2= "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$transaction_id'";
                                $stmt_test_date_2 = $DBcon->prepare( $query_test_date_2 );
                                $stmt_test_date_2->execute();
                                if($stmt_test_date_2->rowCount() > 0) {
                                    while ($row_td_2 = $stmt_test_date_2->fetch(PDO::FETCH_ASSOC)) {
                                        $up_checkout_date_2 = $row_td_2["checkout_date"];
                                        $up_checkout_time_2 = $row_td_2["checkout_time"];
                                    }
                                }

                                if($up_checkout_date_2 != ""){
                                    echo $up_checkout_date_2;
                                }
                                else{
                                    echo "<span class=\"text-muted h6\"> *guest does not extend his/her check-out date yet. </span>";
                                }
                                ?>
                            </li>
                        </ul>
                        <div class="row">
                            <hr style="border-top: 2px dashed #8c8b8b !important;height: 1px;border: 0;clear:both;display:block;width: 96%;">
                        </div>
                        <p class=""><h5>Guest Add-Ons</h5></p>

                        <table class="table" id="tbl_aos">
                                <thead>
                                    <tr>
                                        <td>Name</td>
                                        <td>Qty.</td>
                                        <td>Cost</td>
                                        <td>Total</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                            <tbody>
                                <?php include "../phpfunctions/_frontdesk_for_custom_process_get_inserted_ao.php";?>
                            </tbody>
                        </table>

                        <div class="row">
                            <button type="button" class="btn btn-md btn-danger" name="btnCheckout" onclick="checkOut();"> CHECK-OUT </button>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="white-box">



                        <div class="row">
                            <h3 class="box-title"> <i class="fa fa-book"></i> AVAILABLE ADD-ONS</h3>

                            <table class="table">
                                <thead class="table-inverse">
                                <tr>
                                    <td>Add-On</td>
                                    <td>Price</td>
                                    <td>ACTION</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php include "../phpfunctions/_frontdesk_for_custom_process_get_avaialble_ao.php";?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <hr style="border-top: 1px dotted #8c8b8b !important;height: 1px;border: 0;clear:both;display:block;width: 96%;">
                        </div>
                        <div class="row">
                            <h3 class="box-title"> <i class="fa fa-calendar"></i> EXTEND CHECK-OUT DATE</h3>
                            <form method="post">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-12">Check-out Date:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="checkout_date" id="checkout_date" data-error="PLEASE CHECK-OUT DATE" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-12">Check-out Time:</label>
                                        <div class="input-group bootstrap-timepicker timepicker pull-right">
                                            <input id="checkout_time" name="checkout_time" type="text" class="form-control input-small"/>
                                        </div>

                                        <script type="text/javascript">
                                            $('#checkout_time').timepicker({
                                                minuteStep: 5,
                                                showInputs: false,
                                                disableFocus: true
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="col-sm-12"></label>
                                    <div class="form-group pull-right">
                                        <button class="btn btn-md btn-success" name="btnUpdateCheckOut">SAVE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="white-box">
                                <h3 class="box-title">BILLS</h3>

                                <div class="row container">
                                    <h3 class="box-title">UNPAID BILL</h3>
                                    <div class="col-md-12">
                                        <?php include "../phpfunctions/_frontdesk_custom_process_fetch_unpaid.php";?>
                                        <ul class="list-icons">
                                            <li> <i class="fa fa-caret-right text-info"></i> No of UNPAID Days: <strong class="text-danger"><?php if($no_days<0){echo 0;}else{echo $no_days;}?></strong></li>
                                            <li> <i class="fa fa-caret-right text-info"></i> Room Charges: <strong class="text-danger"><?php echo $rc;?></strong></li>
                                            <li> <i class="fa fa-caret-right text-info"></i> Add-on Charges: <strong class="text-danger"><?php echo $ac;?></strong></li>
                                            <li> <i class="fa fa-caret-right text-info"></i> Total Charges: <strong class="text-danger"><?php echo $total;?></strong></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="row container">
                                    <h3 class="box-title">PAID BILL</h3>
                                    <div class="col-md-12">
                                        <?php include "../phpfunctions/_frontdesk_custom_process_fetch_paid.php";?>
                                        <ul class="list-icons">
                                            <li> <i class="fa fa-caret-right text-info"></i> Room Charges: <strong class="text-danger"><?php echo $rc_p;?></strong></li>
                                            <li> <i class="fa fa-caret-right text-info"></i> Add-on Charges: <strong class="text-danger"><?php echo $ac_p;?></strong></li>
                                            <li> <i class="fa fa-caret-right text-info"></i> Total Charges: <strong class="text-danger"><?php echo $total_p;?></strong></li>
                                            <li> <i class="fa fa-caret-right text-info"></i> Guest Cash: <strong class="text-danger"><?php echo $guest_cash;?></strong></li>
                                            <li> <i class="fa fa-caret-right text-info"></i> Change: <strong class="text-danger"><?php echo $guest_change;?></strong></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="col-md-12">
                            <div class="white-box">
                                <h3 class="box-title">HISTORY</h3>
                                <ul class="list-icons">
                                    <?php include "../phpfunctions/_frontdesk_custom_process_fetch_history.php";?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>


    <script type="text/javascript">
        $('#checkin_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom',
            format: 'mm/dd/yyyy',
            startDate: '+1d',
        });
        $('#checkout_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom',
            format: 'mm/dd/yyyy',
            startDate: '+1d',
        });
    </script>

    <script>
        function checkOut() {
            window.location.href = "checkin_custom_process_checkout.php";
        }
    </script>
</body>

</html>

<?php
include "../phpfunctions/_frontdesk_custom_process_update_checkout.php";
?>
