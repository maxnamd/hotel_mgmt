<?php

include "includes/header.php";
include "../phpfunctions/_frontdesk_get_selected_room.php";

?>
    <script>
        setInterval(function(){ getUsers(); }, 1000);

        function getUsers()
        {
            $.ajax({
                url: '../phpfunctions/ajax_refresh_tnumber.php',
                type: 'post',
                success: function(data) {
                    $('#last_num').val(data);
                }
            });
        }
    </script>

    <div id="wrapper">
        <div class="htmlelement">

        </div>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"></h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">FRONT DESK MODE</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-8">
                        <div class="white-box">
                            <h4 class="box-title"><i class="fa fa-group"></i> Guest Information</h4>
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-12">Guest Name:</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" name="guest_name" data-error="INSERT NAME OF THE GUEST" required/>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Gender</label>

                                            <div class="radio-list">
                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input name="guest_gender" id="radio1" value="Male" type="radio"/>
                                                        <label for="radio1">Male</label>
                                                    </div>
                                                </label>

                                                <label class="radio-inline">
                                                    <div class="radio radio-info">
                                                        <input name="guest_gender" id="radio2" value="Female" type="radio"/>
                                                        <label for="radio2">Female </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-12">Address:</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" name="guest_address" data-error="INSERT ADDRESS OF THE GUEST" />
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="col-sm-12">ID Type:</label>
                                            <div class="col-sm-12">
                                                <select class="form-control" name="guest_id_type" data-error="SELECT THE ID TYPE OF THE GUEST" required>
                                                    <option value="">--SELECT ID TYPE--</option>
                                                    <option value="Drivers License">Drivers License</option>
                                                    <option value="Passport">Passport</option>
                                                    <option value="Voters ID">Voters ID</option>
                                                    <option value="TIN">TIN</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="col-sm-12">ID Number:</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" name="guest_id_number" data-error="INSERT THE ID NUMBER" required/>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="col-sm-12">Phone Number:</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" name="guest_phone_number" data-error="INSERT PHONE NUMBER OF THE GUEST" type="number" required/>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="hidden">
                                   <!-- /*
                                    $sr_name
                                    $sr_floor
                                    $sr_type
                                    $sr_rate
                                    */-->
                                    <input type="hidden" value="<?php echo $sr_id?>" name="sr_id"/>
                                    <input type="hidden" value="<?php echo $sr_floor?>" name="sr_floor"/>
                                    <input type="hidden" value="<?php echo $sr_name?>" name="sr_name"/>
                                    <input type="hidden" value="<?php echo $sr_type?>" name="sr_type"/>
                                    <input type="hidden" value="<?php echo $sr_rate?>" name="sr_rate"/>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="hidden" id="last_num" name="last_num"/>
                                            <button class="btn btn-success btn-md pull-right" name="btnProceed">PROCEED</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="box-title"> <i class="fa fa-info-circle"> </i> CHECK-IN INFO</h4>
                            </div>

                            <div class="panel-body">
                                <p>
                                    You have selected the <strong><?php echo $sr_name;?></strong> with a category of <strong><?php echo $sr_type;?></strong>, and the Rate per day is <strong><?php echo $rate?></strong>.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <?php
    include "includes/scripts.php";
    ?>

</body>

</html>

<?php
if(isset($_POST["btnProceed"])){
    $last_num = $_POST['last_num'];
    $transaction_num = str_pad($last_num, 7, "0", STR_PAD_LEFT);
    $transaction_num = 'HTL'.$transaction_num;
    $_SESSION["transaction_number"] = $transaction_num;
    $_SESSION["guest_name"] = $_POST["guest_name"];
    $_SESSION["guest_gender"] = $_POST["guest_gender"];
    $_SESSION["guest_address"] = $_POST["guest_address"];
    $_SESSION["guest_id_type"] = $_POST["guest_id_type"];
    $_SESSION["guest_id_number"] = $_POST["guest_id_number"];
    $_SESSION["guest_phone_number"] = $_POST["guest_phone_number"];
    $_SESSION["sr_id"] = $_POST["sr_id"];



    $query_clean_1 = "DELETE FROM tbl_activities_addons_temp WHERE transaction_number NOT IN(SELECT t_number FROM tbl_transaction_number WHERE t_number IS NOT NULL)";
    $stmt_clean_1 = $DBcon->prepare( $query_clean_1 );
    $stmt_clean_1->execute();

    /*$query_clean_tno = "DELETE FROM tbl_transaction_number WHERE t_number NOT IN(SELECT transaction_number FROM tbl_activities WHERE transaction_number IS NOT NULL)";
    $stmt_clean = $DBcon->prepare( $query_clean_tno );
    $stmt_clean->execute();*/


    include "../phpfunctions/connect.php";
    $query_tn = "INSERT INTO tbl_transaction_number (t_number) VALUES ('$transaction_num')";
    $stmt_tn = $DBcon->prepare( $query_tn );
    $stmt_tn ->execute();

    echo "<script>
     window.location.href = \"checkin_process_2.php\";
    </script>";
}


?>
