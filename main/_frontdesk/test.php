<?php
include "includes/header.php";
include "includes/scripts.php";
?>

    <div id="wrapper">

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard Page</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Dashboard</a></li>
                            <li class="active">Home</li>
                        </ol>
                    </div>
                    <!-- /.breadcrumb -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Blank Starter page</h3>


                            <?php
                            /**
                             * Created by PhpStorm.
                             * User: ayson
                             * Date: 2/13/2018
                             * Time: 7:45 PM
                             */

                            date_default_timezone_set('Asia/Manila');
                            $currentHour = date('m/d/Y h:i A');
                            include "../phpfunctions/connect.php";
                            $query_notify_b1 = "SELECT * FROM tbl_activities WHERE act_status = 'ONGOING'";
                            $stmt_b1 = $DBcon->prepare( $query_notify_b1 );
                            $stmt_b1->execute();

                            if($stmt_b1->rowCount() > 0) {

                                while ($row_b1 = $stmt_b1->fetch(PDO::FETCH_ASSOC)) {
                                    $x_room_name = $row_b1["room_name"];
                                    $x_checkout_date = $row_b1["checkout_date"];
                                    $x_checkout_time = $row_b1["checkout_time"];

                                    $x_transaction_number = $row_b1["transaction_number"];

                                    $query_notify_b2 = "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$x_transaction_number'";
                                    $stmt_b2 = $DBcon->prepare( $query_notify_b2 );
                                    $stmt_b2->execute();
                                    if($stmt_b2->rowCount() > 0) {
                                        while ($row_b2 = $stmt_b2->fetch(PDO::FETCH_ASSOC)) {
                                            $y_checkout_date = $row_b2["checkout_date"];
                                            $y_checkout_time = $row_b2["checkout_time"];
                                        }

                                        if($y_checkout_date != ""){
                                            $test_date = $y_checkout_date . ' ' . $y_checkout_time;
                                            $dateDiff = intval((strtotime($currentHour)-strtotime($test_date))/60);
                                            $hours = intval($dateDiff/60);
                                            $minutes = $dateDiff%60;
                                            echo $hours;
                                            echo "<br>";
                                            ?>
                                            <!-- Start an Alert -->
                                            <div id="alertbottomleft" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-bottom-left"> <img src="../../plugins/images/users/genu.jpg" class="img" alt="img"><a href="#" class="closed">&times;</a>
                                                <h4>You have a Message!</h4>
                                                <b>John Doe</b> sent you a message.</div>
                                            <script type="text/javascript">
                                                    $("#alertbottomleft").fadeToggle(4050);
                                            </script>
                                            <?php
                                        }
                                        else{
                                            $dateDiff = intval((strtotime($currentHour)-strtotime($y_checkout_time))/60);
                                            $hours = intval($dateDiff/60);
                                            $minutes = $dateDiff%60;
                                            echo $currentHour;
                                        }
                                    }


                                }
                            }
                            ?>
                        </div>
                    </div>

                </div>
                <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>


</body>

</html>
