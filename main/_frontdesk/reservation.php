<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/3/2018
 * Time: 9:56 PM
 */

include "includes/header.php";
include "includes/scripts.php";
?>
<div id="wrapper">

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <!-- .page title -->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Reservation</h4>
                </div>
                <!-- /.page title -->
                <!-- .breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Reservation</li>
                    </ol>
                </div>
                <!-- /.breadcrumb -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-5">
                    <div class="white-box">
                        <h3 class="box-title">Book a Reservation</h3>

                        <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-6">Guest Name:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="guest_name" data-error="INSERT NAME OF THE GUEST" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-6">Guest Phone:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="guest_phone" data-error="INSERT PHONE NUMBER OF THE GUEST" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-6">Address:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="guest_address" data-error="INSERT NAME OF THE GUEST" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">ID Type:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="guest_id_type" data-error="SELECT THE ID TYPE OF THE GUEST" required>
                                                <option value="">--SELECT ID TYPE--</option>
                                                <option value="Drivers License">Drivers License</option>
                                                <option value="Passport">Passport</option>
                                                <option value="Voters ID">Voters ID</option>
                                                <option value="TIN">TIN</option>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-12">ID Number:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="guest_id_number" data-error="INSERT THE ID NUMBER" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-6">Select Room:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="room_name" data-error="SELECT A ROOM" required>
                                                <option value="">--SELECT ROOM--</option>
                                                <?php include "../phpfunctions/_frontdesk_reservation_readrooms.php";?>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-sm-12">Check-in Date:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="checkin_date" id="checkin_date" data-error="PLEASE CHOOSE CHECK-IN DATE" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-sm-12">Check-in Time:</label>
                                        <div class="input-group bootstrap-timepicker timepicker pull-right">
                                            <input id="checkin_time" name="checkin_time" type="text" class="form-control input-small"/>
                                        </div>

                                        <script type="text/javascript">
                                            $('#checkin_time').timepicker({
                                                minuteStep: 5,
                                                showInputs: false,
                                                disableFocus: true
                                            });
                                        </script>

                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-sm-12">Check-out Date:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" name="checkout_date" id="checkout_date" data-error="PLEASE CHECK-OUT DATE" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-sm-12">Check-out Time:</label>
                                        <div class="input-group bootstrap-timepicker timepicker pull-right">
                                            <input id="checkout_time" name="checkout_time" type="text" class="form-control input-small"/>
                                        </div>

                                        <script type="text/javascript">
                                            $('#checkout_time').timepicker({
                                                minuteStep: 5,
                                                showInputs: false,
                                                disableFocus: true
                                            });
                                        </script>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success btn-md pull-right" name="btnReserve">RESERVE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="white-box">
                        <h3 class="box-title">Reservation List</h3>

                        <table id="demo-foo-row-toggler" class="table toggle-circle table-hover color-bordered-table dark-bordered-table">
                            <thead class="">
                                <th data-toggle="true">Room Name</th>
                                <th>Check-in Date</th>
                                <th>Check-out Date</th>
                                <th>Guest Name</th>
                                <th>Contact</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th data-hide="all"> Guest Address </th>
                                <th data-hide="all"> ID Type </th>
                                <th data-hide="all"> ID Number </th>
                            </thead>

                            <tbody id="refme">
                                <?php include '../phpfunctions/_frontdesk_reservation_readreservation.php'; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- .row -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; Elite Admin brought to you by themedesigner.in </footer>
    </div>
    <!-- /#page-wrapper -->
</div>

    <?php

    ?>
    <script type="text/javascript">
        $('#checkin_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom',
            format: 'mm/dd/yyyy',
            startDate: '+1d',
        });
        $('#checkout_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom',
            format: 'mm/dd/yyyy',
            startDate: '+2d',
        });
    </script>
</body>
</html>
<?php
    include "../phpfunctions/_frontdesk_reservation_insert.php";
?>