<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/11/2018
 * Time: 9:21 PM
 */

include "connect.php";
$query_room = "SELECT * FROM tbl_room_list";
$stmt_room = $DBcon->prepare( $query_room );
$stmt_room->execute();

if($stmt_room->rowCount() > 0) {

    $i = 0;
    while ($row_room = $stmt_room->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row_room["id"];
        extract($row_room);
        ?>
        <tr>
            <td> <?php echo $row_room["room_name"]?></td>
            <td> <?php echo $row_room["rate"]?></td>
            <td> <span class="label label-success" style="background-color: #0b67cd !important;"><?php echo $row_room["status"]?></span></td>

            <td>
                <form method="post">
                    <button type="button" class="btn btn-success" onclick="clickAvailable(this.value);"  data-id="<?php echo $row_room['id']?>" value="<?php echo $row_room['id']?>"> AVAILABLE </button>
                    <button type="button" class="btn btn-success" style="background-color: #1c78de !important;"  onclick="clickCleaning(this.value);" data-id="<?php echo $row_room['id']?>" value="<?php echo $row_room['id']?>"> CLEANING </button>
                    <button type="button" class="btn btn-warning" onclick="clickMaintenance(this.value);"  data-id="<?php echo $row_room['id']?>" value="<?php echo $row_room['id']?>"> MAINTENANCE </button>
                </form>
            </td>
        </tr>
        <?php
    }
}
?>
<script>
    function clickAvailable(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ajax_clickAvailable.php",
            data:'room_id='+val,
            success: function(data){
                $("#test").load("../phpfunctions/_frontdesk_room_management.php");
            }
        });
    }
    function clickCleaning(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ajax_clickCleaning.php",
            data:'room_id='+val,
            success: function(data){
                $("#test").load("../phpfunctions/_frontdesk_room_management.php");
            }
        });
    }
    function clickMaintenance(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ajax_clickMaintenance.php",
            data:'room_id='+val,
            success: function(data){
                $("#test").load("../phpfunctions/_frontdesk_room_management.php");
            }
        });
    }
</script>
