<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 4:33 PM
 */
if(isset($_POST["btnReserve"])){
    include "connect.php";

    $guest_name = $_POST["guest_name"];
    $guest_phone = $_POST["guest_phone"];
    $room_name = "";
    $room_id = $_POST["room_name"];
    $checkin_time = $_POST["checkin_time"];
    $checkin_date = $_POST["checkin_date"];
    $checkout_time = $_POST["checkout_time"];
    $checkout_date = $_POST["checkout_date"];
    $guest_address = $_POST["guest_address"];
    $guest_id_type = $_POST["guest_id_type"];
    $guest_id_number = $_POST["guest_id_number"];

    $query_sroom = "SELECT * FROM tbl_room_list WHERE id = '$room_id'";
    $stmt_sroom = $DBcon->prepare( $query_sroom );
    $stmt_sroom->execute();

    if($stmt_sroom->rowCount() == 1) {
        $i = 0;
        while ($row_sroom = $stmt_sroom->fetch(PDO::FETCH_ASSOC)) {
            $my_id = $row_sroom["id"];
            extract($row_sroom);
            $room_name = $row_sroom["room_name"];
        }
    }

    $query_insertREV = "INSERT INTO tbl_reservations (room_name, checkin_time , checkin_date, checkout_time ,checkout_date, guest_name, guest_address, guest_id_type, guest_id_number,guest_phone,reservation_status) VALUES ('$room_name','$checkin_time','$checkin_date','$checkout_time','$checkout_date','$guest_name','$guest_address','$guest_id_type','$guest_id_number','$guest_phone','ready')";
    $stmt_insertREV = $DBcon->prepare( $query_insertREV );
    $stmt_insertREV->execute();

    $query_log = "INSERT INTO tbl_logs (user,activity) VALUES ('$username','Inserted a reservation')";
    $stmt_log = $DBcon->prepare( $query_log );
    $stmt_log ->execute();

    echo"
        <script type='text/javascript'>
            
            swal({
              title: 'SUCCESS!',
              text: \"New Reservation Added!\",
              type: \"success\",
              timer: 2000,
            }).then(
              function() {
            // Redirect the user
            window.location.href = \"reservation.php\";
            console.log('The Ok Button was clicked.');
            },
              // handling the promise rejection
              function (dismiss) {
                if (dismiss === 'timer') {
                   window.location.href = \"reservation.php\";
                }
              }
            )
        </script>
    ";
}



?>

<?php
if(isset($_POST["btnReserveFinal"])){

    include "connect.php";
    $last_num = $_POST['last_num'];
    $transaction_num = str_pad($last_num, 7, "0", STR_PAD_LEFT);
    $transaction_num = 'HTL'.$transaction_num;

    $a_room_name = $_POST["room_name"];
    $query_res = "SELECT * FROM tbl_room_list WHERE room_name = '$a_room_name'";
    $stmt_res = $DBcon->prepare( $query_res );
    $stmt_res->execute();

    if($stmt_res->rowCount() > 0) {
        while ($row_res = $stmt_res->fetch(PDO::FETCH_ASSOC)) {
            $my_id = $row_res["id"];
            extract($row_res);

            $room_rate = $row_res["rate"];
            $room_type = $row_res["room_type"];
            $floor_position = $row_res["floor_position"];
        }
    }



    $_SESSION["res_transaction_number"] = $transaction_num;
    $_SESSION["temp_resID"] = $_POST["temp_resID"];
    $_SESSION["res_guest_name"] = $_POST["guest_name"];
    $_SESSION["res_guest_gender"] = "";
    $_SESSION["res_guest_phone"] = $_POST["guest_phone"];
    $_SESSION["res_checkin_date"] = $_POST["checkin_date"];
    $_SESSION["res_checkout_date"] = $_POST["checkout_date"];
    $_SESSION["res_checkin_time"] = $_POST["checkin_time"];
    $_SESSION["res_checkout_time"] = $_POST["checkout_time"];
    $_SESSION["res_guest_address"] = $_POST["guest_address"];
    $_SESSION["res_guest_id_type"] = $_POST["guest_id_type"];
    $_SESSION["res_guest_id_number"] = $_POST["guest_id_number"];
    $_SESSION["res_room_name"] = $_POST["room_name"];
    $_SESSION["res_room_type"] = $room_type;
    $_SESSION["res_room_rate"] = $room_rate;
    $_SESSION["res_floor_position"] = $floor_position;

    $query_tn = "INSERT INTO tbl_transaction_number (t_number) VALUES ('$transaction_num')";
    $stmt_tn = $DBcon->prepare( $query_tn );
    $stmt_tn ->execute();


    echo "<script>
    window.location.href = \"checkin_reservation_process_2.php\";
    </script>";
}
?>
