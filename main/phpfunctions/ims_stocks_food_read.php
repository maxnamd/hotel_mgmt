<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 12:14 AM
 */

include "connect.php";
$query = "SELECT * FROM ims_tbl_stocks WHERE category = 'FOOD'";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row_stocks=$stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id_stock = $row_stocks["id"];
        $stname = $row_stocks["name"];

        extract($row_stocks);
        ?>
        <tr>
            <td><?php echo $row_stocks["name"]; ?></td>
            <td><?php echo $row_stocks["category"]; ?></td>
            <td><?php echo $row_stocks["quantity"]; ?></td>
            <td class="text-center">
                <?php

                $query_test_stock = "SELECT * FROM ims_stock_history WHERE stock_name = '$stname'";
                $stmt_test_stock = $DBcon->prepare( $query_test_stock );
                $stmt_test_stock->execute();
                $result_test = $stmt_test_stock->rowCount();

                if($result_test == 0){
                    echo "<a class='label label-danger'>NO HISTORY YET</a>";
                }
                else{ ?>
                    <a class="label label-table label-inverse" href="reports_stock_history.php?stockID=<?php echo $my_id_stock; ?>">History</a>
                <?php }?>
            </td>
        </tr>
        <?php
        $i++; }

}
?>
<?php
if(isset($_POST['btnUpdateStock'])){

    $id = $_POST['id'];
    $stock_name = $_POST['stock_name'];
    $stock_category = $_POST['stock_category'];
    $stock_quantity = $_POST['stock_quantity'];

    $stock_category_hidden = $_POST['stock_category_hidden'];
    $stock_name_hidden = $_POST['stock_name_hidden'];
    $stock_quantity_hidden = $_POST['stock_quantity_hidden'];

    if($stock_category_hidden==$stock_category && $stock_name==$stock_name_hidden && $stock_quantity_hidden==$stock_quantity){
        $_SESSION["tab_no"] = "1";
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'There is an error updating the stock. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ims.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"ims.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $_SESSION["tab_no"] = "1";
        $sql = "UPDATE ims_tbl_stocks SET name = '$stock_name',category = '$stock_category',quantity = '$stock_quantity' WHERE id = '$id'";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"Stock Updated!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ims.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"ims.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {
            $_SESSION["tab_no"] = "1";
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'There is an error updating the stock. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ims.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"ims.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>

