<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 10:07 PM
 */

    $guest_name = $_SESSION['res_guest_name'];
    $guest_gender = $_SESSION['res_guest_gender'];
    $guest_address = $_SESSION['res_guest_address'];
    $guest_id_type = $_SESSION['res_guest_id_type'];
    $guest_id_number = $_SESSION['res_guest_id_number'];
    $guest_phone_number = $_SESSION['res_guest_phone'];
    $room_rate = $_SESSION['res_room_rate'];



    $guest_checkin_date = $_SESSION['res_checkin_date'];
    $guest_checkin_time = $_SESSION["res_checkin_time"];
    $guest_checkout_date = $_SESSION['res_checkout_date'];
    $guest_checkout_time = $_SESSION["res_checkout_time"];
    $guest_trasaction_id = $_SESSION['res_transaction_number'];

    $guest_av_days = strtotime($guest_checkout_date)-strtotime($guest_checkin_date);
    $guest_av_days = round($guest_av_days / (60 * 60 * 24));
    $_SESSION['res_guest_av_days'] = $guest_av_days;

    $guest_total_addons = $_SESSION['res_guest_total_addons'];
    $guest_discount = 0;

    $room_charges = $room_rate*$guest_av_days;
    $plain_total_payable  = $room_charges + $guest_total_addons;
    $patched_discount = $plain_total_payable * $guest_discount;
    $guest_total_payable = $plain_total_payable + $patched_discount;


?>