<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 12:14 AM
 */

include "connect.php";
$query = "SELECT * FROM ims_tbl_stocks";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row_stocks=$stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id_stock = $row_stocks["id"];
        $stname = $row_stocks["name"];
        extract($row_stocks);
        ?>
        <script type="text/javascript">
            function showTest_<?php echo $my_id_stock?>() {
                document.getElementById("testmehide_<?php echo $my_id_stock?>").classList.remove("hidden");
            }
        </script>
        <tr>
            <td><?php echo $row_stocks["name"]; ?></td>
            <td><?php echo $row_stocks["category"]; ?></td>
            <td><?php echo $row_stocks["quantity"]; ?></td>
            <td class="text-center">
                <?php

                $query_test_stock = "SELECT * FROM ims_stock_history WHERE stock_name = '$stname'";
                $stmt_test_stock = $DBcon->prepare( $query_test_stock );
                $stmt_test_stock->execute();
                $result_test = $stmt_test_stock->rowCount();

                if($result_test == 0){
                    echo "<a class='label label-danger'>NO HISTORY YET</a>";
                }
                else{ ?>
                    <a class="label label-table label-inverse" href="reports_stock_history.php?stockID=<?php echo $my_id_stock; ?>">History</a>
                <?php }?>



            </td>

            <td>
                <a href="#" class="col-sm-4 label label-table label-inverse" onclick="showTest_<?php echo $my_id_stock?>();"> <span class="fa fa-gear"> </span></a>

                <span id="testmehide_<?php echo $my_id_stock?>" class="hidden">
                    <a href="#editModal_<?php echo$i?>" class="col-sm-4 btn btn-sm btn-default" title="Edit Menu" data-toggle="modal" data-id='"<?php echo $i?>"'> <i class="glyphicon glyphicon-edit"></i></a>
                    <a class="col-sm-4 btn btn-sm btn-danger" id="delete_stock" data-id="<?php echo $my_id_stock; ?>" href="javascript:void(0)"><i class="glyphicon glyphicon-trash"></i></a>
                </span>

                <div class="modal fade" id="editModal_<?php echo$i?>" tabindex="-1" role="dialog" aria-labelledby="editModal_<?php echo$i?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">Edit Stock: <span class="font-weight-bold"><?php echo $row_stocks["name"];?></span></h4>
                            </div>
                            <div class="modal-body">
                                <form method="post">


                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Room Name:</label>
                                        <input type="text" class="form-control" id="" value="<?php echo $row_stocks["name"];?>" name="stock_name"/>
                                        <input type="hidden" name="stock_name_hidden" value="<?php echo $row_stocks["room_name"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Stock Category:</label>

                                        <input type="hidden" name="stock_category_hidden" value="<?php echo $row_stocks["category"];?>"/>
                                        <select class="form-control" name="stock_category" id="stock_category_<?php echo $i?>" data-error="PLEASE CHOOSE A STOCK CATEGORY" required>
                                            <option value="<?php echo $row_stocks["stock_category"];?>"> <?php echo $row_stocks["category"];?> </option>
                                            <?php
                                            $query_sc = "SELECT * FROM ims_tbl_category";
                                            $stmt_sc = $DBcon->prepare( $query_sc );
                                            $stmt_sc->execute();

                                            if($stmt_sc->rowCount() > 0) {
                                                while ($row_sc = $stmt_sc->fetch(PDO::FETCH_ASSOC)) {
                                                    $my_id_sc = $row_sc["id"];
                                                    extract($row_sc);
                                                    ?>
                                                    <option value="<?php echo $row_sc['name']; ?>"><?php echo $row_sc['name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="room_rate_edit" class="control-label">Quantity:</label>
                                        <input class="form-control" id="quantity_edit_<?php echo $i?>" value="<?php echo $row_stocks["quantity"];?>" name="stock_quantity"/>
                                        <input type="hidden" name="stock_quantity_hidden" value="<?php echo $row_stocks["quantity"];?>"/>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $row_stocks["id"];?>"/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button name="btnUpdateStock" type="submit" class="btn btn-success">UPDATE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
        <?php
        $i++; }

}
?>
<?php
if(isset($_POST['btnUpdateStock'])){

    $id = $_POST['id'];
    $stock_name = $_POST['stock_name'];
    $stock_category = $_POST['stock_category'];
    $stock_quantity = $_POST['stock_quantity'];

    $stock_category_hidden = $_POST['stock_category_hidden'];
    $stock_name_hidden = $_POST['stock_name_hidden'];
    $stock_quantity_hidden = $_POST['stock_quantity_hidden'];

    if($stock_category_hidden==$stock_category && $stock_name==$stock_name_hidden && $stock_quantity_hidden==$stock_quantity){
        $_SESSION["tab_no"] = "1";
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'There is an error updating the stock. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $_SESSION["tab_no"] = "1";
        $sql = "UPDATE ims_tbl_stocks SET name = '$stock_name',category = '$stock_category',quantity = '$stock_quantity' WHERE id = '$id'";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"Stock Updated!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"ims.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {
            $_SESSION["tab_no"] = "1";
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'There is an error updating the stock. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"ims.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>

