<script>
    function getStockCategory(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ims_stocks_ajax.php",
            data:'stock_name='+val,
            success: function(data){
                $('#stock_category_new').val(data);
            }
        });
    }
</script>

<div class="row white-box">
    <!-- .left side -->
    <div class="col-sm-6">
        <h3 class="box-title"> <i class="fa fa-bed"></i> Stocks</h3>
        <div class="admintbl">
            <table id="tbl_rooms" class="table display nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="col-sm-3">Stock Name</th>
                    <th class="col-sm-2">Category</th>
                    <th class="col-sm-1 text-center">Quantity</th>
                    <th class="col-sm-2 text-center">Action</th>
                    <th class="col-sm-3">

                    </th>
                </tr>
                </thead>

                <tbody>
                <?php include "../phpfunctions/ims_stocks_read.php";?>
                </tbody>
            </table>
        </div>

    </div>
    <!-- /.left side -->

    <!-- .right side -->
    <div class="col-sm-6">

        <h3 class="box-title"> <i class="fa fa-plus"></i> Add Stocks <a class="pull-right fcbtn btn btn-outline btn-success btn-1f" style="color: black !important;" data-toggle="modal" data-target="#addModal">ACQUISITION-IN</a></h3>
        <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-12">Stock Name:</label>
                        <div class="col-sm-12">
                            <input class="form-control" name="stock_name" data-error="INSERT STOCK NAME" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-12">Stock Type:</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="stock_category" id="stock_category"  data-error="PLEASE CHOOSE A STOCK CATEGORY" required>
                                <option value="">--SELECT CATEGORY TYPE--</option>
                                <?php

                                $DBhost = "localhost";
                                $DBuser = "root";
                                $DBpass = "";
                                $DBname = "hotel_db";

                                try {
                                    $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname",$DBuser,$DBpass);
                                    $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                } catch(PDOException $ex){
                                    die($ex->getMessage());
                                }
                                $query = "SELECT * FROM ims_tbl_category";
                                $stmt = $DBcon->prepare( $query );
                                $stmt->execute();

                                if($stmt->rowCount() > 0) {

                                    $i = 0;
                                    while ($row_cat = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                        extract($row_cat);
                                        ?>
                                        <option value="<?php echo $row_cat['name']; ?>"><?php echo $row_cat['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row hidden">


                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-12">Quantity:</label>
                        <div class="col-sm-12">
                            <input class="form-control" name="quantity" id="quantity" data-error="PLEASE ENTER A STOCK QUANTITY" value="0" type="hidden"/>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-success pull-right" type="submit" name="btnAddStock">ADD</button>
                </div>
            </div>
        </form>

        <script>
            $(document).ready(function(){

                $(document).on('click', '#delete_stock', function(e){
                    var stockId = $(this).data('id');
                    SwalDeleteStock(stockId);
                    console.log(stockId);
                    e.preventDefault();
                });

            });


            function SwalDeleteStock(stockId){
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,

                    preConfirm: function() {
                        return new Promise(function(resolve) {
                            $.ajax({
                                url: '../phpfunctions/ims_stocks_delete.php',
                                type: 'POST',
                                data: 'delete='+stockId,
                                dataType: 'json'
                            })
                                .done(function(response){
                                    swal('Deleted!', response.message, response.status);
                                    window.setTimeout(function(){
                                        window.location.href = "stocks.php";
                                    }, 2000);
                                })
                                .fail(function(){
                                    swal('Oops...', 'Something went wrong with ajax !', 'error');
                                });
                        });
                    },
                    allowOutsideClick: false
                });
            }
        </script>
    </div>
    <!-- /.right side -->
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create New Stock</h4>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal">

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12">Stock Name:</label>
                                <div class="col-sm-12">
                                    <select class="form-control"  onChange="getStockCategory(this.value);" name="stock_name" id="stock_name_new" data-error="PLEASE STOCK NAME" required>
                                        <option value="">--SELECT STOCK--</option>
                                        <?php
                                        $query_stock = "SELECT * FROM ims_tbl_stocks";
                                        $stmt_stock = $DBcon->prepare( $query_stock );
                                        $stmt_stock->execute();

                                        if($stmt_stock->rowCount() > 0) {
                                            while ($row_stock = $stmt_stock->fetch(PDO::FETCH_ASSOC)) {
                                                extract($row_stock);
                                                ?>
                                                <option value="<?php echo $row_stock['name']; ?>"><?php echo $row_stock['name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12">Quantity:</label>
                                <div class="col-sm-12">
                                    <input class="form-control" name="quantity" id="quantity" data-error="PLEASE ENTER A STOCK QUANTITY" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-12">Stock Category:</label>
                                <div class="col-sm-12">
                                    <input class="form-control" name="stock_category" id="stock_category_new" readonly>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-12">Supplier Name:</label>
                                <div class="col-sm-12">
                                    <input class="form-control" name="supplier_name" data-error="INSERT SUPPLIER" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="submit"class="btn btn-success" name="btnAddNewStock" value="ADD">
                        <input type="reset" class="btn btn-default" value="Clear">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include "ims_stocks_insert.php";?>