<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 10:14 PM
 */


if(isset($_POST['btnInsertAO'])) {
    include "connect.php";
    $date = date('m/d/Y');
    $selected_addon = $_POST["ao_name"];

    $post_ao_name = $_POST["ao_name"];
    $post_ao_cost = $_POST["ao_cost"];

    $query_check_ao = "SELECT COUNT(*) AS num_rows FROM tbl_activities_addons_temp WHERE ao_name='{$selected_addon}' AND transaction_number = '$transaction_number' LIMIT 1;";
    $stmt_check_ao = $DBcon->prepare($query_check_ao);
    $stmt_check_ao->execute();
    $check_if_ao_exists = "";


    if ($stmt_check_ao->rowCount() > 0) {

        while ($row_check_addons = $stmt_check_ao->fetch(PDO::FETCH_ASSOC)) {
            extract($row_check_addons);
            $check_if_ao_exists = $row_check_addons["num_rows"];
        }
    }

    if ($check_if_ao_exists > 0){
        $query_fetch_ao_exist = "SELECT * FROM tbl_activities_addons_temp WHERE ao_name = '$selected_addon' AND transaction_number = '$transaction_number'";
        $stmt_fetch_ao_exist = $DBcon->prepare($query_fetch_ao_exist);
        $stmt_fetch_ao_exist->execute();
        if ($stmt_fetch_ao_exist->rowCount() > 0) {
            while ($row_fetch_addons_exist = $stmt_fetch_ao_exist->fetch(PDO::FETCH_ASSOC)) {
                extract($row_fetch_addons_exist);

                $ao_exist_name = $row_fetch_addons_exist["ao_name"];
                $ao_exist_quantity = $row_fetch_addons_exist["ao_quantity"];
                $ao_exist_cost = $row_fetch_addons_exist["ao_cost"];
                $ao_exist_total = $row_fetch_addons_exist["ao_total"];
                $ao_exist_quantity+=1;

                $total_cost = $ao_exist_quantity * $ao_exist_cost;

                $query_3 = "UPDATE tbl_activities_addons_temp SET ao_quantity ='$ao_exist_quantity', ao_total = '$total_cost' WHERE ao_name = '$selected_addon' AND transaction_number = '$transaction_number'";
                $stmt_3 = $DBcon->prepare($query_3);
                $stmt_3->execute();

                echo "<script>
                    setTimeout('window.location.replace(\'checkin_reservation_process_2.php\')',600);
                </script>\";";
            }
        }
    }
    elseif($check_if_ao_exists == 0){
        $query_3 = "INSERT INTO tbl_activities_addons_temp (ao_name, ao_quantity, ao_cost, ao_total, transaction_number, transaction_date, paid_status, clerk) VALUES ('$post_ao_name','1','$post_ao_cost','$post_ao_cost','$transaction_number','$date','','$username')";
        $stmt_3 = $DBcon->prepare($query_3);
        $stmt_3->execute();

        echo "<script>
                    setTimeout('window.location.replace(\'checkin_reservation_process_2.php\')',600);
                </script>\";";
    }
}
?>