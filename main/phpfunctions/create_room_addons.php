<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/16/2018
 * Time: 9:25 PM
 */



if(isset($_POST['btnAdd_ao'])){

    $ao_name = $_POST['ao_name'];
    $ao_cost = $_POST['ao_cost'];
    if($ao_name=="" || $ao_cost<=0){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'No Items Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRoomCategory.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRoomCategory.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $sql = "INSERT INTO tbl_room_addons (ao_name,ao_cost)VALUES ('$ao_name','$ao_cost')";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"New Add-Ons Added!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageAddOns.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageAddOns.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'No Items Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageAddOns.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageAddOns.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>



