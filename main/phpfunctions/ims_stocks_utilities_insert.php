<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/26/2018
 * Time: 7:12 PM
 */


if(isset($_POST['btnAddStock'])){

    $stock_name = $_POST['stock_name'];
    $stock_category = $_POST['stock_category'];
    $quantity = $_POST['quantity'];
    $v_quantity = $_POST['quantity'];
    $supplier_name = $_POST['supplier_name'];


    $query_exist = "SELECT * FROM ims_tbl_stocks WHERE name = '$stock_name'";
    $stmt_exist = $DBcon->prepare( $query_exist );
    $stmt_exist->execute();
    while($row_exist=$stmt_exist->fetch(PDO::FETCH_ASSOC)) {
        extract($row_exist);
        $current_quantity = $row_exist["quantity"];
    }
    if($v_quantity <= 0){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'Check the quantity! No stock added',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
    }
    else {

        if ($stmt_exist->rowCount() >= 1) {

            $new_quantity = $v_quantity + $current_quantity;
            $query_update_exist = "UPDATE ims_tbl_stocks SET quantity = '$new_quantity' WHERE name = '$stock_name'";
            $stmt_update_exist = $DBcon->prepare($query_update_exist);
            $stmt_update_exist->execute();

            $query_to_history = "INSERT INTO ims_stock_history (activity, stock_name, supplier, quantity, user, acquisition_type) VALUES ('$username added $v_quantity stocks to $stock_name','$stock_name','$supplier_name','$v_quantity','$username','IN')";
            $stmt_to_history = $DBcon->prepare($query_to_history);
            $stmt_to_history->execute();

            echo "
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: 'Added $v_quantity in Stock ($stock_name) Added.',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
        } else {
            if ($stock_name == "") {
                $_SESSION["tab_no"] = "1";
                echo "
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'No Stock Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
            } else {
                $query_to_history = "INSERT INTO ims_stock_history (activity, stock_name, supplier, quantity, user, acquisition_type) VALUES ('(new) $username added $v_quantity stocks to $stock_name','$stock_name','$supplier_name','$v_quantity','$username','IN')";
                $stmt_to_history = $DBcon->prepare($query_to_history);
                $stmt_to_history->execute();

                $sql = "INSERT INTO ims_tbl_stocks (name, category, quantity)VALUES ('$stock_name','$stock_category','$quantity')";

                if ($conn->query($sql) === TRUE) {
                    $_SESSION["tab_no"] = "1";
                    echo "
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"New Stock Category Added!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
                } else {
                    $_SESSION["tab_no"] = "1";
                    echo "
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'No Stock Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
                }

            }

        }

    }
}
if(isset($_POST['btnAddNewStock'])){

    $stock_name = $_POST['stock_name'];
    $stock_category = $_POST['stock_category'];
    $quantity = $_POST['quantity'];

    $query_exist = "SELECT * FROM ims_tbl_stocks WHERE name = '$stock_name'";
    $stmt_exist = $DBcon->prepare( $query_exist );
    $stmt_exist->execute();

    if($stmt_exist->rowCount() >= 1) {
        $_SESSION["tab_no"] = "1";
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'No Stock Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        if($stock_name==""){
            $_SESSION["tab_no"] = "1";
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'No Stock Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
        }
        else{
            $sql = "INSERT INTO ims_tbl_stocks (name, category, quantity)VALUES ('$stock_name','$stock_category','$quantity')";

            if ($conn->query($sql) === TRUE) {
                $_SESSION["tab_no"] = "1";
                echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"New Stock Category Added!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
            }
            else {
                $_SESSION["tab_no"] = "1";
                echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'No Stock Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks.php\";
                    }
                  }
                )
			</script>
		";
            }

        }

    }


}
?>