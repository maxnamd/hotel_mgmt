<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/25/2018
 * Time: 9:38 PM
 */

$guest_total_addons = 0;
include "connect.php";
$temp_ao = "SELECT * FROM tbl_activities_addons_temp WHERE transaction_number = '$transaction_number'";
$stmt_temp_ao = $DBcon->prepare( $temp_ao );
$stmt_temp_ao ->execute();


if($stmt_temp_ao->rowCount() > 0) {

    while($row_temp_ao=$stmt_temp_ao->fetch(PDO::FETCH_ASSOC)) {
        extract($row_temp_ao);
        $guest_total_addons+=$row_temp_ao["ao_total"];
        ?>
        <tr>
            <td><?php echo $row_temp_ao["ao_name"]?></td>
            <td><?php echo $row_temp_ao["ao_quantity"]?></td>
            <td><?php echo $row_temp_ao["ao_cost"]?></td>
            <td><?php echo $row_temp_ao["ao_total"]?></td>
            <td>
                <button type="button" class="btn btn-sm" onclick="clickDelete(this.value);" value="<?php echo $row_temp_ao['id']?>">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
        <?php
    }
}
$_SESSION['guest_total_addons'] = $guest_total_addons;
?>
<script type="text/javascript">
    function clickDelete(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ajax_delete_temp_addon.php",
            data:'t_ao_id='+val,
            success: function(data){
                $("#tbody").load(" #tbody");
            }
        });
    }
</script>
