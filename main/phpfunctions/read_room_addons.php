<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 12:14 AM
 */
?>
<?php

include "connect.php";
$query_ao = "SELECT * FROM tbl_room_addons";
$stmt_ao = $DBcon->prepare( $query_ao );
$stmt_ao->execute();

if($stmt_ao->rowCount() > 0) {

    $i = 0;
    while($row_ao=$stmt_ao->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row_ao["id"];
        extract($row_ao);
        ?>
        <tr>
            <td><?php echo $row_ao["ao_name"]; ?></td>
            <td><?php echo $row_ao["ao_cost"]; ?></td>
            <td>
                <center>
                    <a href="#editModal_<?php echo$i?>" class="col-sm-4 btn btn-md btn-default" title="Edit Menu" data-toggle="modal" data-id='"<?php echo $i?>"'> <i class="glyphicon glyphicon-edit"></i></a>
                    <a class="col-sm-4 btn btn-md btn-danger" id="delete_addons" data-id="<?php echo $my_id; ?>" href="javascript:void(0)"><i class="glyphicon glyphicon-trash"></i></a>
                </center>

                <div class="modal fade" id="editModal_<?php echo$i?>" tabindex="-1" role="dialog" aria-labelledby="editModal_<?php echo$i?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">Edit Add-On: <span class="font-weight-bold"><?php echo $row_ao["ao_name"];?></span></h4>
                            </div>
                            <div class="modal-body">
                                <form method="post">


                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Add-On Name:</label>
                                        <input type="text" class="form-control" id="recipient-name1" value="<?php echo $row_ao["ao_name"];?>" name="ao_name"/>
                                        <input type="hidden" name="ao_name_hidden" value="<?php echo $row_ao["ao_name"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="room_rate_edit" class="control-label">Add-On Cost:</label>
                                        <input class="form-control" id="room_rate_edit_<?php echo $i?>" value="<?php echo $row_ao["ao_cost"];?>" name="ao_cost_edit"/>
                                        <input type="hidden" name="ao_cost_hidden" value="<?php echo $row_ao["ao_cost"];?>"/>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $row_ao["id"];?>"/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button name="btnUpdateAddOn" type="submit" class="btn btn-success">UPDATE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
        <?php
        $i++; }

}
?>
<?php
if(isset($_POST['btnUpdateAddOn'])){

    $id = $_POST['id'];
    $ao_name = $_POST['ao_name'];
    $ao_cost = $_POST['ao_cost_edit'];

    $ao_name_old = $_POST['ao_name_hidden'];
    $ao_cost_old = $_POST['ao_cost_hidden'];

    if($ao_name==$ao_name_old && $ao_cost<=$ao_cost_old){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'There is an error updating the room add-ons. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageAddOns.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageAddOns.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $sql = "UPDATE tbl_room_addons SET ao_name = '$ao_name',ao_cost = '$ao_cost' WHERE id = '$id'";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"Add-Ons Updated!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageAddOns.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageAddOns.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {

            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'There is an error updating the room add-ons. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageAddOns.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageAddOns.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>

