<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/3/2018
 * Time: 10:34 PM
 */

include "connect.php";
$query_avroom = "SELECT * FROM tbl_room_list WHERE status = 'AVAILABLE'";
$stmt_avroom = $DBcon->prepare( $query_avroom );
$stmt_avroom->execute();

if($stmt_avroom->rowCount() > 0) {

    $i = 0;
    while ($row_avroom = $stmt_avroom->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row_avroom["id"];
        extract($row_avroom);
    ?>
    <option value="<?php echo $row_avroom['id']?>"><?php echo $row_avroom['room_name'] . '(' . $row_avroom['rate'] . '/Day)'?></option>
    <?php
    }
}
?>