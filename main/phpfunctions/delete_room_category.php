<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/16/2018
 * Time: 10:29 PM
 */
header('Content-type: application/json; charset=UTF-8');
$response = array();

if ($_POST['delete']) {

    $DBhost = "localhost";
    $DBuser = "root";
    $DBpass = "";
    $DBname = "hotel_db";

    try {
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname",$DBuser,$DBpass);
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $ex){
        die($ex->getMessage());
    }

    $pid = intval($_POST['delete']);
    $query = "DELETE FROM tbl_room_category WHERE id =:pid";
    $stmt = $DBcon->prepare( $query );
    $stmt->execute(array(':pid'=>$pid));

    if ($stmt) {
        $response['status']  = 'success';
        $response['message'] = 'Product Deleted Successfully ...';
    } else {
        $response['status']  = 'error';
        $response['message'] = 'Unable to delete product ...';
    }
    echo json_encode($response);
}
?>

