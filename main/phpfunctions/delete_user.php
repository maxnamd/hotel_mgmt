<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 11:15 PM
 */

header('Content-type: application/json; charset=UTF-8');
$response = array();

if ($_POST['delete']) {

    $DBhost = "localhost";
    $DBuser = "root";
    $DBpass = "";
    $DBname = "hotel_db";

    try {
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname",$DBuser,$DBpass);
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $ex){
        die($ex->getMessage());
    }

    $rid = intval($_POST['delete']);
    $query = "DELETE FROM tbl_room_list WHERE id =:rid";
    $stmt = $DBcon->prepare( $query );
    $stmt->execute(array(':rid'=>$rid));

    if ($stmt) {
        $response['status']  = 'success';
        $response['message'] = 'Room Deleted Successfully ...';
    } else {
        $response['status']  = 'error';
        $response['message'] = 'Unable to delete the selected room ...';
    }
    echo json_encode($response);
}
?>