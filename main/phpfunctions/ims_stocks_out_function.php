<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/28/2018
 * Time: 7:14 PM
 */

include "connect.php";
$query = "SELECT * FROM ims_tbl_stocks WHERE category = '$what_cat'";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row_stocks=$stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id_stock = $row_stocks["id"];
        extract($row_stocks);
        ?>
        <tr>
            <td><?php echo $row_stocks["name"]; ?></td>
            <td><?php echo $row_stocks["category"]; ?></td>
            <td><?php echo $row_stocks["quantity"]; ?></td>
        </tr>
        <?php
        $i++; }

}
?>
<?php
if(isset($_POST["btnRelease"])){
    $stock_name = $_POST["stock_name"];
    $w_quantity = $_POST["quantity"];
    $requested_by = $_POST["requested_by"];
    $purpose = $_POST["purpose"];

    $query_fetch_selected= "SELECT * FROM ims_tbl_stocks WHERE name = '$stock_name'";
    $stmt_selected = $DBcon->prepare( $query_fetch_selected );
    $stmt_selected->execute();
    $result = $stmt_selected -> fetch();
    $quantity_left = $result ["quantity"];

    if($w_quantity <= 0 || $quantity_left < $w_quantity){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'Check the quantity!',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks_out.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks_out.php\";
                    }
                  }
                )
			</script>
		";
    }
    elseif($stock_name == "" || empty($stock_name)){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'Select a stock!',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks_out.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks_out.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{


        $query_history= "INSERT INTO ims_stock_history (activity, stock_name, supplier, quantity, user, care_of, acquisition_type) VALUES ('$username release a ($w_quantity)$stock_name','$stock_name','N/A','$w_quantity','$username','$requested_by','OUT')";
        $stmt_history = $DBcon->prepare( $query_history );
        $stmt_history->execute();

        $new_quantity = $quantity_left - $w_quantity;

        $query_update_stock = "UPDATE ims_tbl_stocks SET quantity = '$new_quantity' WHERE name = '$stock_name'";
        $stmt_update_stock= $DBcon->prepare($query_update_stock);
        $stmt_update_stock->execute();

        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: '$new_quantity stocks remaining to $stock_name!',
                  type: \"success\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"stocks_out.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"stocks_out.php\";
                    }
                  }
                )
			</script>
		";
    }

}
?>
