<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/21/2018
 * Time: 9:57 PM
 */

$DBhost = "localhost";
$DBuser = "root";
$DBpass = "";
$DBname = "hotel_db";

try {
    $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname",$DBuser,$DBpass);
    $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $ex){
    die($ex->getMessage());
}
?>