<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 12:14 AM
 */

include "connect.php";
$query = "SELECT * FROM ims_tbl_stocks WHERE category = '$what_cat'";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row_stocks=$stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id_stock = $row_stocks["id"];
        $stname = $row_stocks["name"];

        extract($row_stocks);
        ?>
        <tr>
            <td><?php echo $row_stocks["name"]; ?></td>
            <td><?php echo $row_stocks["category"]; ?></td>
            <td><?php echo $row_stocks["quantity"]; ?></td>
            <td class="text-center">
                <?php

                $query_test_stock = "SELECT * FROM ims_stock_history WHERE stock_name = '$stname'";
                $stmt_test_stock = $DBcon->prepare( $query_test_stock );
                $stmt_test_stock->execute();
                $result_test = $stmt_test_stock->rowCount();

                if($result_test == 0){
                    echo "<a class='label label-danger'>NO HISTORY YET</a>";
                }
                else{ ?>
                    <a class="label label-table label-inverse" href="reports_stock_history.php?stockID=<?php echo $my_id_stock; ?>">History</a>
                <?php }?>
            </td>
        </tr>
        <?php
        $i++; }

}
else{
    echo "<tr><td colspan='3' class='font-bold text-center'>NO DATA AVAILABLE</td></tr>";
}
?>
