<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/25/2018
 * Time: 10:37 PM
 */

include "connect.php";
$query_log = "SELECT * FROM tbl_logs ORDER BY timestamp DESC";
$stmt_log = $DBcon->prepare( $query_log );
$stmt_log ->execute();


if($stmt_log->rowCount() > 0) {

    while($row_logs=$stmt_log->fetch(PDO::FETCH_ASSOC)) {
        extract($row_logs);
        ?>
        <tr>
            <td>
                <?php echo $row_logs["activity"];?>
            </td>

            <td>
                <?php echo $row_logs["user"];?>
            </td>

            <td>
                <?php echo $row_logs["timestamp"];?>
            </td>
        </tr>
        <?php
    }
}
?>