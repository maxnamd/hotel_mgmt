<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 10:48 PM
 */

include "connect.php";
if(isset($_POST['btnPay'])){
    $userid = $username;
    $guest_tendered = $_POST['guest_tendered'];
    $guest_change = $guest_tendered-$guest_total_payable;
    $date = date('Y/m/d');

    $query_payment = "INSERT INTO tbl_activities (act_name, act_status, floor_position, room_name, room_type, room_rate, checkin_date, checkin_time, checkout_date, checkout_time, guest_av_days, room_charges, transaction_date, transaction_number, guest_discount, guest_total_addons, guest_total_payable_wo_discount, guest_total_payable_w_discount, guest_cash, guest_change, guest_name, guest_gender, guest_address, guest_id_type, guest_id_number, guest_phone_number,clerk) VALUES ('CHECKIN','ONGOING','$sr_floor','$sr_name','$sr_type','$sr_rate','$guest_checkin_date','$guest_checkin_time','$guest_checkout_date','$guest_checkout_time','$guest_av_days','$room_charges','$date','$guest_trasaction_id','$guest_discount','$guest_total_addons','$plain_total_payable','$guest_total_payable','$guest_tendered','$guest_change','$guest_name', '$guest_gender', '$guest_address', '$guest_id_type', '$guest_id_number', '$guest_phone_number','$userid')";
    $stmt_payment = $DBcon->prepare( $query_payment );
    $stmt_payment->execute();

    $query_transfer = "INSERT INTO tbl_activities_addons SELECT * FROM tbl_activities_addons_temp WHERE transaction_number = '$guest_trasaction_id'";
    $stmt_transfer = $DBcon->prepare( $query_transfer );
    $stmt_transfer->execute();


    $query_clean_tno = "DELETE FROM tbl_transaction_number WHERE t_number NOT IN(SELECT transaction_number FROM tbl_activities WHERE transaction_number IS NOT NULL)";
    $stmt_clean = $DBcon->prepare( $query_clean_tno );
    $stmt_clean->execute();

    $query_clean_1 = "DELETE FROM tbl_activities_addons_temp WHERE transaction_number NOT IN(SELECT transaction_number FROM tbl_activities WHERE transaction_number IS NOT NULL)";
    $stmt_clean_1 = $DBcon->prepare( $query_clean_1 );
    $stmt_clean_1->execute();

    $query_ao_set_status = "UPDATE tbl_activities_addons SET paid_status = 'YES' WHERE transaction_number = '$guest_trasaction_id'";
    $stmt_ao_set_status = $DBcon->prepare( $query_ao_set_status );
    $stmt_ao_set_status->execute();

    /*temp_resID*/
    $temp_resID = $_SESSION["temp_resID"];
    $query_clean_reservation = "UPDATE tbl_reservations SET reservation_status = 'done' WHERE id = '$temp_resID'";
    $stmt_clean_res = $DBcon->prepare( $query_clean_reservation );
    $stmt_clean_res->execute();

    $query_set_status = "UPDATE tbl_room_list SET status = 'OCCUPIED' WHERE room_name = '$sr_name'";
    $stmt_set_status = $DBcon->prepare( $query_set_status );
    $stmt_set_status->execute();

    $query_populate = "INSERT INTO tbl_activities_unbilled_charges (room_charges, addon_charges, total_unbilled_charges, transaction_number, guest_cash, guest_change, paid_status) VALUES ('0','0','0','$guest_trasaction_id','0','0','NO')";
    $stmt_populate = $DBcon->prepare( $query_populate );
    $stmt_populate->execute();

    $query_log = "INSERT INTO tbl_logs (user,activity) VALUES ('$username','Successfully checked-in a guest from reservation')";
    $stmt_log = $DBcon->prepare( $query_log );
    $stmt_log ->execute();

    echo "
    <script type='text/javascript'>
            swal({
              title: 'SUCCESS!',
              text: \"Room has been checked-in!\",
              type: \"success\",
              timer: 2000,
            }).then(
              function() {
            // Redirect the user
            window.location.href = \"checkin_reservation_process_4_final.php\";
            console.log('The Ok Button was clicked.');
            },
              // handling the promise rejection
              function (dismiss) {
                if (dismiss === 'timer') {
                   window.location.href = \"checkin_reservation_process_4_final.php\";
                }
              }
            )
    </script>";
}
?>