<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 12:14 AM
 */

include "connect.php";
$query = "SELECT * FROM tbl_room_list";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row_rooms=$stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row_rooms["id"];
        extract($row_rooms);
        ?>

        <script>
            function getRoomRate_edit_<?php echo $i?>(val) {
                $.ajax({
                    type: "POST",
                    url: "phpfunctions/ajax_getRoomRate_edit.php",
                    data:'room_category='+val,
                    success: function(data){
                        $('#room_rate_edit_<?php echo $i?>').val(data);
                    }
                });
            }
        </script>
        <tr>
            <td><?php echo $row_rooms["floor_position"]; ?></td>
            <td><?php echo $row_rooms["room_name"]; ?></td>
            <td><?php echo $row_rooms["room_type"]; ?></td>
            <td><?php echo $row_rooms["rate"]; ?></td>
            <td>
                <center>
                    <a href="#editModal_<?php echo$i?>" class="col-sm-4 btn btn-md btn-default" title="Edit Menu" data-toggle="modal" data-id='"<?php echo $i?>"'> <i class="glyphicon glyphicon-edit"></i></a>
                    <a class="col-sm-4 btn btn-md btn-danger" id="delete_room" data-id="<?php echo $my_id; ?>" href="javascript:void(0)"><i class="glyphicon glyphicon-trash"></i></a>
                </center>

                <div class="modal fade" id="editModal_<?php echo$i?>" tabindex="-1" role="dialog" aria-labelledby="editModal_<?php echo$i?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">Edit Room: <span class="font-weight-bold"><?php echo $row_rooms["room_name"];?></span></h4>
                            </div>
                            <div class="modal-body">
                                <form method="post">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Room Floor:</label>
                                        <select class="form-control" data-error="INSERT ROOM NUMBER" name="floor_position" required>
                                            <option value="<?php echo $row_rooms["floor_position"];?>"><?php echo $row_rooms["floor_position"];?></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                        <input type="hidden" name="floor_position_hidden" value="<?php echo $row_rooms["floor_position"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Room Name:</label>
                                        <input type="text" class="form-control" id="recipient-name1" value="<?php echo $row_rooms["room_name"];?>" name="room_name"/>
                                        <input type="hidden" name="room_name_hidden" value="<?php echo $row_rooms["room_name"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Room Type:</label>

                                        <input type="hidden" name="room_category_hidden" value="<?php echo $row_rooms["room_category"];?>"/>
                                        <select class="form-control" name="room_category" id="room_category" onChange="getRoomRate_edit_<?php echo $i?>(this.value);" data-error="PLEASE CHOOSE A ROOM TYPE" required>
                                            <option value="<?php echo $row_rooms["room_type"];?>"> <?php echo $row_rooms["room_type"];?> </option>
                                            <?php
                                            $query = "SELECT * FROM tbl_room_category";
                                            $stmt_grrE = $DBcon->prepare( $query );
                                            $stmt_grrE->execute();

                                            if($stmt_grrE->rowCount() > 0) {


                                                while ($row_cat = $stmt_grrE->fetch(PDO::FETCH_ASSOC)) {
                                                    $my_id = $row_cat["id"];
                                                    extract($row_cat);
                                                    ?>
                                                    <option value="<?php echo $row_cat['room_category']; ?>"><?php echo $row_cat['room_category']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="room_rate_edit" class="control-label">Room Rate:</label>
                                        <input class="form-control" id="room_rate_edit_<?php echo $i?>" value="<?php echo $row_rooms["rate"];?>" name="room_rate_edit" readonly/>
                                        <input type="hidden" name="room_rate_hidden" value="<?php echo $row_rooms["rate"];?>"/>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $row_rooms["id"];?>"/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button name="btnUpdateRoom" type="submit" class="btn btn-success">UPDATE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
        <?php
        $i++; }

}
?>
<?php
if(isset($_POST['btnUpdateRoom'])){

    $id = $_POST['id'];
    $room_category = $_POST['room_category'];
    $room_rate_edit = $_POST['room_rate_edit'];
    $floor_position = $_POST['floor_position'];
    $room_name = $_POST['room_name'];

    $room_category_old = $_POST['room_category_hidden'];
    $room_rate_old = $_POST['room_rate_hidden'];
    $floor_position_old = $_POST['floor_position_hidden'];
    $room_name_old = $_POST['room_name_hidden'];

    if($room_category==$room_category_old && $room_rate_edit<=$room_rate_old && $floor_position==$floor_position_old && $room_name==$room_name_old){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'There is an error updating the room category. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRooms.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRooms.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $sql = "UPDATE tbl_room_list SET floor_position = '$floor_position',room_name = '$room_name',room_type = '$room_category',rate = '$room_rate_edit' WHERE id = '$id'";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"Room Updated!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRooms.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRooms.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {

            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'There is an error updating the room. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRooms.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRooms.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>

