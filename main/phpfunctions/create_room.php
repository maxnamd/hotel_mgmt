<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 10:53 PM
 */


if(isset($_POST['btnAddRoom'])){

    $room_name = $_POST['room_name'];
    $room_rate = $_POST['room_rate'];
    $floor_position = $_POST['floor_position'];
    $room_category = $_POST['room_category'];


    $sql = "INSERT INTO tbl_room_list (floor_position,room_name,room_type,rate,status)VALUES ('$floor_position','$room_name','$room_category','$room_rate','AVAILABLE')";
    if ($conn->query($sql) === TRUE) {
        echo"
        <script type='text/javascript'>
            
            swal({
              title: 'SUCCESS!',
              text: \"New Room Added!\",
              type: \"success\",
              timer: 2000,
            }).then(
              function() {
            // Redirect the user
            window.location.href = \"manageRooms.php\";
            console.log('The Ok Button was clicked.');
            },
              // handling the promise rejection
              function (dismiss) {
                if (dismiss === 'timer') {
                   window.location.href = \"manageRooms.php\";
                }
              }
            )
        </script>
    ";
    }
    else {
        echo"
        <script type='text/javascript'>
            
            swal({
              title: 'ERROR IN QUERY!',
              text: 'No Room Added. Please Try Again',
              type: \"error\",
              timer: 10000,
            }).then(
              function() {
            // Redirect the user
            window.location.href = \"manageRooms.php\";
            console.log('The Ok Button was clicked.');
            },
              // handling the promise rejection
              function (dismiss) {
                if (dismiss === 'timer') {
                   window.location.href = \"manageRooms.php\";
                }
              }
            )
        </script>
    ";
    }

}
?>