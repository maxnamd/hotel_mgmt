<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 3/1/2018
 * Time: 11:41 PM
 */
?>

<?php

include "../phpfunctions/connect.php";
$query_history = "SELECT * FROM ims_stock_history ORDER BY timestamp DESC";
$stmt_history = $DBcon->prepare( $query_history );
$stmt_history->execute();

if($stmt_history->rowCount() > 0) {

    while($row_history=$stmt_history->fetch(PDO::FETCH_ASSOC)) {
        extract($row_history);
        ?>
        <tr>
            <td>
                <?php
                if($row_history["acquisition_type"]=="IN"){
                    echo "<span class=\"label label-table label-success\">IN</span>";
                }
                elseif ($row_history["acquisition_type"]=="OUT"){
                    echo "<span class=\"label label-table label-inverse\">OUT</span>";
                }
                ?>
            </td>
            <td><?php echo $row_history["stock_name"]; ?></td>
            <td><?php echo $row_history["quantity"]; ?></td>
            <td>
                <?php
                if($row_history["acquisition_type"]=="IN"){
                    echo $row_history["supplier"];
                }
                else{
                    ECHO "N/A";
                }
                ?>
            </td>
            <td>
                <?php
                if($row_history["acquisition_type"]=="OUT"){
                    echo $row_history["care_of"];
                }
                else{
                    ECHO "N/A";
                }
                ?>
            </td>
            <td><?php echo $row_history["timestamp"]; ?></td>

        </tr>
        <?php
    }
}
else{
    echo "<tr><td colspan='4' class='text-center font-bold'>NO DATA AVAILABLE</td></tr>";
}
?>
