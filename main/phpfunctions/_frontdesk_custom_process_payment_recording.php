<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/9/2018
 * Time: 10:24 PM
 */

include "connect.php";
if(isset($_POST['btnPay'])){
    $userid = $username;
    $guest_tendered = $_POST['guest_tendered'];
    $guest_change = $guest_tendered-$total;
    $date = date('Y/m/d');



    if($_SESSION["od_days"]>1){
        $od = $_SESSION["total_overdue"];
        $new_total = $total;
        $query_payment = "UPDATE tbl_activities_unbilled_charges SET overdue = '$od',total_unbilled_charges = '$new_total',guest_cash = '$guest_tendered', guest_change = '$guest_change',transaction_date = '$date' WHERE transaction_number = '$transaction_id'";
        $stmt_payment = $DBcon->prepare( $query_payment );
        $stmt_payment->execute();
    }
    else{
        $query_payment = "UPDATE tbl_activities_unbilled_charges SET guest_cash = '$guest_tendered', guest_change = '$guest_change',transaction_date = '$date' WHERE transaction_number = '$transaction_id'";
        $stmt_payment = $DBcon->prepare( $query_payment );
        $stmt_payment->execute();
    }

    $query_clean_tno = "DELETE FROM tbl_transaction_number WHERE t_number NOT IN(SELECT transaction_number FROM tbl_activities WHERE transaction_number IS NOT NULL)";
    $stmt_clean = $DBcon->prepare( $query_clean_tno );
    $stmt_clean->execute();

    $query_set_status_1 = "UPDATE tbl_room_list SET status = 'AVAILABLE' WHERE room_name = '$a_room_name'";
    $stmt_set_status_1 = $DBcon->prepare( $query_set_status_1 );
    $stmt_set_status_1->execute();

    $query_set_status_2 = "UPDATE tbl_activities SET act_status = 'DONE' WHERE transaction_number = '$transaction_id'";
    $stmt_set_status_2 = $DBcon->prepare( $query_set_status_2 );
    $stmt_set_status_2->execute();

    $query_set_status_3 = "UPDATE tbl_activities_unbilled_charges SET paid_status = 'YES' WHERE transaction_number = '$transaction_id'";
    $stmt_set_status_3 = $DBcon->prepare( $query_set_status_3 );
    $stmt_set_status_3->execute();

    $query_ao_set_status = "UPDATE tbl_activities_addons SET paid_status = 'YES' WHERE transaction_number = '$transaction_id'";
    $stmt_ao_set_status = $DBcon->prepare( $query_ao_set_status );
    $stmt_ao_set_status->execute();


    unset($_SESSION['total_overdue']);
    unset($_SESSION['od_days']);

    echo "
    <script type='text/javascript'>
            
            swal({
              title: 'SUCCESS!',
              text: \"THANKS!\",
              type: \"success\",
              timer: 2000,
            }).then(
              function() {
            // Redirect the user
            window.location.href = \"checkin_custom_process_final.php\";
            console.log('The Ok Button was clicked.');
            },
              // handling the promise rejection
              function (dismiss) {
                if (dismiss === 'timer') {
                   window.location.href = \"checkin_custom_process_final.php\";
                }
              }
            )
    </script>";
}
?>