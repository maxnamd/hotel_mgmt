<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/17/2018
 * Time: 12:14 AM
 */

include "connect.php";
$query = "SELECT * FROM tbl_users WHERE userlevel != 'MANAGER'";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row_users=$stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row_users);
        ?>

        <script>
            function getRoomRate_edit_<?php echo $i?>(val) {
                $.ajax({
                    type: "POST",
                    url: "phpfunctions/ajax_getRoomRate_edit.php",
                    data:'room_category='+val,
                    success: function(data){
                        $('#room_rate_edit_<?php echo $i?>').val(data);
                    }
                });
            }
        </script>
        <tr>
            <td><?php echo $row_users["username"]; ?></td>
            <td><?php echo $row_users["first_name"] . ' ' . $row_users["last_name"]; ?></td>
            <td><?php echo $row_users["email"]; ?></td>
            <td><?php echo $row_users["userlevel"]; ?></td>
            <td>
                <center>
                    <a href="#editModal_<?php echo$i?>" class="col-sm-4 btn btn-md btn-default" title="Edit Menu" data-toggle="modal" data-id='"<?php echo $i?>"'> <i class="glyphicon glyphicon-edit"></i></a>
                    <a class="col-sm-4 btn btn-md btn-danger" id="delete_room" data-id="<?php echo $my_id; ?>" href="javascript:void(0)"><i class="glyphicon glyphicon-trash"></i></a>
                </center>

                <div class="modal fade" id="editModal_<?php echo$i?>" tabindex="-1" role="dialog" aria-labelledby="editModal_<?php echo$i?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">Edit User: <span class="font-weight-bold"><?php echo $row_users["username"];?></span></h4>
                            </div>
                            <div class="modal-body">
                                <form method="post">

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">First Name:</label>
                                        <input type="text" class="form-control" value="<?php echo $row_users["first_name"];?>" name="first_name"/>
                                        <input type="hidden" name="room_name_hidden" value="<?php echo $row_users["first_name"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Last Name:</label>
                                        <input type="text" class="form-control" value="<?php echo $row_users["last_name"];?>" name="last_name"/>
                                        <input type="hidden" name="last_name_hidden" value="<?php echo $row_users["last_name"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Email:</label>
                                        <input type="text" class="form-control" value="<?php echo $row_users["email"];?>" name="email"/>
                                        <input type="hidden" name="email_hidden" value="<?php echo $row_users["email"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">User Type:</label>

                                        <input type="hidden" name="userlevel_hidden" value="<?php echo $row_users["userlevel"];?>"/>
                                        <select class="form-control" name="userlevel" id="room_category" data-error="PLEASE CHOOSE A USER TYPE" required>
                                            <option value="<?php echo $row_users["userlevel"];?>"> <?php echo $row_users["userlevel"];?> </option>
                                            <?php
                                            $myvar = $row_users["userlevel"];
                                            $query = "SELECT * FROM tbl_users WHERE userlevel != 'MANAGER' AND userlevel !=  '$myvar' GROUP BY userlevel";
                                            $stmt_ul = $DBcon->prepare( $query );
                                            $stmt_ul->execute();

                                            if($stmt_ul->rowCount() > 0) {


                                                while ($row_ul = $stmt_ul->fetch(PDO::FETCH_ASSOC)) {
                                                    extract($row_ul);
                                                    ?>
                                                    <option value="<?php echo $row_ul['userlevel']; ?>"><?php echo $row_ul['userlevel']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $row_users["id"];?>"/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button name="btnUpdate" type="submit" class="btn btn-success">UPDATE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
        <?php
        $i++; }

}
?>
<?php
if(isset($_POST['btnUpdate'])){

    $id = $_POST['id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $userlevel = $_POST['userlevel'];
    $email = $_POST['email'];

    $first_name_old = $_POST['first_name_hidden'];
    $last_name_old = $_POST['last_name_hidden'];
    $userlevel_old = $_POST['userlevel_hidden'];
    $email_old = $_POST['email_hidden'];

    if($first_name==$first_name_old && $last_name==$last_name_old && $userlevel==$userlevel_old && $email==$email_old){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'There is an error updating user. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageUsers.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageUsers.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $sql = "UPDATE tbl_users SET first_name = '$first_name',last_name = '$last_name',email = '$email',userlevel = '$userlevel' WHERE id = '$id'";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"Room Updated!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageUsers.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageUsers.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {

            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'There is an error updating user. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageUsers.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageUsers.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>

