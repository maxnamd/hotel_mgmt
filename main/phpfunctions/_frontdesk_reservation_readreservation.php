<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/4/2018
 * Time: 4:36 PM
 */

include "connect.php";
$query_res = "SELECT * FROM tbl_reservations WHERE reservation_status != 'trash' ORDER BY reservation_status DESC ";
$stmt_res = $DBcon->prepare( $query_res );
$stmt_res->execute();

if($stmt_res->rowCount() > 0) {

    $i = 0;
    while ($row_res = $stmt_res->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row_res["id"];
        extract($row_res);
        ?>
        <tr>
            <td><?php echo $row_res["room_name"];?></td>
            <td><?php echo $row_res["checkin_date"];?></td>
            <td><?php echo $row_res["checkout_date"];?></td>
            <td><?php echo $row_res["guest_name"];?></td>
            <td><?php echo $row_res["guest_phone"];?></td>

            <td>
                <?php
                if($row_res["reservation_status"] == "done"){
                echo "<span class=\"label label-table label-success\">DONE</span>";
                }
                else{
                    echo "<span class=\"label label-table label-danger\">TO BE RESERVE</span>";
                }
                ?>
            </td>
            <td>
                <?php
                if($row_res["reservation_status"] == "done"){
                    echo "<button type='button' class='btn btn-block btn-default'>DONE</button>";
                }
                else{
                    ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="checkin_reservation_process_1.php?resID=<?php echo $row_res['id']; ?>" class="btn btn-default btn-block"> <i class="fa fa-toggle-left"> </i> Book Now </a>
                        </div>

                        <div class="col-sm-6">
                            <form method="post">
                                <input type="hidden" name="hid_id" value="<?php echo $row_res['id'];?>"/>
                                <button type="button" class="btn btn-danger btn-block" onclick="clickDelete(this.value);"  data-id="<?php echo $row_res['id']?>" value="<?php echo $row_res['id']?>"> <i class="fa fa-trash"> </i>Delete</button>
                            </form>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </td>
            <td><?php echo $row_res["guest_address"];?></td>
            <td><?php echo $row_res["guest_id_type"];?></td>
            <td><?php echo $row_res["guest_id_number"];?></td>
        </tr>
        <?php
    }
}
else{
    echo "<tr><td></td><td colspan='1'><td colspan='4'>NO RESERVATIONS YET</td></td></tr>";
}
?>
<script>
    function clickDelete(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ajax_trash_reservation.php",
            data:'res_id='+val,
            success: function(data,response){
                swal('Deleted!', response.message, response.status);
                window.setTimeout(function(){
                    window.location.href = "reservation.php";
                }, 2000);
            }
        });
    }
</script>
