<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/22/2018
 * Time: 10:35 PM
 */
$query_res_quick = "SELECT * FROM tbl_reservations WHERE reservation_status != 'trash' AND reservation_status != 'done' ORDER BY reservation_status DESC ";
$stmt_res_quick = $DBcon->prepare( $query_res_quick );
$stmt_res_quick->execute();

if($stmt_res_quick->rowCount() > 0) {

$i = 0;
while ($row_res_quick = $stmt_res_quick->fetch(PDO::FETCH_ASSOC)) {
    extract($row_res_quick);
    ?>
    <tr>
        <td>
            <i class="fa fa-user"> </i> <?php echo $row_res_quick["guest_name"];?>
            <a href="checkin_reservation_process_1.php?resID=<?php echo $row_res_quick['id']; ?>" class="btn btn-default btn-block"> <i class="fa fa-toggle-left"> </i> Book Now </a>
            <br>
            <i class="fa fa-phone"> </i> <?php echo $row_res_quick["guest_phone"];?>
            <br>
            <i class="fa fa-bed"> </i> <?php echo $row_res_quick["room_name"];?>
            <br>
            <i class="fa fa-calendar"> </i> Check-in Date: <?php echo $row_res_quick["checkin_date"];?>
            <br>
            <i class="fa fa-calendar"> </i> Check-out Date: <?php echo $row_res_quick["checkout_date"];?>
        </td>
    </tr>
    <?php
    }
}
else{
    echo "<tr><td class='font-bold text-center' colspan='1'>NO RESERVATIONS YET</td></tr>";
}
?>
