<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/7/2018
 * Time: 7:42 PM
 */

include "connect.php";
$query_addons = "SELECT * FROM tbl_room_addons";
$stmt_addons = $DBcon->prepare( $query_addons );
$stmt_addons ->execute();


if($stmt_addons->rowCount() > 0) {

    while($row_addons=$stmt_addons->fetch(PDO::FETCH_ASSOC)) {
        $aid = $row_addons["id"];
        extract($row_addons);
        ?>



        <tr>
            <td><?php echo $row_addons["ao_name"]?></td>
            <td><?php echo $row_addons["ao_cost"]?></td>
            <td>
                <form method="post">
                    <input type="hidden" name="ao_name" value="<?php echo $row_addons['ao_name']?>"/>
                    <input type="hidden" name="ao_cost" value="<?php echo $row_addons['ao_cost']?>"/>
                    <input type="hidden" name="ao_stock" value="<?php echo $row_addons['ao_stock']?>"/>
                    <button type="button" class="btn btn-sm btn-default btnInsertAO" onclick="insertAddOn(this.value);" id="btnInsertAO_" data-id="<?php echo $row_addons['id']?>" value="<?php echo $row_addons['id']?>" name="btnInsertAO"> <i class="fa fa-plus"></i></button>
                </form>
            </td>
        </tr>
        <?php
    }
}
?>
<script>
    function insertAddOn(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ajax_frontdesk_cp_insert_ao.php",
            data:'ao_id='+val,
            success: function(data){
                $("#tbl_aos").load(" #tbl_aos");
            }
        });
    }
</script>
