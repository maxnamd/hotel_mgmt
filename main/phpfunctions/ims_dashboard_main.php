<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/26/2018
 * Time: 12:04 AM
 */

include "connect.php";
$query_fetch_stats_total= "SELECT * FROM ims_tbl_stocks GROUP BY name";
$stmt_stats_total = $DBcon->prepare( $query_fetch_stats_total );
$stmt_stats_total->execute();

$query_fetch_stats_total_released= "SELECT SUM(quantity) AS total_q FROM ims_tbl_stocks";
$stmt_stats_total_released = $DBcon->prepare( $query_fetch_stats_total_released );
$stmt_stats_total_released->execute();
$result = $stmt_stats_total_released -> fetch();

$query_fetch_low_stock= "SELECT * FROM ims_tbl_stocks WHERE quantity <= 5 GROUP BY name";
$stmt_stats_low = $DBcon->prepare( $query_fetch_low_stock );
$stmt_stats_low->execute();

$count_all = $stmt_stats_total->rowCount();
$total_quantity = $result ["total_q"];
$count_low = $stmt_stats_low->rowCount();
/*while($row_stats=$stmt_stats->fetch(PDO::FETCH_ASSOC)) {
    extract($row_stats);
    echo "";
}*/
?>

<script>

    $(document).ready(function() {

        $('#calendar').fullCalendar({
            defaultDate: '2018-03-12',
            editable: true,
            eventLimit: true, // allow "more" link when too many events
        });

    });

</script>
<style>

    body {
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    }

    #calendar {
        margin: 0 auto;
    }

</style>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-success">
            <div class="panel-heading">

                <a href="product.php" style="text-decoration:none;color:black;">
                    Total Stock Types
                    <span class="badge pull pull-right"><?php echo $count_all; ?></span>
                </a>

            </div> <!--/panel-hdeaing-->
        </div> <!--/panel-->
    </div> <!--/col-md-4-->

    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <a href="orders.php?o=manord" style="text-decoration:none;color:black;">
                    Total Stocks
                    <span class="badge pull pull-right"><?php echo $total_quantity; ?></span>
                </a>

            </div> <!--/panel-hdeaing-->
        </div> <!--/panel-->
    </div> <!--/col-md-4-->

    <div class="col-md-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <a href="product.php" style="text-decoration:none;color:black;">
                    Low Stock
                    <span class="badge pull pull-right"><?php echo $count_low; ?></span>
                </a>

            </div> <!--/panel-hdeaing-->
        </div> <!--/panel-->
    </div> <!--/col-md-4-->

    <div class="col-md-4">
        <div class="card">
            <div class="cardHeader">
                <h1><?php echo date('d'); ?></h1>
            </div>

            <div class="cardContainer">
                <p><?php echo date('l') .' '.date('d').', '.date('Y'); ?></p>
            </div>
        </div>
        <br/>

        <div class="card">
            <div class="cardHeader" style="background-color:#245580;">
                <h1>99999</h1>
            </div>

            <div class="cardContainer">
                <p> <i class="glyphicon glyphicon-usd"></i> Total Revenue</p>
            </div>
        </div>

    </div>

    <div class="col-md-8">
        <div id='calendar'></div>
    </div>

</div>

