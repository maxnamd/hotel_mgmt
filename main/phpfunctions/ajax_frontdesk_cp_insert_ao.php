<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/7/2018
 * Time: 7:46 PM
 */

if(!empty($_POST["ao_id"])) {
    session_start();
    $transaction_id = $_SESSION["actID"];
    $ao_id = $_POST["ao_id"];
    include "connect.php";
    $query = "SELECT * FROM tbl_room_addons WHERE id = '$ao_id'";
    $stmt = $DBcon->prepare( $query );
    $stmt->execute();

    if($stmt->rowCount() > 0) {
        $i = 0;
        while ($row_all_ao = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $my_id = $row_all_ao["id"];
            extract($row_all_ao);
            $selected_addon = $row_all_ao["ao_name"];
            $post_ao_name = $row_all_ao["ao_name"];
            $post_ao_cost = $row_all_ao["ao_cost"];
        }


        //////////////////////////////
        $query_check_ao = "SELECT COUNT(*) AS num_rows FROM tbl_activities_addons WHERE ao_name='{$selected_addon}' AND transaction_number = '$transaction_id' AND paid_status = 'NO' LIMIT 1;";
        $stmt_check_ao = $DBcon->prepare($query_check_ao);
        $stmt_check_ao->execute();
        $check_if_ao_exists = "";


        if ($stmt_check_ao->rowCount() > 0) {

            while ($row_check_addons = $stmt_check_ao->fetch(PDO::FETCH_ASSOC)) {
                extract($row_check_addons);
                $check_if_ao_exists = $row_check_addons["num_rows"];
            }
        }

        if ($check_if_ao_exists > 0){
            $query_fetch_ao_exist = "SELECT * FROM tbl_activities_addons WHERE ao_name = '$selected_addon' AND transaction_number = '$transaction_id' AND paid_status = 'NO'";
            $stmt_fetch_ao_exist = $DBcon->prepare($query_fetch_ao_exist);
            $stmt_fetch_ao_exist->execute();
            if ($stmt_fetch_ao_exist->rowCount() > 0) {
                while ($row_fetch_addons_exist = $stmt_fetch_ao_exist->fetch(PDO::FETCH_ASSOC)) {
                    extract($row_fetch_addons_exist);

                    $ao_exist_name = $row_fetch_addons_exist["ao_name"];
                    $ao_exist_quantity = $row_fetch_addons_exist["ao_quantity"];
                    $ao_exist_cost = $row_fetch_addons_exist["ao_cost"];
                    $ao_exist_total = $row_fetch_addons_exist["ao_total"];
                    $ao_exist_quantity+=1;

                    $total_cost = $ao_exist_quantity * $ao_exist_cost;

                    $query_3 = "UPDATE tbl_activities_addons SET ao_quantity ='$ao_exist_quantity', ao_total = '$total_cost' WHERE ao_name = '$selected_addon' AND transaction_number = '$transaction_id' AND paid_status = 'NO'";
                    $stmt_3 = $DBcon->prepare($query_3);
                    $stmt_3->execute();

                    date_default_timezone_set('Asia/Manila');
                    $date = date('g:ia \o\n l jS F Y');
                    $sentence = "(1 PC) $post_ao_name / $post_ao_cost has been added";
                    $query_hs = "INSERT INTO tbl_activities_history (act_name, transaction_number, date) VALUES ('$sentence','$transaction_id','$date')";
                    $stmt_hs= $DBcon->prepare($query_hs);
                    $stmt_hs->execute();


                    /****************************************************/
                    $query_fetch_details = "SELECT * FROM tbl_activities WHERE transaction_number = '$transaction_id'";
                    $stmt_fetch_details = $DBcon->prepare( $query_fetch_details );
                    $stmt_fetch_details ->execute();


                    if($stmt_fetch_details->rowCount() > 0) {
                        while($row_fetch_details=$stmt_fetch_details->fetch(PDO::FETCH_ASSOC)) {
                            extract($row_fetch_details);

                            $checkin_date = $row_fetch_details["checkin_date"];
                            $checkin_time = $row_fetch_details["checkin_time"];
                            $billed_room_charges = $row_fetch_details["room_charges"];
                            $room_rate = $row_fetch_details["room_rate"];
                            $billed_charges = $row_fetch_details["guest_total_payable_w_discount"];
                        }
                    }
                    // unbilled charges = uc
                    $query_check_uc = "SELECT COUNT(*) AS num_rows FROM tbl_activities_unbilled_charges WHERE transaction_number='{$transaction_id}' LIMIT 1;";
                    $stmt_check_uc = $DBcon->prepare($query_check_uc);
                    $stmt_check_uc->execute();
                    $check_if_uc_exists = "";


                    if ($stmt_check_uc->rowCount() > 0) {

                        while ($row_check_uc = $stmt_check_uc->fetch(PDO::FETCH_ASSOC)) {
                            extract($row_check_uc);
                            $check_if_uc_exists = $row_check_uc["num_rows"];
                        }
                    }

                    if ($check_if_uc_exists > 0) {

                        $query_fetch_uc_exist = "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$transaction_id'";
                        $stmt_fetch_uc_exist = $DBcon->prepare($query_fetch_uc_exist);
                        $stmt_fetch_uc_exist->execute();
                        if ($stmt_fetch_uc_exist->rowCount() > 0) {
                            while ($row_fetch_uc_exist = $stmt_fetch_uc_exist->fetch(PDO::FETCH_ASSOC)) {
                                extract($row_fetch_uc_exist);
                                $f_addon_charges=$row_fetch_uc_exist["addon_charges"];
                                $total_unbilled_charges=$row_fetch_uc_exist["total_unbilled_charges"];
                                $total_unbilled_charges+=$post_ao_cost;
                                $f_addon_charges+=$post_ao_cost;
                                $query_3 = "UPDATE tbl_activities_unbilled_charges SET addon_charges = '$f_addon_charges', total_unbilled_charges = '$total_unbilled_charges' WHERE transaction_number = '$transaction_id'";
                                $stmt_3 = $DBcon->prepare($query_3);
                                $stmt_3->execute();
                            }
                        }
                    }
                    else{
                        $query_save_bill = "INSERT INTO tbl_activities_unbilled_charges (addon_charges, total_unbilled_charges,transaction_number) VALUE ('$post_ao_cost','$post_ao_cost','$transaction_id')";
                        $stmt_save_bill = $DBcon->prepare( $query_save_bill );
                        $stmt_save_bill ->execute();
                    }
                    /****************************************************/

                }
            }
        }
        elseif($check_if_ao_exists == 0){


            $query_3 = "INSERT INTO tbl_activities_addons (ao_name, ao_quantity, ao_cost, ao_total, transaction_number, transaction_date, paid_status, clerk) VALUES ('$post_ao_name','1','$post_ao_cost','$post_ao_cost','$transaction_id','','NO','')";
            $stmt_3 = $DBcon->prepare($query_3);
            $stmt_3->execute();

            date_default_timezone_set('Asia/Manila');
            $date = date('g:ia \o\n l jS F Y');
            $sentence = "(1 PC) $post_ao_name / $post_ao_cost has been added";
            $query_hs = "INSERT INTO tbl_activities_history (act_name, transaction_number, date) VALUES ('$sentence','$transaction_id','$date')";
            $stmt_hs= $DBcon->prepare($query_hs);
            $stmt_hs->execute();


            /****************************************************/
            $query_fetch_details = "SELECT * FROM tbl_activities WHERE transaction_number = '$transaction_id'";
            $stmt_fetch_details = $DBcon->prepare( $query_fetch_details );
            $stmt_fetch_details ->execute();


            if($stmt_fetch_details->rowCount() > 0) {
                while($row_fetch_details=$stmt_fetch_details->fetch(PDO::FETCH_ASSOC)) {
                    extract($row_fetch_details);

                    $checkin_date = $row_fetch_details["checkin_date"];
                    $checkin_time = $row_fetch_details["checkin_time"];
                    $billed_room_charges = $row_fetch_details["room_charges"];
                    $room_rate = $row_fetch_details["room_rate"];
                    $billed_charges = $row_fetch_details["guest_total_payable_w_discount"];
                }
            }
            // unbilled charges = uc
            $query_check_uc = "SELECT COUNT(*) AS num_rows FROM tbl_activities_unbilled_charges WHERE transaction_number='{$transaction_id}' LIMIT 1;";
            $stmt_check_uc = $DBcon->prepare($query_check_uc);
            $stmt_check_uc->execute();
            $check_if_uc_exists = "";


            if ($stmt_check_uc->rowCount() > 0) {

                while ($row_check_uc = $stmt_check_uc->fetch(PDO::FETCH_ASSOC)) {
                    extract($row_check_uc);
                    $check_if_uc_exists = $row_check_uc["num_rows"];
                }
            }

            if ($check_if_uc_exists > 0) {

                $query_fetch_uc_exist = "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$transaction_id'";
                $stmt_fetch_uc_exist = $DBcon->prepare($query_fetch_uc_exist);
                $stmt_fetch_uc_exist->execute();
                if ($stmt_fetch_uc_exist->rowCount() > 0) {
                    while ($row_fetch_uc_exist = $stmt_fetch_uc_exist->fetch(PDO::FETCH_ASSOC)) {
                        extract($row_fetch_uc_exist);
                        $f_addon_charges=$row_fetch_uc_exist["addon_charges"];
                        $total_unbilled_charges=$row_fetch_uc_exist["total_unbilled_charges"];
                        $total_unbilled_charges+=$post_ao_cost;
                        $f_addon_charges+=$post_ao_cost;
                        $query_3 = "UPDATE tbl_activities_unbilled_charges SET addon_charges = '$f_addon_charges', total_unbilled_charges = '$total_unbilled_charges' WHERE transaction_number = '$transaction_id'";
                        $stmt_3 = $DBcon->prepare($query_3);
                        $stmt_3->execute();
                    }
                }
            }
            else{
                $query_save_bill = "INSERT INTO tbl_activities_unbilled_charges (addon_charges, total_unbilled_charges,transaction_number) VALUE ('$post_ao_cost','$post_ao_cost','$transaction_id')";
                $stmt_save_bill = $DBcon->prepare( $query_save_bill );
                $stmt_save_bill ->execute();
            }
            /****************************************************/

        }
    }
    else{
        echo 0;
    }
}
?>