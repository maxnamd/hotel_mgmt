<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/16/2018
 * Time: 11:41 PM
 */
?>
<?php

include "connect.php";
$query = "SELECT * FROM tbl_room_category";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row["id"];
        extract($row);
        ?>
        <tr>
            <td><?php echo $row["room_category"]; ?></td>
            <td><?php echo $row["rate"]; ?></td>
            <td>
                <center>
                    <a href="#editModal_<?php echo$i?>" class="col-sm-4 btn btn-md btn-default" title="Edit Menu" data-toggle="modal" data-id='"<?php echo $row['id'];?>"'> <i class="glyphicon glyphicon-edit"></i></a>
                    <a class="col-sm-4 btn btn-md btn-danger" id="delete_roomcat" data-id="<?php echo $my_id; ?>" href="javascript:void(0)"><i class="glyphicon glyphicon-trash"></i></a>
                </center>

                <div class="modal fade" id="editModal_<?php echo$i?>" tabindex="-1" role="dialog" aria-labelledby="editModal_<?php echo$i?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">Edit Category: <span class="font-weight-bold"><?php echo $row["room_category"];?></span></h4>
                            </div>
                            <div class="modal-body">
                                <form method="post">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Category Name:</label>
                                        <input type="text" class="form-control" id="recipient-name1" value="<?php echo $row["room_category"];?>" name="room_category"/>
                                        <input type="hidden" name="room_category_hidden" value="<?php echo $row["room_category"];?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Category Rate:</label>
                                        <input type="number" class="form-control" id="recipient-name1" value="<?php echo $row["rate"];?>" name="room_rate"/>
                                        <input type="hidden" name="room_rate_hidden" value="<?php echo $row["rate"];?>"/>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $my_id;?>"/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button name="btnUpdateCategory" type="submit" class="btn btn-success">UPDATE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
        <?php
    $i++; }

} else {

    ?>
    <tr>
        <td colspan="3">No Room Category Found</td>
    </tr>
    <?php

}
?>
<?php
if(isset($_POST['btnUpdateCategory'])){

    $id = $_POST['id'];
    $room_category = $_POST['room_category'];
    $room_rate = $_POST['room_rate'];
    $room_category_old = $_POST['room_category_hidden'];
    $room_rate_old = $_POST['room_rate_hidden'];

    if($room_category==$room_category_old && $room_rate==$room_rate_old ){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'There is an error updating the room category. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRoomCategory.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRoomCategory.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $sql = "UPDATE tbl_room_category SET room_category = '$room_category',rate = '$room_rate' WHERE id = '$id'";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"Category Updated!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRoomCategory.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRoomCategory.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {

            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'There is an error updating the room category. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRoomCategory.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRoomCategory.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>
