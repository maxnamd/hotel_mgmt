<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/9/2018
 * Time: 9:29 PM
 */

include "connect.php";
    $query_fetch_unpaid= "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$transaction_id'";
    $stmt_fetch_unpaid = $DBcon->prepare($query_fetch_unpaid);
    $stmt_fetch_unpaid->execute();
    if ($stmt_fetch_unpaid->rowCount() > 0) {
        while ($row_fetch_unpaid = $stmt_fetch_unpaid->fetch(PDO::FETCH_ASSOC)) {
            extract($row_fetch_unpaid);
            $rc=$row_fetch_unpaid["room_charges"];
            $ac=$row_fetch_unpaid["addon_charges"];
            $checkout_date_up=$row_fetch_unpaid["checkout_date"];
            $total=$row_fetch_unpaid["total_unbilled_charges"];

            $guest_cash_up = $row_fetch_unpaid["guest_cash"];
            $guest_change_up = $row_fetch_unpaid["guest_change"];
            $transaction_date_up = $row_fetch_unpaid["transaction_date"];
            $clerk_up = $row_fetch_unpaid["clerk"];

            $query_xx= "SELECT * FROM tbl_activities WHERE transaction_number = '$transaction_id'";
            $stmt_xx = $DBcon->prepare($query_xx);
            $stmt_xx->execute();
            if ($stmt_xx->rowCount() > 0) {
                while ($row_xx = $stmt_xx->fetch(PDO::FETCH_ASSOC)) {
                    extract($row_xx);
                    $checkin_date_p = $row_xx["checkout_date"];
                }
            }

            $no_days = strtotime($checkout_date_up)-strtotime($checkin_date_p);
            $no_days = round($no_days / (60 * 60 * 24));


        }
    }

    $query_fetch_paid= "SELECT * FROM tbl_activities WHERE transaction_number = '$transaction_id'";
    $stmt_fetch_paid = $DBcon->prepare($query_fetch_paid);
    $stmt_fetch_paid->execute();
    if ($stmt_fetch_paid->rowCount() > 0) {
        while ($row_fetch_paid = $stmt_fetch_paid->fetch(PDO::FETCH_ASSOC)) {
            extract($row_fetch_paid);

            $a_room_rate=$row_fetch_paid["room_rate"];
            $a_room_name=$row_fetch_paid["room_name"];
            $a_room_type=$row_fetch_paid["room_type"];
            $a_room_floor=$row_fetch_paid["floor_position"];

            $rc_p=$row_fetch_paid["room_charges"];
            $ac_p=$row_fetch_paid["guest_total_addons"];
            $total_p=$row_fetch_paid["guest_total_payable_w_discount"];
            $no_days=$row_fetch_paid["guest_av_days"];

            $checkout_date=$row_fetch_paid["checkout_date"];
            $checkin_date=$row_fetch_paid["checkin_date"];
            $checkout_time=$row_fetch_paid["checkout_time"];
            $checkin_time=$row_fetch_paid["checkin_time"];

            $guest_cash = $row_fetch_paid["guest_cash"];
            $guest_change = $row_fetch_paid["guest_change"];
            $guest_name = $row_fetch_paid["guest_name"];
            $guest_gender = $row_fetch_paid["guest_gender"];
            $guest_address = $row_fetch_paid["guest_address"];
            $guest_id_type = $row_fetch_paid["guest_id_type"];
            $guest_id_number = $row_fetch_paid["guest_id_number"];
            $guest_phone_number = $row_fetch_paid["guest_phone_number"];
        }
    }

    $guest_discount = 0;
?>