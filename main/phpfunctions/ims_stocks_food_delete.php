<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/26/2018
 * Time: 10:36 PM
 */

header('Content-type: application/json; charset=UTF-8');
$response = array();

if ($_POST['delete']) {
    $_SESSION["tab_no"] = "1";
    $DBhost = "localhost";
    $DBuser = "root";
    $DBpass = "";
    $DBname = "hotel_db";

    try {
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname", $DBuser, $DBpass);
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }

    $pid = intval($_POST['delete']);
    $query_del = "DELETE FROM ims_tbl_stocks WHERE id =:pid";
    $stmt_del = $DBcon->prepare($query_del);
    $stmt_del->execute(array(':pid' => $pid));

    if ($stmt_del) {
        $response['status'] = 'success';
        $response['message'] = 'Stock Deleted Successfully ...';
    } else {
        $response['status'] = 'error';
        $response['message'] = 'Unable to Stock...';
    }
    echo json_encode($response);
}
?>