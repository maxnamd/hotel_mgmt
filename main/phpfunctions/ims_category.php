<!-- .row Manage Room Cat-->
<div class="row white-box">
    <!-- .left side -->
    <div class="col-sm-6 table-responsive">
        <h3 class="box-title"> <i class="fa fa-bed"></i> Stock Categories </h3>
        <table id="tbl_roomCat" class="table display nowrap" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="col-sm-4">Name</th>
                <th class="col-sm-4">User</th>
                <th class="col-sm-4 text-center">Action</th>
            </tr>
            </thead>

            <tbody>
            <?php include "../phpfunctions/ims_category_read.php";?>
            </tbody>
        </table>
    </div>
    <!-- /.left side -->

    <!-- .right side -->
    <div class="col-sm-6">

        <h3 class="box-title">
            <i class="fa fa-plus"></i> Add Category
        </h3>
        <form class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle="validator">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-12">Category Name:</label>
                        <div class="col-sm-12">
                            <input class="form-control" name="category_name" data-error="INSERT CATEGORY NAME" required/>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-12">For User:</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="category_user" id="category_user_<?php echo $i?>" data-error="PLEASE CHOOSE A USER CATEGORY" required>
                                <option value="">--SELECT USER CATEGORY--</option>
                                <?php
                                $query_uc = "SELECT * FROM tbl_users WHERE userlevel != 'MANAGER' AND userlevel != 'CLERK' AND userlevel != 'INVENTORY_ADMIN' GROUP BY userlevel";
                                $stmt_uc = $DBcon->prepare( $query_uc );
                                $stmt_uc->execute();

                                if($stmt_uc->rowCount() > 0) {
                                    while ($row_uc = $stmt_uc->fetch(PDO::FETCH_ASSOC)) {
                                        extract($row_uc);
                                        ?>
                                        <option value="<?php echo $row_uc['userlevel']; ?>"><?php echo $row_uc['userlevel']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-success pull-right" type="submit" name="btnAddCategory">ADD</button>
                </div>
            </div>


        </form>
    </div>
    <!-- /.right side -->



    <script>
        $(document).ready(function(){

            $(document).on('click', '#delete_stockcat', function(e){
                var roomCatId = $(this).data('id');
                SwalDelete(roomCatId);
                e.preventDefault();
            });

        });


        function SwalDelete(roomCatId){
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,

                preConfirm: function() {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url: '../phpfunctions/ims_category_delete.php',
                            type: 'POST',
                            data: 'delete='+roomCatId,
                            dataType: 'json'
                        })
                            .done(function(response){
                                swal('Deleted!', response.message, response.status);
                                window.setTimeout(function(){
                                    window.location.href = "category.php";
                                }, 2000);
                            })
                            .fail(function(){
                                swal('Oops...', 'Something went wrong with ajax !', 'error');
                            });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
</div>
<!-- /.row Manage Room Cat-->
<?php
include "ims_category_insert.php";
?>