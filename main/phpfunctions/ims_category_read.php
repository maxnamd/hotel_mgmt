<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/26/2018
 * Time: 6:57 PM
 */


include "connect.php";
$query = "SELECT * FROM ims_tbl_category";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

if($stmt->rowCount() > 0) {

    $i = 0;
    while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row["id"];
        extract($row);
        ?>
        <tr>
            <td><?php echo $row["name"]; ?></td>
            <td><?php echo $row["category_user"]; ?></td>
            <td>
                <center>
                    <a href="#editModal_<?php echo$i?>" class="col-sm-4 btn btn-md btn-default" title="Edit Menu" data-toggle="modal" data-id='"<?php echo $row['id'];?>"'> <i class="glyphicon glyphicon-edit"></i></a>
                    <a class="col-sm-4 btn btn-md btn-danger" id="delete_stockcat" data-id="<?php echo $my_id; ?>" href="javascript:void(0)"><i class="glyphicon glyphicon-trash"></i></a>
                </center>

                <div class="modal fade" id="editModal_<?php echo$i?>" tabindex="-1" role="dialog" aria-labelledby="editModal_<?php echo$i?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">Edit Category: <span class="font-weight-bold"><?php echo $row["name"];?></span></h4>
                            </div>
                            <div class="modal-body">
                                <form method="post">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Category Name:</label>
                                        <input type="text" class="form-control" id="" value="<?php echo $row["name"];?>" name="name"/>

                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $row["id"];?>"/>
                                    <input type="hidden" value="<?php echo $row["name"];?>" name="stock_category_edit"/>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button name="btnUpdateCategory" type="submit" class="btn btn-success">UPDATE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
        <?php
        $i++; }

} else {

    ?>
    <tr>
        <td colspan="2" class="text-center font-bold">No Stock Category Found</td>
    </tr>
    <?php

}
?>
<?php
if(isset($_POST['btnUpdateCategory'])){

    $id = $_POST['id'];
    $stock_category = $_POST['name'];

    $stock_category_old = $_POST['stock_category_edit'];

    if($stock_category==$stock_category_old){
        $_SESSION["tab_no"] = "2";
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'There is an error updating the stock category. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ims.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"category.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $_SESSION["tab_no"] = "2";
        $sql = "UPDATE ims_tbl_category SET name = '$stock_category' WHERE id = '$id'";
        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"Category Updated!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ims.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"category.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {

            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'There is an error updating the stock category. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"category.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"category.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>

