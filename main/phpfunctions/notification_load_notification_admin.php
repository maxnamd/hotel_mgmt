<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 3/30/2018
 * Time: 10:11 AM
 */

    include "connect.php";
    $query_logs = "SELECT * FROM tbl_logs WHERE user != '$username' ORDER BY timestamp DESC LIMIT 4 ";
    $stmt_logs = $DBcon->prepare( $query_logs );
    $stmt_logs->execute();


    if($stmt_logs->rowCount() > 0) {
        while ($row_log = $stmt_logs->fetch(PDO::FETCH_ASSOC)) {
            extract($row_log);

                $dn_id = $row_log["user"];
                $query_dn  = "SELECT * FROM tbl_users WHERE userlevel != 'MANAGER' AND username = '$dn_id'";
                $stmt_dn = $DBcon->prepare( $query_dn );
                $stmt_dn->execute();
                $result = $stmt_dn->fetch();
                $doerName = $result ["first_name"] . ' ' . $result ["last_name"];

            ?>
            <a href="#">
                <div class="user-img"> <img src="../../plugins/images/users/default.png" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                <div class="mail-contnet">
                    <h5><?php echo $doerName;?></h5>
                    <span class="mail-desc"><?php echo $row_log["activity"];?></span> <span class="time"><?php echo  $row_log["timestamp"]?></span>
                </div>
            </a>
            <?php
        }
    }
?>