<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/26/2018
 * Time: 8:56 PM
 */

header('Content-type: application/json; charset=UTF-8');
$response = array();

if ($_POST['delete']) {

    $_SESSION["tab_no"] = "2";
    $DBhost = "localhost";
    $DBuser = "root";
    $DBpass = "";
    $DBname = "hotel_db";

    try {
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname",$DBuser,$DBpass);
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $ex){
        die($ex->getMessage());
    }

    $pid = intval($_POST['delete']);
    $query = "DELETE FROM ims_tbl_category WHERE id =:pid";
    $stmt = $DBcon->prepare( $query );
    $stmt->execute(array(':pid'=>$pid));

    if ($stmt) {
        $response['status']  = 'success';
        $response['message'] = 'Stock Category Deleted Successfully ...';
    } else {
        $response['status']  = 'error';
        $response['message'] = 'Unable to Stock Category ...';
    }
    echo json_encode($response);
}
?>