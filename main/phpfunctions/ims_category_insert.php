<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/26/2018
 * Time: 7:12 PM
 */


if(isset($_POST['btnAddCategory'])){

    $category_name = $_POST['category_name'];
    $category_user = $_POST['category_user'];

    $query_exist = "SELECT * FROM ims_tbl_category WHERE name = '$category_name' AND category_user = '$category_user'";
    $stmt_exist = $DBcon->prepare( $query_exist );
    $stmt_exist->execute();

    if($stmt_exist->rowCount() >= 1) {
        $_SESSION["tab_no"] = "2";
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'No Stock Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ims.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"category.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        if($category_name==""){
            $_SESSION["tab_no"] = "2";
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'No Stock Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"ims.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"category.php\";
                    }
                  }
                )
			</script>
		";
        }
        else{
            $sql = "INSERT INTO ims_tbl_category (name,category_user)VALUES ('$category_name','$category_user')";

            if ($conn->query($sql) === TRUE) {
                $_SESSION["tab_no"] = "2";
                echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"New Stock Category Added!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"category.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"category.php\";
                    }
                  }
                )
			</script>
		";
            }
            else {
                $_SESSION["tab_no"] = "2";
                echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'No Stock Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"category.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"category.php\";
                    }
                  }
                )
			</script>
		";
            }

        }

    }


}
?>