<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/18/2018
 * Time: 10:29 PM
 */
include "connect.php";
$query = "SELECT * FROM tbl_room_list";
$stmt = $DBcon->prepare( $query );
$stmt->execute();

$query_find_av_rooms = "SELECT * FROM tbl_room_list WHERE status = 'AVAILABLE'";
$query_find_nv_rooms = "SELECT * FROM tbl_room_list WHERE status = 'OCCUPIED'";

$stmt_av  = $DBcon->prepare( $query_find_av_rooms );
$stmt_av->execute();
$stmt_nv  = $DBcon->prepare( $query_find_nv_rooms );
$stmt_nv->execute();

$count_av = $stmt_av->rowCount();
$count_nv = $stmt_nv->rowCount();
$count_all = $stmt->rowCount();

if($stmt->rowCount() > 0) {

    $i = 0;
    while ($row_rooms = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $my_id = $row_rooms["id"];
        extract($row_rooms);


        if ($row_rooms["status"] == "AVAILABLE") {
            $img_src = "../../plugins/images/room_available.png";
        } elseif ($row_rooms["status"] == "OCCUPIED") {
            $img_src = "../../plugins/images/room_occupied.png";
        } elseif ($row_rooms["status"] == "CLEANING") {
            $img_src = "../../plugins/images/room_cleaning.png";
        } elseif ($row_rooms["status"] == "MAINTENANCE") {
            $img_src = "../../plugins/images/room_maintenance.png";
        }
        ?>

        <div class="boxes">
        <figure class="imghvr-reveal-left"><img src="<?php echo $img_src; ?>" alt="example-image" height="120px">
        <figcaption style="">
            <h5>
                <center><strong>FLOOR: <?php echo $row_rooms["floor_position"]; ?></strong></center>
            </h5>
            <center><?php echo $row_rooms["room_type"]; ?>
                <i class="fa fa-money"></i> PHP <?php echo $row_rooms["rate"]; ?></center>
        </figcaption>

        <!--<a href="checkin_process_1.php?srID=<?php /*echo $row_rooms['id']; */ ?>"></a>-->
        <?php
        if ($row_rooms["status"] == "AVAILABLE") {
            ?>
            <a href="checkin_process_1.php?srID=<?php echo $row_rooms['id']; ?>"></a>
            <?php
        } elseif ($row_rooms["status"] == "OCCUPIED") {

            //or means occupied room
            $or_room_name = $row_rooms["room_name"];
            $query_search_or = "SELECT * FROM tbl_activities WHERE room_name = '$or_room_name' AND act_status = 'ONGOING'";
            $stmt_search_or = $DBcon->prepare($query_search_or);
            $stmt_search_or->execute();

            if ($stmt_search_or->rowCount() > 0) {
                $transaction_number = "";
                $i = 0;
                while ($row_search_or = $stmt_search_or->fetch(PDO::FETCH_ASSOC)) {
                    $my_id = $row_search_or["activity_id"];
                    extract($row_search_or);
                    $transaction_number = $row_search_or['transaction_number'];

                }
                ?>
                <a href="checkin_custom_process.php?actID=<?php echo $transaction_number;?>"></a>
                <?php
            }
        }
        ?>
            </figure>

            <textarea onclick="this.focus();this.select()" style="width: 120px;"
                      readonly="readonly"><?php echo $row_rooms["room_name"]; ?></textarea>
            </div>


            <?php
            $i++;


    }


}
else{
    echo "<div class='col-sm-3'></div><div class='col-sm-6'><h1>NO ROOMS YET</h1></div><div class='col-sm-3'></div>";
}
?>

