<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/7/2018
 * Time: 11:04 PM
 */

if(isset($_POST["btnUpdateCheckOut"])){
    $room_rate = "";
    $billed_room_charges = "";
    $billed_charges = "";
    $new_checkout_date = $_POST["checkout_date"];
    $new_checkout_time = $_POST["checkout_time"];

    $query_fetch_details = "SELECT * FROM tbl_activities WHERE transaction_number = '$transaction_id'";
    $stmt_fetch_details = $DBcon->prepare( $query_fetch_details );
    $stmt_fetch_details ->execute();
    if($stmt_fetch_details->rowCount() > 0) {
        while($row_fetch_details=$stmt_fetch_details->fetch(PDO::FETCH_ASSOC)) {
            extract($row_fetch_details);

            $old_checkin_date = $row_fetch_details["checkin_date"];
            $old_checkout_date = $row_fetch_details["checkout_date"];
            $checkin_time = $row_fetch_details["checkin_time"];
            $billed_room_charges = $row_fetch_details["room_charges"];
            $room_rate = $row_fetch_details["room_rate"];
            $billed_charges = $row_fetch_details["guest_total_payable_w_discount"];
        }
    }


    ////////////////////
    // unbilled charges = uc
    $query_check_uc = "SELECT COUNT(*) AS num_rows FROM tbl_activities_unbilled_charges WHERE transaction_number='{$transaction_id}' LIMIT 1;";
    $stmt_check_uc = $DBcon->prepare($query_check_uc);
    $stmt_check_uc->execute();
    $check_if_uc_exists = "";


    if ($stmt_check_uc->rowCount() > 0) {

        while ($row_check_uc = $stmt_check_uc->fetch(PDO::FETCH_ASSOC)) {
            extract($row_check_uc);
            $check_if_uc_exists = $row_check_uc["num_rows"];
        }
    }

    if ($check_if_uc_exists > 0) {

        $query_fetch_uc_exist = "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$transaction_id'";
        $stmt_fetch_uc_exist = $DBcon->prepare($query_fetch_uc_exist);
        $stmt_fetch_uc_exist->execute();
        if ($stmt_fetch_uc_exist->rowCount() > 0) {
            while ($row_fetch_uc_exist = $stmt_fetch_uc_exist->fetch(PDO::FETCH_ASSOC)) {
                extract($row_fetch_uc_exist);

                $guest_discount = 0;
                $addon_charges = $row_fetch_uc_exist["addon_charges"];
                $total_unbilled_charges = $row_fetch_uc_exist["total_unbilled_charges"];

                $guest_av_days = strtotime($new_checkout_date)-strtotime($old_checkout_date);
                $guest_av_days = round($guest_av_days / (60 * 60 * 24));
                $_SESSION['cp_guest_av_days'] = $guest_av_days;

                $guest_total_addons = $_SESSION['cp_guest_total_addons'];
                $room_charges = ($room_rate*$guest_av_days);
                $plain_total_payable  = $room_charges + $guest_total_addons;
                $patched_discount = $plain_total_payable * $guest_discount;
                $guest_total_payable = ($plain_total_payable - $patched_discount);
                $guest_total_payable += $total_unbilled_charges;

                $query_3 = "UPDATE tbl_activities_unbilled_charges SET checkout_time = '$new_checkout_time', checkout_date = '$new_checkout_date', room_charges = '$room_charges', total_unbilled_charges = '$guest_total_payable' WHERE transaction_number = '$transaction_id'";
                $stmt_3 = $DBcon->prepare($query_3);
                $stmt_3->execute();
            }
        }
    }
    else{

        $guest_av_days = strtotime($new_checkout_date)-strtotime($old_checkin_date);
        $guest_av_days = round($guest_av_days / (60 * 60 * 24));
        $_SESSION['cp_guest_av_days'] = $guest_av_days;
        $guest_discount = 0;
        $guest_total_addons = $_SESSION['cp_guest_total_addons'];
        $room_charges =($room_rate*$guest_av_days) - $billed_room_charges;
        $plain_total_payable  = $room_charges + $guest_total_addons;
        $patched_discount = $plain_total_payable * $guest_discount;
        $guest_total_payable =  $plain_total_payable + $patched_discount;

        $query_save_bill = "INSERT INTO tbl_activities_unbilled_charges (checkout_time, checkout_date, room_charges, total_unbilled_charges,transaction_number) VALUE ('$new_checkout_time','$new_checkout_date','$room_charges','$guest_total_payable','$transaction_id')";
        $stmt_save_bill = $DBcon->prepare( $query_save_bill );
        $stmt_save_bill ->execute();
    }

    echo "
    <script type='text/javascript'>
            
            swal({
              title: 'SUCCESS!',
              text: \"UPDATED THE CHECKOUT DATE!\",
              type: \"success\",
              timer: 2000,
            }).then(
              function() {
            // Redirect the user
            window.location.href = \"checkin_custom_process.php\";
            console.log('The Ok Button was clicked.');
            },
              // handling the promise rejection
              function (dismiss) {
                if (dismiss === 'timer') {
                   window.location.href = \"checkin_custom_process.php\";
                }
              }
            )
    </script>";

}
?>