<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 1/16/2018
 * Time: 9:25 PM
 */



if(isset($_POST['btnAddCategory'])){

    $room_category = $_POST['room_category'];
    $room_rate = $_POST['room_rate'];
    if($room_category=="" || $room_rate<=0){
        echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR!',
                  text: 'No Room Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRoomCategory.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRoomCategory.php\";
                    }
                  }
                )
			</script>
		";
    }
    else{
        $sql = "INSERT INTO tbl_room_category (room_category,rate)VALUES ('$room_category','$room_rate')";

        if ($conn->query($sql) === TRUE) {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'SUCCESS!',
                  text: \"New Room Category Added!\",
                  type: \"success\",
                  timer: 2000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRoomCategory.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRoomCategory.php\";
                    }
                  }
                )
			</script>
		";
        }
        else {
            echo"
			<script type='text/javascript'>
				
				swal({
                  title: 'ERROR IN QUERY!',
                  text: 'No Room Category Added. Please Try Again',
                  type: \"error\",
                  timer: 10000,
                }).then(
                  function() {
                // Redirect the user
                window.location.href = \"manageRoomCategory.php\";
                console.log('The Ok Button was clicked.');
                },
                  // handling the promise rejection
                  function (dismiss) {
                    if (dismiss === 'timer') {
                       window.location.href = \"manageRoomCategory.php\";
                    }
                  }
                )
			</script>
		";
        }

    }
}
?>



