<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/22/2018
 * Time: 11:36 PM
 */

include "connect.php";
$query_act = "SELECT * FROM tbl_activities";
$stmt_act = $DBcon->prepare( $query_act );
$stmt_act->execute();

if($stmt_act->rowCount() > 0) {

    $i = 0;
    while($row_act=$stmt_act->fetch(PDO::FETCH_ASSOC)) {
        extract($row_act);

        $trasaction_id = $row_act["transaction_number"];

        $query_act_inside = "SELECT * FROM tbl_activities_unbilled_charges WHERE transaction_number = '$trasaction_id'";
        $stmt_act_inside = $DBcon->prepare( $query_act_inside );
        $stmt_act_inside->execute();
        if($stmt_act_inside->rowCount() > 0) {
            while($row_act_inside=$stmt_act_inside->fetch(PDO::FETCH_ASSOC)) {
                extract($row_act_inside);
                $room_charge_inside=$row_act_inside["room_charges"];
                $addon_charge_inside=$row_act_inside["addon_charges"];
                $checkout_date_inside=$row_act_inside["checkout_date"];
                $total_inside=$row_act_inside["total_unbilled_charges"];
                $guest_cash_inside=$row_act_inside["guest_cash"];
                $guest_change_inside=$row_act_inside["guest_change"];
            }
        }
        ?>
        <tr>
            <td><?php echo $row_act["transaction_number"]; ?></td>
            <td><?php echo $row_act["guest_name"]; ?></td>
            <td><?php echo $row_act["room_name"]; ?></td>
            <td><?php echo $row_act["checkin_date"]; ?></td>
            <td><?php echo $row_act["checkout_date"]; ?> ( Extend:
                <?php if($checkout_date_inside != ""){echo $checkout_date_inside;}else{echo "NO";}?>
                )
            </td>
            <td><?php echo $row_act["guest_cash"]+$guest_cash_inside; ?></td>
            <td><?php echo $row_act["guest_total_payable_w_discount"]+$total_inside; ?></td>

            <td><?php echo "Cash"; ?></td>
            <td><?php echo $row_act["clerk"];; ?></td>
            <!--BREAK DOWN-->
            <td><?php echo $row_act["guest_cash"]; ?> ( 2nd transaction:
                <?php if($guest_cash_inside != 0){echo $guest_cash_inside;}else{echo "0";}?>
                )
            </td>
            <td><?php echo $row_act["guest_total_payable_w_discount"];?> (2nd transaction:
                <?php if($total_inside != 0){echo $guest_cash_inside;}else{echo "0";}?>
                )
            </td>

            <td><?php echo $row_act["room_charges"]; ?> (2nd transaction:
                <?php if($room_charge_inside != 0){echo $room_charge_inside;}else{echo "0";}?>
                )
            </td>
            <td><?php echo $row_act["guest_av_days"]; ?>
                (2nd transaction:
                <?php
                if($checkout_date_inside != ""){
                    $guest_av_days_inside = strtotime($checkout_date_inside)-strtotime($row_act["checkout_date"]);
                    $guest_av_days_inside = round($guest_av_days / (60 * 60 * 24));
                    echo $guest_av_days_inside;
                }
                else{
                    echo "0";
                }
                ?>
                )
            </td>
            <td><?php echo "YES"; ?> (2nd transaction: YES)

            </td>
            <td><?php echo $row_act["guest_total_addons"]; ?> (2nd transaction:
                <?php if($addon_charge_inside != 0){echo $addon_charge_inside;}else{echo "0";}?>
                )
            </td>
            <td><?php echo $row_act["guest_change"]; ?> (2nd transaction:
                <?php if($guest_change_inside != 0){echo $guest_change_inside;}else{echo "0";}?>
                )
            </td>
            <td><?php echo $row_act["guest_discount"]; ?> (2nd transaction: 0)

            </td>
        </tr>
        <?php
        $i++; }

}
else{
    echo "<tr><td colspan='15' class='font-bold text-center'>NO DATA</td></tr>";
}
?>