<?php
/**
 * Created by PhpStorm.
 * User: ayson
 * Date: 2/6/2018
 * Time: 9:05 PM
 */

$cp_guest_total_addons = 0;
include "connect.php";
$temp_ao = "SELECT * FROM tbl_activities_addons WHERE transaction_number = '$transaction_id'";
$stmt_temp_ao = $DBcon->prepare( $temp_ao );
$stmt_temp_ao ->execute();


if($stmt_temp_ao->rowCount() > 0) {

    while($row_ao=$stmt_temp_ao->fetch(PDO::FETCH_ASSOC)) {
        extract($row_ao);

        ?>
        <tr>
            <td><?php echo $row_ao["ao_name"]?>
                <?php
                if($row_ao["paid_status"] == "NO"){
                    echo "<span class='text-danger font-bold'>(PAYABLE)</span>";
                    $cp_guest_total_addons+=$row_ao["ao_total"];
                }
                ?>
            </td>
            <td><?php echo $row_ao["ao_quantity"]?></td>
            <td><?php echo $row_ao["ao_cost"]?></td>
            <td><?php echo $row_ao["ao_total"]?></td>
            <td>
                <?php
                if($row_ao["paid_status"] == "YES"){
                    ?>
                    <button type="button" disabled class="btn btn-default"> <i class="fa fa-chain-broken"></i></button>
                    <?php
                }
                else{
                    ?>
                    <button type="button" class="btn btn-default" onclick="clickDelete(this.value);" value="<?php echo $row_ao['id']?>"> <i class="fa fa-trash"></i></button>
                    <?php
                }
                ?>
            </td>
        </tr>
        <?php
    }
}
else{
    echo "<tr><td class='font-bold text-center' colspan='5'>NO DATA AVAILABLE</td></tr>";
}
$_SESSION['cp_guest_total_addons'] = $cp_guest_total_addons;
?>

<script type="text/javascript">
    function clickDelete(val) {
        $.ajax({
            type: "POST",
            url: "../phpfunctions/ajax_delete_addon_cp.php",
            data:'ao_id='+val,
            success: function(data){
                $("#tbl_aos").load(" #tbl_aos");
            }
        });
    }
</script>
