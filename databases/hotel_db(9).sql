-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2018 at 05:40 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hotel_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities`
--

CREATE TABLE IF NOT EXISTS `tbl_activities` (
  `activity_id` int(11) NOT NULL,
  `act_name` varchar(60) NOT NULL,
  `act_status` varchar(20) NOT NULL,
  `floor_position` varchar(60) NOT NULL,
  `room_name` varchar(60) NOT NULL,
  `room_type` varchar(60) NOT NULL,
  `room_rate` varchar(60) NOT NULL,
  `checkin_date` varchar(60) NOT NULL,
  `checkin_time` varchar(60) NOT NULL,
  `checkout_date` varchar(60) NOT NULL,
  `checkout_time` varchar(60) NOT NULL,
  `guest_av_days` int(3) NOT NULL,
  `room_charges` varchar(100) NOT NULL,
  `transaction_date` varchar(60) NOT NULL,
  `transaction_number` text NOT NULL,
  `guest_discount` varchar(5) NOT NULL,
  `guest_total_addons` varchar(100) NOT NULL,
  `guest_total_payable_wo_discount` varchar(100) NOT NULL,
  `guest_total_payable_w_discount` varchar(100) NOT NULL,
  `guest_cash` varchar(100) NOT NULL,
  `guest_change` varchar(100) NOT NULL,
  `guest_name` varchar(100) NOT NULL,
  `guest_gender` varchar(25) NOT NULL,
  `guest_address` varchar(200) NOT NULL,
  `guest_id_type` varchar(60) NOT NULL,
  `guest_id_number` varchar(30) NOT NULL,
  `guest_phone_number` varchar(20) NOT NULL,
  `clerk` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activities`
--

INSERT INTO `tbl_activities` (`activity_id`, `act_name`, `act_status`, `floor_position`, `room_name`, `room_type`, `room_rate`, `checkin_date`, `checkin_time`, `checkout_date`, `checkout_time`, `guest_av_days`, `room_charges`, `transaction_date`, `transaction_number`, `guest_discount`, `guest_total_addons`, `guest_total_payable_wo_discount`, `guest_total_payable_w_discount`, `guest_cash`, `guest_change`, `guest_name`, `guest_gender`, `guest_address`, `guest_id_type`, `guest_id_number`, `guest_phone_number`, `clerk`) VALUES
(1, 'CHECKIN', 'ONGOING', '1', 'Room 100', 'Single', '1500', '02/08/2018', '0:26:29 A.M.', '02/12/2018', '0:26:29 A.M.', 4, '6000', '2018/02/08', 'HTL0000001', '0', '400', '6400', '6400', '6500', '100', 'Mario Quinto', 'Male', 'Malasiqui, Pangasinan', 'Passport', '112123213412', '09502222341', 'clerk02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_addons`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_addons` (
  `id` int(11) NOT NULL,
  `ao_name` varchar(60) NOT NULL,
  `ao_quantity` varchar(4) NOT NULL,
  `ao_cost` varchar(4) NOT NULL,
  `ao_total` varchar(60) NOT NULL,
  `transaction_number` varchar(60) NOT NULL,
  `transaction_date` varchar(60) NOT NULL,
  `paid_status` varchar(10) NOT NULL,
  `clerk` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activities_addons`
--

INSERT INTO `tbl_activities_addons` (`id`, `ao_name`, `ao_quantity`, `ao_cost`, `ao_total`, `transaction_number`, `transaction_date`, `paid_status`, `clerk`) VALUES
(1, 'Pillow', '2', '200', '400', 'HTL0000001', '', 'YES', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_addons_temp`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_addons_temp` (
  `id` int(11) NOT NULL,
  `ao_name` varchar(60) NOT NULL,
  `ao_quantity` varchar(4) NOT NULL,
  `ao_cost` varchar(4) NOT NULL,
  `ao_total` varchar(60) NOT NULL,
  `transaction_number` varchar(60) NOT NULL,
  `transaction_date` varchar(60) NOT NULL,
  `paid_status` varchar(10) NOT NULL,
  `clerk` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_history`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_history` (
  `id` int(11) NOT NULL,
  `act_name` text NOT NULL,
  `transaction_number` varchar(30) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities_unbilled_charges`
--

CREATE TABLE IF NOT EXISTS `tbl_activities_unbilled_charges` (
  `id` int(11) NOT NULL,
  `checkout_time` varchar(20) NOT NULL,
  `checkout_date` varchar(20) NOT NULL,
  `room_charges` varchar(5) NOT NULL,
  `addon_charges` varchar(5) NOT NULL,
  `total_unbilled_charges` varchar(5) NOT NULL,
  `transaction_number` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_activities_unbilled_charges`
--

INSERT INTO `tbl_activities_unbilled_charges` (`id`, `checkout_time`, `checkout_date`, `room_charges`, `addon_charges`, `total_unbilled_charges`, `transaction_number`) VALUES
(5, '12:35 AM', '02/13/2018', '7500', '', '-1100', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_guests`
--

CREATE TABLE IF NOT EXISTS `tbl_guests` (
  `id` int(11) NOT NULL,
  `guest_name` varchar(100) NOT NULL,
  `guest_gender` varchar(25) NOT NULL,
  `guest_address` varchar(250) NOT NULL,
  `guest_id_type` varchar(60) NOT NULL,
  `guest_id_number` varchar(30) NOT NULL,
  `guest_phone_number` varchar(19) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id` int(11) NOT NULL,
  `user` text NOT NULL,
  `activity` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservations`
--

CREATE TABLE IF NOT EXISTS `tbl_reservations` (
  `id` int(11) NOT NULL,
  `room_name` varchar(60) NOT NULL,
  `checkin_time` varchar(20) NOT NULL,
  `checkin_date` varchar(60) NOT NULL,
  `checkout_time` varchar(20) NOT NULL,
  `checkout_date` varchar(60) NOT NULL,
  `guest_name` varchar(60) NOT NULL,
  `guest_address` varchar(60) NOT NULL,
  `guest_id_type` varchar(60) NOT NULL,
  `guest_id_number` varchar(60) NOT NULL,
  `guest_phone` varchar(20) NOT NULL,
  `reservation_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_addons`
--

CREATE TABLE IF NOT EXISTS `tbl_room_addons` (
  `id` int(11) NOT NULL,
  `ao_name` varchar(60) NOT NULL,
  `ao_cost` varchar(60) NOT NULL,
  `stock` varchar(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_addons`
--

INSERT INTO `tbl_room_addons` (`id`, `ao_name`, `ao_cost`, `stock`) VALUES
(1, 'Pillow', '200', ''),
(2, 'Bed Sheet', '500', ''),
(3, 'Blanket', '600', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_category`
--

CREATE TABLE IF NOT EXISTS `tbl_room_category` (
  `id` int(11) NOT NULL,
  `room_category` varchar(60) NOT NULL,
  `rate` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_category`
--

INSERT INTO `tbl_room_category` (`id`, `room_category`, `rate`) VALUES
(3, 'Single', '1500'),
(7, 'Elite', '2500'),
(9, 'Economical', '1300'),
(10, 'Budget', '50000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_list`
--

CREATE TABLE IF NOT EXISTS `tbl_room_list` (
  `id` int(11) NOT NULL,
  `floor_position` varchar(10) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `room_type` varchar(60) NOT NULL,
  `rate` varchar(60) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_list`
--

INSERT INTO `tbl_room_list` (`id`, `floor_position`, `room_name`, `room_type`, `rate`, `status`) VALUES
(1, '1', 'Room 100', 'Single', '1500', 'OCCUPIED'),
(2, '1', 'ROOM 101', 'Single', '1500', 'MAINTENANCE'),
(3, '2', 'ROOM 250', 'Economical', '1300', 'AVAILABLE'),
(4, '1', 'ROOM 102', 'Single', '1500', 'AVAILABLE'),
(5, '1', 'ROOM 103', 'Single', '1500', 'AVAILABLE'),
(6, '1', 'ROOM 104', 'Elite', '2500', 'AVAILABLE'),
(7, '2', 'ROOM 200', 'Economical', '1300', 'AVAILABLE'),
(8, '2', 'ROOM 201', 'Elite', '2500', 'AVAILABLE'),
(9, '2', 'ROOM 203', 'Elite', '2500', 'AVAILABLE'),
(10, '2', 'ROOM 205', 'Budget', '50000', 'AVAILABLE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(11) NOT NULL,
  `minimum_hour` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `minimum_hour`) VALUES
(9999, '12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_number`
--

CREATE TABLE IF NOT EXISTS `tbl_transaction_number` (
  `id` int(11) NOT NULL,
  `t_number` text NOT NULL,
  `status` varchar(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction_number`
--

INSERT INTO `tbl_transaction_number` (`id`, `t_number`, `status`) VALUES
(1, 'HTL0000001', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(200) NOT NULL,
  `password_temp` varchar(60) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `email` text NOT NULL,
  `paths` text NOT NULL,
  `userlevel` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `password_temp`, `first_name`, `last_name`, `email`, `paths`, `userlevel`) VALUES
(1, 'clerk01', 'aafae9b562154fa444a91de51d0f8053', 'clerk01', 'Pedro', 'Penduko', 'pedropenduko@gmail.com', '', 'CLERK');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `tbl_activities_addons`
--
ALTER TABLE `tbl_activities_addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activities_addons_temp`
--
ALTER TABLE `tbl_activities_addons_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activities_history`
--
ALTER TABLE `tbl_activities_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activities_unbilled_charges`
--
ALTER TABLE `tbl_activities_unbilled_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_guests`
--
ALTER TABLE `tbl_guests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_addons`
--
ALTER TABLE `tbl_room_addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_category`
--
ALTER TABLE `tbl_room_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_list`
--
ALTER TABLE `tbl_room_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaction_number`
--
ALTER TABLE `tbl_transaction_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_activities_addons`
--
ALTER TABLE `tbl_activities_addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_activities_addons_temp`
--
ALTER TABLE `tbl_activities_addons_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_activities_history`
--
ALTER TABLE `tbl_activities_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_activities_unbilled_charges`
--
ALTER TABLE `tbl_activities_unbilled_charges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_guests`
--
ALTER TABLE `tbl_guests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_room_addons`
--
ALTER TABLE `tbl_room_addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_room_category`
--
ALTER TABLE `tbl_room_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_room_list`
--
ALTER TABLE `tbl_room_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_transaction_number`
--
ALTER TABLE `tbl_transaction_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
