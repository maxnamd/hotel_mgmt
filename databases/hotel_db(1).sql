-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2018 at 12:47 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hotel_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id` int(11) NOT NULL,
  `user` text NOT NULL,
  `activity` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservations`
--

CREATE TABLE IF NOT EXISTS `tbl_reservations` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_addons`
--

CREATE TABLE IF NOT EXISTS `tbl_room_addons` (
  `id` int(11) NOT NULL,
  `ao_name` varchar(60) NOT NULL,
  `ao_cost` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_addons`
--

INSERT INTO `tbl_room_addons` (`id`, `ao_name`, `ao_cost`) VALUES
(1, 'Pillow', '200'),
(2, 'Bed Sheet', '500');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_category`
--

CREATE TABLE IF NOT EXISTS `tbl_room_category` (
  `id` int(11) NOT NULL,
  `room_category` varchar(60) NOT NULL,
  `rate` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_category`
--

INSERT INTO `tbl_room_category` (`id`, `room_category`, `rate`) VALUES
(3, 'Single', '1500'),
(7, 'Elite', '2500'),
(9, 'Economical', '1300');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_list`
--

CREATE TABLE IF NOT EXISTS `tbl_room_list` (
  `id` int(11) NOT NULL,
  `floor_position` varchar(10) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `room_type` varchar(60) NOT NULL,
  `rate` varchar(60) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room_list`
--

INSERT INTO `tbl_room_list` (`id`, `floor_position`, `room_name`, `room_type`, `rate`, `status`) VALUES
(1, '1', 'Room 100', 'Single', '1500', 'AVAILABLE'),
(2, '1', 'ROOM 101', 'Single', '1500', 'AVAILABLE'),
(3, '2', 'ROOM 250', 'Economical', '1300', 'AVAILABLE'),
(4, '1', 'ROOM 102', 'Single', '1500', ''),
(5, '1', 'ROOM 103', 'Single', '1500', ''),
(6, '1', 'ROOM 104', 'Elite', '2500', ''),
(7, '2', 'ROOM 200', 'Economical', '1300', ''),
(8, '2', 'ROOM 201', 'Elite', '2500', ''),
(9, '2', 'ROOM 203', 'Elite', '2500', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(11) NOT NULL,
  `minimum_hour` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `minimum_hour`) VALUES
(9999, '12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_addons`
--
ALTER TABLE `tbl_room_addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_category`
--
ALTER TABLE `tbl_room_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_room_list`
--
ALTER TABLE `tbl_room_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_reservations`
--
ALTER TABLE `tbl_reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_room_addons`
--
ALTER TABLE `tbl_room_addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_room_category`
--
ALTER TABLE `tbl_room_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_room_list`
--
ALTER TABLE `tbl_room_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
